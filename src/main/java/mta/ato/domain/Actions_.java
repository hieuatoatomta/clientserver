package mta.ato.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Actions.class)
public abstract class Actions_ {

	public static volatile SingularAttribute<Actions, String> code;
	public static volatile SetAttribute<Actions, ObjectAction> dsObjectActions;
	public static volatile SingularAttribute<Actions, String> name;
	public static volatile SetAttribute<Actions, RoleObject> dsRoleObjects;
	public static volatile SingularAttribute<Actions, String> description;
	public static volatile SingularAttribute<Actions, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<Actions, Long> id;
	public static volatile SingularAttribute<Actions, Long> status;

	public static final String CODE = "code";
	public static final String DS_OBJECT_ACTIONS = "dsObjectActions";
	public static final String NAME = "name";
	public static final String DS_ROLE_OBJECTS = "dsRoleObjects";
	public static final String DESCRIPTION = "description";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String STATUS = "status";

}


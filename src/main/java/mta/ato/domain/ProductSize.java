package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ProductSize.
 */
@Entity
@Table(name = "product_size")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProductSize implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Long amount;

    @OneToMany(mappedBy = "productSize")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<DetailedOrders> dsDetailedOrders = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "dsProductSizes", allowSetters = true)
    private Products products;

    @ManyToOne
    @JsonIgnoreProperties(value = "dsProductSizes", allowSetters = true)
    private Sizes sizes;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public ProductSize amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Set<DetailedOrders> getDsDetailedOrders() {
        return dsDetailedOrders;
    }

    public ProductSize dsDetailedOrders(Set<DetailedOrders> detailedOrders) {
        this.dsDetailedOrders = detailedOrders;
        return this;
    }

    public ProductSize addDsDetailedOrders(DetailedOrders detailedOrders) {
        this.dsDetailedOrders.add(detailedOrders);
        detailedOrders.setProductSize(this);
        return this;
    }

    public ProductSize removeDsDetailedOrders(DetailedOrders detailedOrders) {
        this.dsDetailedOrders.remove(detailedOrders);
        detailedOrders.setProductSize(null);
        return this;
    }

    public void setDsDetailedOrders(Set<DetailedOrders> detailedOrders) {
        this.dsDetailedOrders = detailedOrders;
    }

    public Products getProducts() {
        return products;
    }

    public ProductSize products(Products products) {
        this.products = products;
        return this;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Sizes getSizes() {
        return sizes;
    }

    public ProductSize sizes(Sizes sizes) {
        this.sizes = sizes;
        return this;
    }

    public void setSizes(Sizes sizes) {
        this.sizes = sizes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductSize)) {
            return false;
        }
        return id != null && id.equals(((ProductSize) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductSize{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            "}";
    }
}

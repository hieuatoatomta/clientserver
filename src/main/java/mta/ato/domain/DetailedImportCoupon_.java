package mta.ato.domain;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DetailedImportCoupon.class)
public abstract class DetailedImportCoupon_ {

	public static volatile SingularAttribute<DetailedImportCoupon, ImportCoupon> importCoupon;
	public static volatile SingularAttribute<DetailedImportCoupon, Long> amount;
	public static volatile SingularAttribute<DetailedImportCoupon, Sizes> sizes;
	public static volatile SingularAttribute<DetailedImportCoupon, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<DetailedImportCoupon, Long> id;
	public static volatile SingularAttribute<DetailedImportCoupon, BigDecimal> importPrice;
	public static volatile SingularAttribute<DetailedImportCoupon, Products> products;

	public static final String IMPORT_COUPON = "importCoupon";
	public static final String AMOUNT = "amount";
	public static final String SIZES = "sizes";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String IMPORT_PRICE = "importPrice";
	public static final String PRODUCTS = "products";

}


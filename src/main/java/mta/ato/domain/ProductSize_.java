package mta.ato.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductSize.class)
public abstract class ProductSize_ {

	public static volatile SetAttribute<ProductSize, DetailedOrders> dsDetailedOrders;
	public static volatile SingularAttribute<ProductSize, Long> amount;
	public static volatile SingularAttribute<ProductSize, Sizes> sizes;
	public static volatile SingularAttribute<ProductSize, Long> id;
	public static volatile SingularAttribute<ProductSize, Products> products;

	public static final String DS_DETAILED_ORDERS = "dsDetailedOrders";
	public static final String AMOUNT = "amount";
	public static final String SIZES = "sizes";
	public static final String ID = "id";
	public static final String PRODUCTS = "products";

}


package mta.ato.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImageLink.class)
public abstract class ImageLink_ {

	public static volatile SingularAttribute<ImageLink, String> imageLink;
	public static volatile SingularAttribute<ImageLink, String> name;
	public static volatile SingularAttribute<ImageLink, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<ImageLink, Long> id;
	public static volatile SingularAttribute<ImageLink, Long> status;
	public static volatile SingularAttribute<ImageLink, Products> products;

	public static final String IMAGE_LINK = "imageLink";
	public static final String NAME = "name";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String STATUS = "status";
	public static final String PRODUCTS = "products";

}


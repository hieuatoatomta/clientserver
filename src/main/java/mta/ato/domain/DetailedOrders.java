package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A DetailedOrders.
 */
@Entity
@Table(name = "detailed_orders")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DetailedOrders implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Long amount;

    @Column(name = "price", precision = 21, scale = 2)
    private BigDecimal price;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @ManyToOne
    @JsonIgnoreProperties(value = "dsDetailedOrders", allowSetters = true)
    private Orders orders;

    @ManyToOne
    @JsonIgnoreProperties(value = "dsDetailedOrders", allowSetters = true)
    private ProductSize productSize;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public DetailedOrders amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public DetailedOrders price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public DetailedOrders updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Orders getOrders() {
        return orders;
    }

    public DetailedOrders orders(Orders orders) {
        this.orders = orders;
        return this;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public ProductSize getProductSize() {
        return productSize;
    }

    public DetailedOrders productSize(ProductSize productSize) {
        this.productSize = productSize;
        return this;
    }

    public void setProductSize(ProductSize productSize) {
        this.productSize = productSize;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailedOrders)) {
            return false;
        }
        return id != null && id.equals(((DetailedOrders) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailedOrders{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", price=" + getPrice() +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}

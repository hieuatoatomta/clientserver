package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A DetailedImportCoupon.
 */
@Entity
@Table(name = "detailed_import_coupon")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DetailedImportCoupon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Long amount;

    @Column(name = "import_price", precision = 21, scale = 2)
    private BigDecimal importPrice;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @ManyToOne
    @JoinColumn(name = "importCoupon_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = "dsReceipts", allowSetters = true)
    private ImportCoupon importCoupon;

    @ManyToOne
    @JoinColumn(name = "products_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = "dsDetailedImportCoupons", allowSetters = true)
    private Products products;

    @ManyToOne
    @JsonIgnoreProperties(value = "dsDetailedImportCoupons", allowSetters = true)
    private Sizes sizes;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public DetailedImportCoupon amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public BigDecimal getImportPrice() {
        return importPrice;
    }

    public DetailedImportCoupon importPrice(BigDecimal importPrice) {
        this.importPrice = importPrice;
        return this;
    }

    public void setImportPrice(BigDecimal importPrice) {
        this.importPrice = importPrice;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public DetailedImportCoupon updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public ImportCoupon getImportCoupon() {
        return importCoupon;
    }

    public DetailedImportCoupon importCoupon(ImportCoupon importCoupon) {
        this.importCoupon = importCoupon;
        return this;
    }

    public void setImportCoupon(ImportCoupon importCoupon) {
        this.importCoupon = importCoupon;
    }

    public Products getProducts() {
        return products;
    }

    public DetailedImportCoupon products(Products products) {
        this.products = products;
        return this;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Sizes getSizes() {
        return sizes;
    }

    public DetailedImportCoupon sizes(Sizes sizes) {
        this.sizes = sizes;
        return this;
    }

    public void setSizes(Sizes sizes) {
        this.sizes = sizes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailedImportCoupon)) {
            return false;
        }
        return id != null && id.equals(((DetailedImportCoupon) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailedImportCoupon{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", importPrice=" + getImportPrice() +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}

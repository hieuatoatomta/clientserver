package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import mta.ato.utils.annotations.Ignore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A ImportCoupon.
 */
@Entity
@Table(name = "import_coupon")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ImportCoupon implements Serializable {

    @Ignore
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 60)
    @Column(name = "code", length = 60)
    private String code;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @OneToMany(mappedBy = "importCoupon")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<DetailedImportCoupon> dsReceipts = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "users_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = "dsImportCoupons", allowSetters = true)
    private Users users;

    @ManyToOne
    @JoinColumn(name = "supplier_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = "dsImportCoupons", allowSetters = true)
    private Supplier supplier;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public ImportCoupon code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public ImportCoupon description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public ImportCoupon updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Set<DetailedImportCoupon> getDsReceipts() {
        return dsReceipts;
    }

    public ImportCoupon dsReceipts(Set<DetailedImportCoupon> detailedImportCoupons) {
        this.dsReceipts = detailedImportCoupons;
        return this;
    }

    public ImportCoupon addDsReceipt(DetailedImportCoupon detailedImportCoupon) {
        this.dsReceipts.add(detailedImportCoupon);
        detailedImportCoupon.setImportCoupon(this);
        return this;
    }

    public ImportCoupon removeDsReceipt(DetailedImportCoupon detailedImportCoupon) {
        this.dsReceipts.remove(detailedImportCoupon);
        detailedImportCoupon.setImportCoupon(null);
        return this;
    }

    public void setDsReceipts(Set<DetailedImportCoupon> detailedImportCoupons) {
        this.dsReceipts = detailedImportCoupons;
    }

    public Users getUsers() {
        return users;
    }

    public ImportCoupon users(Users users) {
        this.users = users;
        return this;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public ImportCoupon supplier(Supplier supplier) {
        this.supplier = supplier;
        return this;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImportCoupon)) {
            return false;
        }
        return id != null && id.equals(((ImportCoupon) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImportCoupon{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}

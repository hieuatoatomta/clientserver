package mta.ato.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Billing.class)
public abstract class Billing_ {

	public static volatile SingularAttribute<Billing, String> code;
	public static volatile SingularAttribute<Billing, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<Billing, Long> id;
	public static volatile SingularAttribute<Billing, Long> status;

	public static final String CODE = "code";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String STATUS = "status";

}


package mta.ato.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Supplier.class)
public abstract class Supplier_ {

	public static volatile SingularAttribute<Supplier, String> code;
	public static volatile SingularAttribute<Supplier, String> address;
	public static volatile SingularAttribute<Supplier, String> phone;
	public static volatile SingularAttribute<Supplier, String> name;
	public static volatile SingularAttribute<Supplier, String> description;
	public static volatile SetAttribute<Supplier, ImportCoupon> dsImportCoupons;
	public static volatile SingularAttribute<Supplier, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<Supplier, Long> id;
	public static volatile SingularAttribute<Supplier, Long> status;

	public static final String CODE = "code";
	public static final String ADDRESS = "address";
	public static final String PHONE = "phone";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String DS_IMPORT_COUPONS = "dsImportCoupons";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String STATUS = "status";

}


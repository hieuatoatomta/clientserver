package mta.ato.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Users.
 */
@Entity
@Table(name = "users")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 60)
    @Column(name = "name", length = 60)
    private String name;

    @Size(max = 60)
    @Column(name = "full_name", length = 60)
    private String fullName;

    @Size(max = 60)
    @Column(name = "pass", length = 60)
    private String pass;

    @Size(max = 60)
    @Column(name = "path_url", length = 60)
    private String pathUrl;

    @Column(name = "date_of_birth")
    private ZonedDateTime dateOfBirth;

    @Size(max = 100)
    @Column(name = "mail", length = 100)
    private String mail;

    @Size(max = 10)
    @Column(name = "phone", length = 10)
    private String phone;

    @Column(name = "status")
    private Long status;

    @Size(max = 8)
    @Column(name = "reset_key", length = 8)
    private String resetKey;

    @Column(name = "reset_date")
    private ZonedDateTime resetDate;

    @Size(max = 60)
    @Column(name = "creator", length = 60)
    private String creator;

    @Column(name = "creation_time")
    private ZonedDateTime creationTime;

    @OneToMany(mappedBy = "users", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<UserRole> dsUserRoles = new HashSet<>();

    @OneToMany(mappedBy = "users")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ImportCoupon> dsImportCoupons = new HashSet<>();

    @OneToMany(mappedBy = "users")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Orders> dsOrders = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Users name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public Users fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPass() {
        return pass;
    }

    public Users pass(String pass) {
        this.pass = pass;
        return this;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPathUrl() {
        return pathUrl;
    }

    public Users pathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
        return this;
    }

    public void setPathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
    }

    public ZonedDateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public Users dateOfBirth(ZonedDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public void setDateOfBirth(ZonedDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMail() {
        return mail;
    }

    public Users mail(String mail) {
        this.mail = mail;
        return this;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public Users phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getStatus() {
        return status;
    }

    public Users status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getResetKey() {
        return resetKey;
    }

    public Users resetKey(String resetKey) {
        this.resetKey = resetKey;
        return this;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public ZonedDateTime getResetDate() {
        return resetDate;
    }

    public Users resetDate(ZonedDateTime resetDate) {
        this.resetDate = resetDate;
        return this;
    }

    public void setResetDate(ZonedDateTime resetDate) {
        this.resetDate = resetDate;
    }

    public String getCreator() {
        return creator;
    }

    public Users creator(String creator) {
        this.creator = creator;
        return this;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public ZonedDateTime getCreationTime() {
        return creationTime;
    }

    public Users creationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(ZonedDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public Set<UserRole> getDsUserRoles() {
        return dsUserRoles;
    }

    public Users dsUserRoles(Set<UserRole> userRoles) {
        this.dsUserRoles = userRoles;
        return this;
    }

    public Users addDsUserRole(UserRole userRole) {
        this.dsUserRoles.add(userRole);
        userRole.setUsers(this);
        return this;
    }

    public Users removeDsUserRole(UserRole userRole) {
        this.dsUserRoles.remove(userRole);
        userRole.setUsers(null);
        return this;
    }

    public void setDsUserRoles(Set<UserRole> userRoles) {
        this.dsUserRoles = userRoles;
    }

    public Set<ImportCoupon> getDsImportCoupons() {
        return dsImportCoupons;
    }

    public Users dsImportCoupons(Set<ImportCoupon> importCoupons) {
        this.dsImportCoupons = importCoupons;
        return this;
    }

    public Users addDsImportCoupon(ImportCoupon importCoupon) {
        this.dsImportCoupons.add(importCoupon);
        importCoupon.setUsers(this);
        return this;
    }

    public Users removeDsImportCoupon(ImportCoupon importCoupon) {
        this.dsImportCoupons.remove(importCoupon);
        importCoupon.setUsers(null);
        return this;
    }

    public void setDsImportCoupons(Set<ImportCoupon> importCoupons) {
        this.dsImportCoupons = importCoupons;
    }

    public Set<Orders> getDsOrders() {
        return dsOrders;
    }

    public Users dsOrders(Set<Orders> orders) {
        this.dsOrders = orders;
        return this;
    }

    public Users addDsOrders(Orders orders) {
        this.dsOrders.add(orders);
        orders.setUsers(this);
        return this;
    }

    public Users removeDsOrders(Orders orders) {
        this.dsOrders.remove(orders);
        orders.setUsers(null);
        return this;
    }

    public void setDsOrders(Set<Orders> orders) {
        this.dsOrders = orders;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Users)) {
            return false;
        }
        return id != null && id.equals(((Users) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Users{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", fullName='" + getFullName() + "'" +
            ", pass='" + getPass() + "'" +
            ", pathUrl='" + getPathUrl() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            ", mail='" + getMail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", status=" + getStatus() +
            ", resetKey='" + getResetKey() + "'" +
            ", resetDate='" + getResetDate() + "'" +
            ", creator='" + getCreator() + "'" +
            ", creationTime='" + getCreationTime() + "'" +
            "}";
    }
}

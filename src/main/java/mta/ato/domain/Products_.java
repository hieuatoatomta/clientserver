package mta.ato.domain;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Products.class)
public abstract class Products_ {

	public static volatile SingularAttribute<Products, String> code;
	public static volatile SingularAttribute<Products, String> quantity;
	public static volatile SingularAttribute<Products, BigDecimal> cost;
	public static volatile SetAttribute<Products, DetailedImportCoupon> dsDetailedImportCoupons;
	public static volatile SetAttribute<Products, ProductSize> dsProductSizes;
	public static volatile SingularAttribute<Products, Objects> objects;
	public static volatile SingularAttribute<Products, String> description;
	public static volatile SingularAttribute<Products, ZonedDateTime> updateTime;
	public static volatile SetAttribute<Products, ImageLink> dsImageLinks;
	public static volatile SingularAttribute<Products, ZonedDateTime> relaseDate;
	public static volatile SingularAttribute<Products, Long> rate;
	public static volatile SingularAttribute<Products, String> name;
	public static volatile SingularAttribute<Products, Long> id;
	public static volatile SingularAttribute<Products, Long> status;

	public static final String CODE = "code";
	public static final String QUANTITY = "quantity";
	public static final String COST = "cost";
	public static final String DS_DETAILED_IMPORT_COUPONS = "dsDetailedImportCoupons";
	public static final String DS_PRODUCT_SIZES = "dsProductSizes";
	public static final String OBJECTS = "objects";
	public static final String DESCRIPTION = "description";
	public static final String UPDATE_TIME = "updateTime";
	public static final String DS_IMAGE_LINKS = "dsImageLinks";
	public static final String RELASE_DATE = "relaseDate";
	public static final String RATE = "rate";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String STATUS = "status";

}


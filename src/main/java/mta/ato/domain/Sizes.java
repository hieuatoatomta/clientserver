package mta.ato.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Sizes.
 */
@Entity
@Table(name = "sizes")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Sizes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 60)
    @Column(name = "name", length = 60)
    private String name;

    @Size(max = 60)
    @Column(name = "code", length = 60)
    private String code;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "status")
    private Long status;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @OneToMany(mappedBy = "sizes")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ProductSize> dsProductSizes = new HashSet<>();

    @OneToMany(mappedBy = "sizes")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<DetailedImportCoupon> dsDetailedImportCoupons = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Sizes name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public Sizes code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public Sizes description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatus() {
        return status;
    }

    public Sizes status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public Sizes updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Set<ProductSize> getDsProductSizes() {
        return dsProductSizes;
    }

    public Sizes dsProductSizes(Set<ProductSize> productSizes) {
        this.dsProductSizes = productSizes;
        return this;
    }

    public Sizes addDsProductSize(ProductSize productSize) {
        this.dsProductSizes.add(productSize);
        productSize.setSizes(this);
        return this;
    }

    public Sizes removeDsProductSize(ProductSize productSize) {
        this.dsProductSizes.remove(productSize);
        productSize.setSizes(null);
        return this;
    }

    public void setDsProductSizes(Set<ProductSize> productSizes) {
        this.dsProductSizes = productSizes;
    }

    public Set<DetailedImportCoupon> getDsDetailedImportCoupons() {
        return dsDetailedImportCoupons;
    }

    public Sizes dsDetailedImportCoupons(Set<DetailedImportCoupon> detailedImportCoupons) {
        this.dsDetailedImportCoupons = detailedImportCoupons;
        return this;
    }

    public Sizes addDsDetailedImportCoupon(DetailedImportCoupon detailedImportCoupon) {
        this.dsDetailedImportCoupons.add(detailedImportCoupon);
        detailedImportCoupon.setSizes(this);
        return this;
    }

    public Sizes removeDsDetailedImportCoupon(DetailedImportCoupon detailedImportCoupon) {
        this.dsDetailedImportCoupons.remove(detailedImportCoupon);
        detailedImportCoupon.setSizes(null);
        return this;
    }

    public void setDsDetailedImportCoupons(Set<DetailedImportCoupon> detailedImportCoupons) {
        this.dsDetailedImportCoupons = detailedImportCoupons;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sizes)) {
            return false;
        }
        return id != null && id.equals(((Sizes) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Sizes{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", status=" + getStatus() +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}

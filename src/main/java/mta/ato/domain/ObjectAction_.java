package mta.ato.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ObjectAction.class)
public abstract class ObjectAction_ {

	public static volatile SingularAttribute<ObjectAction, Objects> objects;
	public static volatile SingularAttribute<ObjectAction, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<ObjectAction, Long> id;
	public static volatile SingularAttribute<ObjectAction, Actions> actions;

	public static final String OBJECTS = "objects";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String ACTIONS = "actions";

}


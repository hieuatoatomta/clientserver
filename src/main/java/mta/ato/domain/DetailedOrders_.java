package mta.ato.domain;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DetailedOrders.class)
public abstract class DetailedOrders_ {

	public static volatile SingularAttribute<DetailedOrders, Long> amount;
	public static volatile SingularAttribute<DetailedOrders, BigDecimal> price;
	public static volatile SingularAttribute<DetailedOrders, ProductSize> productSize;
	public static volatile SingularAttribute<DetailedOrders, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<DetailedOrders, Long> id;
	public static volatile SingularAttribute<DetailedOrders, Orders> orders;

	public static final String AMOUNT = "amount";
	public static final String PRICE = "price";
	public static final String PRODUCT_SIZE = "productSize";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String ORDERS = "orders";

}


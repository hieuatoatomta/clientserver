package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Products.
 */
@Entity
@Table(name = "products")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Products implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 60)
    @Column(name = "name", length = 60)
    private String name;

    @Size(max = 60)
    @Column(name = "code", length = 60)
    private String code;

    @Column(name = "quantity")
    private String quantity;

    @Column(name = "cost", precision = 21, scale = 2)
    private BigDecimal cost;

    @Column(name = "rate")
    private Long rate;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "status")
    private Long status;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @Column(name = "relase_date")
    private ZonedDateTime relaseDate;

    @OneToMany(mappedBy = "products", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<ProductSize> dsProductSizes = new HashSet<>();

    @OneToMany(mappedBy = "products", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONE)
    private Set<ImageLink> dsImageLinks = new HashSet<>();

    @OneToMany(mappedBy = "products")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<DetailedImportCoupon> dsDetailedImportCoupons = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "dsObjects", allowSetters = true)
    private Objects objects;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Products name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public Products code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getQuantity() {
        return quantity;
    }

    public Products quantity(String quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public Products cost(BigDecimal cost) {
        this.cost = cost;
        return this;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Long getRate() {
        return rate;
    }

    public Products rate(Long rate) {
        this.rate = rate;
        return this;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public String getDescription() {
        return description;
    }

    public Products description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatus() {
        return status;
    }

    public Products status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public Products updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public ZonedDateTime getRelaseDate() {
        return relaseDate;
    }

    public Products relaseDate(ZonedDateTime relaseDate) {
        this.relaseDate = relaseDate;
        return this;
    }

    public void setRelaseDate(ZonedDateTime relaseDate) {
        this.relaseDate = relaseDate;
    }

    public Set<ProductSize> getDsProductSizes() {
        return dsProductSizes;
    }

    public Products dsProductSizes(Set<ProductSize> productSizes) {
        this.dsProductSizes = productSizes;
        return this;
    }

    public Products addDsProductSize(ProductSize productSize) {
        this.dsProductSizes.add(productSize);
        productSize.setProducts(this);
        return this;
    }

    public Products removeDsProductSize(ProductSize productSize) {
        this.dsProductSizes.remove(productSize);
        productSize.setProducts(null);
        return this;
    }

    public void setDsProductSizes(Set<ProductSize> productSizes) {
        this.dsProductSizes = productSizes;
    }

    public Set<ImageLink> getDsImageLinks() {
        return dsImageLinks;
    }

    public Products dsImageLinks(Set<ImageLink> imageLinks) {
        this.dsImageLinks = imageLinks;
        return this;
    }

    public Products addDsImageLink(ImageLink imageLink) {
        this.dsImageLinks.add(imageLink);
        imageLink.setProducts(this);
        return this;
    }

    public Products removeDsImageLink(ImageLink imageLink) {
        this.dsImageLinks.remove(imageLink);
        imageLink.setProducts(null);
        return this;
    }

    public void setDsImageLinks(Set<ImageLink> imageLinks) {
        this.dsImageLinks = imageLinks;
    }

    public Set<DetailedImportCoupon> getDsDetailedImportCoupons() {
        return dsDetailedImportCoupons;
    }

    public Products dsDetailedImportCoupons(Set<DetailedImportCoupon> detailedImportCoupons) {
        this.dsDetailedImportCoupons = detailedImportCoupons;
        return this;
    }

    public Products addDsDetailedImportCoupon(DetailedImportCoupon detailedImportCoupon) {
        this.dsDetailedImportCoupons.add(detailedImportCoupon);
        detailedImportCoupon.setProducts(this);
        return this;
    }

    public Products removeDsDetailedImportCoupon(DetailedImportCoupon detailedImportCoupon) {
        this.dsDetailedImportCoupons.remove(detailedImportCoupon);
        detailedImportCoupon.setProducts(null);
        return this;
    }

    public void setDsDetailedImportCoupons(Set<DetailedImportCoupon> detailedImportCoupons) {
        this.dsDetailedImportCoupons = detailedImportCoupons;
    }

    public Objects getObjects() {
        return objects;
    }

    public Products objects(Objects objects) {
        this.objects = objects;
        return this;
    }

    public void setObjects(Objects objects) {
        this.objects = objects;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Products)) {
            return false;
        }
        return id != null && id.equals(((Products) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Products{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", quantity='" + getQuantity() + "'" +
            ", cost=" + getCost() +
            ", rate=" + getRate() +
            ", description='" + getDescription() + "'" +
            ", status=" + getStatus() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", relaseDate='" + getRelaseDate() + "'" +
            "}";
    }
}

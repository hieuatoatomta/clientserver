package mta.ato.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Sizes.class)
public abstract class Sizes_ {

	public static volatile SingularAttribute<Sizes, String> code;
	public static volatile SetAttribute<Sizes, DetailedImportCoupon> dsDetailedImportCoupons;
	public static volatile SetAttribute<Sizes, ProductSize> dsProductSizes;
	public static volatile SingularAttribute<Sizes, String> name;
	public static volatile SingularAttribute<Sizes, String> description;
	public static volatile SingularAttribute<Sizes, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<Sizes, Long> id;
	public static volatile SingularAttribute<Sizes, Long> status;

	public static final String CODE = "code";
	public static final String DS_DETAILED_IMPORT_COUPONS = "dsDetailedImportCoupons";
	public static final String DS_PRODUCT_SIZES = "dsProductSizes";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String STATUS = "status";

}


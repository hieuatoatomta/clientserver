package mta.ato.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImportCoupon.class)
public abstract class ImportCoupon_ {

	public static volatile SingularAttribute<ImportCoupon, String> code;
	public static volatile SingularAttribute<ImportCoupon, Supplier> supplier;
	public static volatile SingularAttribute<ImportCoupon, String> description;
	public static volatile SingularAttribute<ImportCoupon, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<ImportCoupon, Long> id;
	public static volatile SetAttribute<ImportCoupon, DetailedImportCoupon> dsReceipts;
	public static volatile SingularAttribute<ImportCoupon, Users> users;

	public static final String CODE = "code";
	public static final String SUPPLIER = "supplier";
	public static final String DESCRIPTION = "description";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String DS_RECEIPTS = "dsReceipts";
	public static final String USERS = "users";

}


package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import mta.ato.utils.annotations.Ignore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A ObjectAction.
 */
@Entity
@Table(name = "object_action")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ObjectAction implements Serializable {

    @Ignore
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @ManyToOne
    @JsonIgnoreProperties(value = "dsObjectActions", allowSetters = true)
    private Objects objects;

    @ManyToOne
    @JsonIgnoreProperties(value = "dsObjectActions", allowSetters = true)
    private Actions actions;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public ObjectAction updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Objects getObjects() {
        return objects;
    }

    public ObjectAction objects(Objects objects) {
        this.objects = objects;
        return this;
    }

    public void setObjects(Objects objects) {
        this.objects = objects;
    }

    public Actions getActions() {
        return actions;
    }

    public ObjectAction actions(Actions actions) {
        this.actions = actions;
        return this;
    }

    public void setActions(Actions actions) {
        this.actions = actions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ObjectAction)) {
            return false;
        }
        return id != null && id.equals(((ObjectAction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ObjectAction{" +
            "id=" + getId() +
            ", updateTime='" + getUpdateTime() + "'" +
            "}";
    }
}

package mta.ato.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserRole.class)
public abstract class UserRole_ {

	public static volatile SingularAttribute<UserRole, Roles> roles;
	public static volatile SingularAttribute<UserRole, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<UserRole, Long> id;
	public static volatile SingularAttribute<UserRole, Users> users;

	public static final String ROLES = "roles";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String USERS = "users";

}


package mta.ato.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import mta.ato.utils.annotations.Ignore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Orders.
 */
@Entity
@Table(name = "orders")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Orders implements Serializable {

    @Ignore
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 60)
    @Column(name = "code", length = 60)
    private String code;

    @Column(name = "address")
    private String address;

    @Column(name = "status")
    private Long status;

    @Column(name = "update_time")
    private ZonedDateTime updateTime;

    @Column(name = "contact")
    private String contact;

    @Size(max = 1000)
    @Column(name = "description", length = 1000)
    private String description;

    @OneToMany(mappedBy = "orders")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<DetailedOrders> dsDetailedOrders = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "users_id", referencedColumnName = "id")
    @JsonIgnoreProperties(value = "dsOrders", allowSetters = true)
    private Users users;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Orders code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public Orders address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getStatus() {
        return status;
    }

    public Orders status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public Orders updateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getContact() {
        return contact;
    }

    public Orders contact(String contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDescription() {
        return description;
    }

    public Orders description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<DetailedOrders> getDsDetailedOrders() {
        return dsDetailedOrders;
    }

    public Orders dsDetailedOrders(Set<DetailedOrders> detailedOrders) {
        this.dsDetailedOrders = detailedOrders;
        return this;
    }

    public Orders addDsDetailedOrders(DetailedOrders detailedOrders) {
        this.dsDetailedOrders.add(detailedOrders);
        detailedOrders.setOrders(this);
        return this;
    }

    public Orders removeDsDetailedOrders(DetailedOrders detailedOrders) {
        this.dsDetailedOrders.remove(detailedOrders);
        detailedOrders.setOrders(null);
        return this;
    }

    public void setDsDetailedOrders(Set<DetailedOrders> detailedOrders) {
        this.dsDetailedOrders = detailedOrders;
    }

    public Users getUsers() {
        return users;
    }

    public Orders users(Users users) {
        this.users = users;
        return this;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Orders)) {
            return false;
        }
        return id != null && id.equals(((Orders) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Orders{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", address='" + getAddress() + "'" +
            ", status=" + getStatus() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", contact='" + getContact() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}

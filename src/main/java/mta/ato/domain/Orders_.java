package mta.ato.domain;

import java.time.ZonedDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Orders.class)
public abstract class Orders_ {

	public static volatile SetAttribute<Orders, DetailedOrders> dsDetailedOrders;
	public static volatile SingularAttribute<Orders, String> code;
	public static volatile SingularAttribute<Orders, String> contact;
	public static volatile SingularAttribute<Orders, String> address;
	public static volatile SingularAttribute<Orders, String> description;
	public static volatile SingularAttribute<Orders, ZonedDateTime> updateTime;
	public static volatile SingularAttribute<Orders, Long> id;
	public static volatile SingularAttribute<Orders, Users> users;
	public static volatile SingularAttribute<Orders, Long> status;

	public static final String DS_DETAILED_ORDERS = "dsDetailedOrders";
	public static final String CODE = "code";
	public static final String ADDRESS = "address";
	public static final String UPDATE_TIME = "updateTime";
	public static final String ID = "id";
	public static final String USERS = "users";
	public static final String STATUS = "status";
	public static final String DESCRIPTION = "description";
	public static final String CONTACT = "contact";

}


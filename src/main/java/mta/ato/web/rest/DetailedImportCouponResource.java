package mta.ato.web.rest;

import mta.ato.service.DetailedImportCouponService;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.dto.DetailedImportCouponDTO;
import mta.ato.service.dto.DetailedImportCouponCriteria;
import mta.ato.service.DetailedImportCouponQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.DetailedImportCoupon}.
 */
@RestController
@RequestMapping("/api")
public class DetailedImportCouponResource {

    private final Logger log = LoggerFactory.getLogger(DetailedImportCouponResource.class);

    private static final String ENTITY_NAME = "detailedImportCoupon";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailedImportCouponService detailedImportCouponService;

    private final DetailedImportCouponQueryService detailedImportCouponQueryService;

    public DetailedImportCouponResource(DetailedImportCouponService detailedImportCouponService, DetailedImportCouponQueryService detailedImportCouponQueryService) {
        this.detailedImportCouponService = detailedImportCouponService;
        this.detailedImportCouponQueryService = detailedImportCouponQueryService;
    }

    /**
     * {@code POST  /detailed-import-coupons} : Create a new detailedImportCoupon.
     *
     * @param detailedImportCouponDTO the detailedImportCouponDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new detailedImportCouponDTO, or with status {@code 400 (Bad Request)} if the detailedImportCoupon has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/detailed-import-coupons")
    public ResponseEntity<DetailedImportCouponDTO> createDetailedImportCoupon(@RequestBody DetailedImportCouponDTO detailedImportCouponDTO) throws URISyntaxException {
        log.debug("REST request to save DetailedImportCoupon : {}", detailedImportCouponDTO);
        if (detailedImportCouponDTO.getId() != null) {
            throw new BadRequestAlertException("A new detailedImportCoupon cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DetailedImportCouponDTO result = detailedImportCouponService.save(detailedImportCouponDTO);
        return ResponseEntity.created(new URI("/api/detailed-import-coupons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /detailed-import-coupons} : Updates an existing detailedImportCoupon.
     *
     * @param detailedImportCouponDTO the detailedImportCouponDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailedImportCouponDTO,
     * or with status {@code 400 (Bad Request)} if the detailedImportCouponDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the detailedImportCouponDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/detailed-import-coupons")
    public ResponseEntity<DetailedImportCouponDTO> updateDetailedImportCoupon(@RequestBody DetailedImportCouponDTO detailedImportCouponDTO) throws URISyntaxException {
        log.debug("REST request to update DetailedImportCoupon : {}", detailedImportCouponDTO);
        if (detailedImportCouponDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DetailedImportCouponDTO result = detailedImportCouponService.save(detailedImportCouponDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, detailedImportCouponDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /detailed-import-coupons} : get all the detailedImportCoupons.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailedImportCoupons in body.
     */
    @GetMapping("/detailed-import-coupons")
    public ResponseEntity<List<DetailedImportCouponDTO>> getAllDetailedImportCoupons(DetailedImportCouponCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DetailedImportCoupons by criteria: {}", criteria);
        Page<DetailedImportCouponDTO> page = detailedImportCouponQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /detailed-import-coupons/count} : count all the detailedImportCoupons.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/detailed-import-coupons/count")
    public ResponseEntity<Long> countDetailedImportCoupons(DetailedImportCouponCriteria criteria) {
        log.debug("REST request to count DetailedImportCoupons by criteria: {}", criteria);
        return ResponseEntity.ok().body(detailedImportCouponQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /detailed-import-coupons/:id} : get the "id" detailedImportCoupon.
     *
     * @param id the id of the detailedImportCouponDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailedImportCouponDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/detailed-import-coupons/{id}")
    public ResponseEntity<DetailedImportCouponDTO> getDetailedImportCoupon(@PathVariable Long id) {
        log.debug("REST request to get DetailedImportCoupon : {}", id);
        Optional<DetailedImportCouponDTO> detailedImportCouponDTO = detailedImportCouponService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailedImportCouponDTO);
    }

    /**
     * {@code DELETE  /detailed-import-coupons/:id} : delete the "id" detailedImportCoupon.
     *
     * @param id the id of the detailedImportCouponDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/detailed-import-coupons/{id}")
    public ResponseEntity<Void> deleteDetailedImportCoupon(@PathVariable Long id) {
        log.debug("REST request to delete DetailedImportCoupon : {}", id);
        detailedImportCouponService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package mta.ato.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import mta.ato.config.Constants;
import mta.ato.domain.*;
import mta.ato.service.*;
import mta.ato.service.dto.*;
import mta.ato.utils.*;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.service.spi.ServiceException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.Orders}.
 */
@RestController
@RequestMapping("/api")
public class OrdersResource {

    private final Logger log = LoggerFactory.getLogger(OrdersResource.class);

    private static final String ENTITY_NAME = "orders";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrdersService ordersService;
    private final ProductsService productsService;
    private final ProductSizeService productSizeService;
    private final UsersService usersService;
    private final SizesService sizesService;
    private final DetailedOrdersService detailedOrdersService;

    private final OrdersQueryService ordersQueryService;

    @Autowired
    MailService mailService;

    public OrdersResource(OrdersService ordersService, OrdersQueryService ordersQueryService,
                          ProductsService productsService,
                          ProductSizeService productSizeService,
                          DetailedOrdersService detailedOrdersService,
                          SizesService sizesService,
                          UsersService usersService) {
        this.ordersService = ordersService;
        this.ordersQueryService = ordersQueryService;
        this.productsService = productsService;
        this.productSizeService = productSizeService;
        this.usersService = usersService;
        this.detailedOrdersService = detailedOrdersService;
        this.sizesService = sizesService;
    }

    @PostMapping(value = "/createOrders", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createOrders(@Valid @RequestBody OrdersDTO ordersDTO, BindingResult result) throws Exception {
        if (ordersDTO.getId() != null) {
            throw new BadRequestAlertException("A new order cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                JSONObject jsonObject = CreateOrder.createOrder(ordersDTO.getTotalPrice(), "Thanh toán hoá đơn ");
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", jsonObject));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    @PostMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createOrder(@Valid @RequestBody OrdersDTO ordersDTO, BindingResult result) throws Exception {
        if (ordersDTO.getId() != null) {
            throw new BadRequestAlertException("A new order cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                String appId = ordersDTO.getAppId();
                JSONObject jsonObject1 = GetOrderStatus.getStatusOrders(appId);
                if (jsonObject1.get("returncode").equals(1)) {
                    Users users = usersService.findByMail(ordersDTO.getMail());
                    if (users == null) {
                        // khách hàng chưa tồn tại, tạo 1 tài khoản user mới
                        UsersDTO usersDTO = new UsersDTO();
                        usersDTO.setPhone(ordersDTO.getPhone());
                        usersDTO.setMail(ordersDTO.getMail());
                        usersDTO.setName(ordersDTO.getMail());
                        usersDTO.setStatus(1L);
                        usersDTO.setFullName(ordersDTO.getName());
                        usersDTO.setPass(RandomStringUtils.randomAlphabetic(8));
                        List<Long> list = new ArrayList<>();
                        list.add(2L);
                        usersDTO.setListRole(list);
                        usersService.save(usersDTO);
                        users = usersService.findByMail(ordersDTO.getMail());
                    }
                    String code = RandomStringUtils.randomNumeric(4) + DateUtil.getDateToString();
                    ordersDTO.setUsersId(users.getId());
                    ordersDTO.setUpdateTime(DateUtil.getDateC());
                    ordersDTO.setContact(ordersDTO.getName() + " - " + ordersDTO.getPhone());
                    ordersDTO.setCode(code);
                    ordersDTO.setStatus(0L); // trạng thái đang thanh toán
                    ordersService.save(ordersDTO);

                    Orders order = ordersService.findByCode(code);
                    if (!ordersDTO.getCustomOrderDTO().isEmpty()) {
                        for (int i = 0; i < ordersDTO.getCustomOrderDTO().size(); i++) {
                            Optional<Products> products = productsService.findOne(ordersDTO.getCustomOrderDTO().get(0).getProductsId());
                            Optional<Sizes> sizes = sizesService.findOne(ordersDTO.getCustomOrderDTO().get(i).getSizesId());
                            Products products1 = products.get();
                            ProductSize productSize = productSizeService.findByProductsAndSizes(products1, sizes.get());
                            if (ordersDTO.getCustomOrderDTO().get(i).getAmount() > productSize.getAmount()) {
                                throw new IllegalArgumentException("Số lượng hàng trong kho không đủ!");
                            } else {
                                DetailedOrdersDTO detailedOrderDTO = new DetailedOrdersDTO();
                                detailedOrderDTO.setUpdateTime(DateUtil.getDateC());
                                detailedOrderDTO.setOrdersId(order.getId());
                                detailedOrderDTO.setAmount(ordersDTO.getCustomOrderDTO().get(i).getAmount());
                                detailedOrderDTO.setPrice(products1.getCost());
                                detailedOrderDTO.setProductSizeId(productSize.getId());
                                detailedOrdersService.save(detailedOrderDTO);
                                // sau khi them thanh cong thi tru so luong da them vao bang productSize
                                Long amount = productSize.getAmount() - ordersDTO.getCustomOrderDTO().get(i).getAmount();
                                productSize.setAmount(amount);
                                productSizeService.saveEntity(productSize);
                            }
                        }
                    }
                    JSONObject jsonObject = CreateOrder.createOrder(ordersDTO.getTotalPrice(), "Thanh toán hoá đơn ");
                    String noiDung ="Xin Chào"  + "<b> " + ordersDTO.getName() + "</b></p>" + " "
                        + Translator.toLocale(Constants.ORDER_CONTENT1)
                        + Translator.toLocale( Constants.EMAIL_CONTENT2 );
                    if (!mailService.sendEmail( ordersDTO.getMail(), Translator.toLocale( Constants.EMAIL_SUBJECT1 ), noiDung, Constants.IS_MULTI_PART, Constants.IS_HTML )) {
                        throw new ServiceException( Translator.toLocale( Constants.EMAIL_FALSE ) );
                    }
                    jsonObject.put("code", order.getCode());
                    return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", jsonObject));
                } else {
                    return ResponseEntity.badRequest().body("Thanh toán thất bị");
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

//    @PostMapping("/sendSimpleEmailToOrder")
//    public ResponseEntity<Object> sendSimpleEmail( @RequestBody ChangePassDTO changePassDTO) {
//        try {
////            UsersDTO sysUserDTO = userJWTService.forgotPassword(  changePassDTO );
////            String emailTo = changePassDTO.getEmail();
////            String refreshToken = RandomStringUtils.randomAlphanumeric( 6 );
////            sysUserDTO.setResetKey( refreshToken );
////            userJWTService.updateKey( sysUserDTO );
//            String htmlMsg = Translator.toLocale(Constants.EMAIL_START)  + "<b> " + sysUserDTO.getFullName() + "</b></p>" + " "
//                + Translator.toLocale(Constants.EMAIL_CONTENT1)
//                + Translator.toLocale( Constants.EMAIL_PATH ) + refreshToken + "  "
//                + Translator.toLocale( Constants.EMAIL_CONTENT2 );
//            if (!mailService.sendEmail( emailTo, Translator.toLocale( Constants.EMAIL_SUBJECT ), htmlMsg, Constants.IS_MULTI_PART, Constants.IS_HTML )) {
//                throw new ServiceException( Translator.toLocale( Constants.EMAIL_FALSE ) );
//            }
//
//            return ResponseEntity.status( 200 ).body(  Translator.toLocale( Constants.EMAIL_TRUE ) );
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//            return ResponseEntity.status(400).body( Utils.getStatusBadRequest(e.getMessage()));
//        }
//    }

    @GetMapping("/callback/{appId}/{code}")
    public Object callback(@PathVariable String appId, @PathVariable String code) {
        JSONObject result = new JSONObject();

        try {
            Orders orders = ordersService.findByCode(code);
            JSONObject jsonObject = GetOrderStatus.getStatusOrders(appId);
            if (jsonObject.get("returncode").equals(1)) {
                // đơn hàng thanh toán thành công
                // cap nhat lại trạng thái đơn hàng
                orders.setStatus(0L);
                ordersService.savetoEntity(orders);
                return ResponseEntity.ok().body(Utils.getStatusOk("Đơn hàng đã thanh toán thành công", null));
            } else {
                orders.setStatus(1L);
                ordersService.savetoEntity(orders);
                return ResponseEntity.ok().body(Utils.getStatusOk("Đơn hàng của bạn chưa thanh toán thành công", null));

            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /orders} : Updates an existing orders.
     *
     * @param ordersDTO the ordersDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ordersDTO,
     * or with status {@code 400 (Bad Request)} if the ordersDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ordersDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/orders")
    public ResponseEntity<OrdersDTO> updateOrders(@Valid @RequestBody OrdersDTO ordersDTO) throws URISyntaxException {
        log.debug("REST request to update Orders : {}", ordersDTO);
        if (ordersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrdersDTO result = ordersService.save(ordersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ordersDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /orders} : get all the orders.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orders in body.
     */
    @GetMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllOrders(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            OrdersCriteria criteria = Utils.mappingCriteria(OrdersCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Users by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Orders_.ID);
            Page<Orders> page = ordersQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Orders item : page) {
                JSONObject usersJson = Utils.convertEntityToJSONObject(item);
                arrayResult.put(usersJson);
            }
            JSONObject result = new JSONObject();
            result.put("count", ordersQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /orders/count} : count all the orders.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/orders/count")
    public ResponseEntity<Long> countOrders(OrdersCriteria criteria) {
        log.debug("REST request to count Orders by criteria: {}", criteria);
        return ResponseEntity.ok().body(ordersQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /orders/:id} : get the "id" orders.
     *
     * @param id the id of the ordersDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ordersDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/orders/{id}")
    public ResponseEntity<OrdersDTO> getOrders(@PathVariable Long id) {
        log.debug("REST request to get Orders : {}", id);
        Optional<OrdersDTO> ordersDTO = ordersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ordersDTO);
    }

    /**
     * {@code DELETE  /orders/:id} : delete the "id" orders.
     *
     * @param id the id of the ordersDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Void> deleteOrders(@PathVariable Long id) {
        log.debug("REST request to delete Orders : {}", id);
        ordersService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package mta.ato.web.rest;

import mta.ato.service.BillingService;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.dto.BillingDTO;
import mta.ato.service.dto.BillingCriteria;
import mta.ato.service.BillingQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.Billing}.
 */
@RestController
@RequestMapping("/api")
public class BillingResource {

    private final Logger log = LoggerFactory.getLogger(BillingResource.class);

    private static final String ENTITY_NAME = "billing";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BillingService billingService;

    private final BillingQueryService billingQueryService;

    public BillingResource(BillingService billingService, BillingQueryService billingQueryService) {
        this.billingService = billingService;
        this.billingQueryService = billingQueryService;
    }

    /**
     * {@code POST  /billings} : Create a new billing.
     *
     * @param billingDTO the billingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new billingDTO, or with status {@code 400 (Bad Request)} if the billing has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/billings")
    public ResponseEntity<BillingDTO> createBilling(@Valid @RequestBody BillingDTO billingDTO) throws URISyntaxException {
        log.debug("REST request to save Billing : {}", billingDTO);
        if (billingDTO.getId() != null) {
            throw new BadRequestAlertException("A new billing cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BillingDTO result = billingService.save(billingDTO);
        return ResponseEntity.created(new URI("/api/billings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /billings} : Updates an existing billing.
     *
     * @param billingDTO the billingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated billingDTO,
     * or with status {@code 400 (Bad Request)} if the billingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the billingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/billings")
    public ResponseEntity<BillingDTO> updateBilling(@Valid @RequestBody BillingDTO billingDTO) throws URISyntaxException {
        log.debug("REST request to update Billing : {}", billingDTO);
        if (billingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BillingDTO result = billingService.save(billingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, billingDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /billings} : get all the billings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of billings in body.
     */
    @GetMapping("/billings")
    public ResponseEntity<List<BillingDTO>> getAllBillings(BillingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Billings by criteria: {}", criteria);
        Page<BillingDTO> page = billingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /billings/count} : count all the billings.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/billings/count")
    public ResponseEntity<Long> countBillings(BillingCriteria criteria) {
        log.debug("REST request to count Billings by criteria: {}", criteria);
        return ResponseEntity.ok().body(billingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /billings/:id} : get the "id" billing.
     *
     * @param id the id of the billingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the billingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/billings/{id}")
    public ResponseEntity<BillingDTO> getBilling(@PathVariable Long id) {
        log.debug("REST request to get Billing : {}", id);
        Optional<BillingDTO> billingDTO = billingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(billingDTO);
    }

    /**
     * {@code DELETE  /billings/:id} : delete the "id" billing.
     *
     * @param id the id of the billingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/billings/{id}")
    public ResponseEntity<Void> deleteBilling(@PathVariable Long id) {
        log.debug("REST request to delete Billing : {}", id);
        billingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

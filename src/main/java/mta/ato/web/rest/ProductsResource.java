package mta.ato.web.rest;

import com.google.common.collect.Lists;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import mta.ato.domain.ImageLink;
import mta.ato.domain.ProductSize;
import mta.ato.domain.Products;
import mta.ato.domain.Products_;
import mta.ato.service.ProductsQueryService;
import mta.ato.service.ProductsService;
import mta.ato.service.dto.ProductSizeDTO;
import mta.ato.service.dto.ProductsCriteria;
import mta.ato.service.dto.ProductsDTO;
import mta.ato.service.mapper.ImageLinkMapper;
import mta.ato.service.mapper.ProductSizeMapper;
import mta.ato.service.mapper.ProductsMapper;
import mta.ato.utils.DateUtil;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing {@link mta.ato.domain.Products}.
 */
@RestController
@RequestMapping("/api")
public class ProductsResource {

    private final Logger log = LoggerFactory.getLogger(ProductsResource.class);

    private static final String ENTITY_NAME = "products";

    private final ProductsMapper productsMapper;
    private final ImageLinkMapper imageLinkMapper;
    private final ProductSizeMapper productSizeMapper;


    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductsService productsService;

    private final ProductsQueryService productsQueryService;

    public ProductsResource(ProductsService productsService,
                            ProductsQueryService productsQueryService,
                            ProductsMapper productsMapper,
                            ProductSizeMapper productSizeMapper,
                            ImageLinkMapper imageLinkMapper) {
        this.productsService = productsService;
        this.productsQueryService = productsQueryService;
        this.productsMapper = productsMapper;
        this.imageLinkMapper = imageLinkMapper;
        this.productSizeMapper = productSizeMapper;
    }

    /**
     * {@code POST  /products} : Create a new products.
     *
     * @param productsDTO the productsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productsDTO, or with status {@code 400 (Bad Request)} if the products has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createProducts(@Valid @RequestBody ProductsDTO productsDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save Products : {}", productsDTO);
        if (productsDTO.getId() != null) {
            throw new BadRequestAlertException("A new products cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                Products products = productsService.findByCode(productsDTO.getCode());
                if (products != null) {
                    throw new IllegalArgumentException("Mã sản phẩm đã tồn tại");
                }
                productsDTO.setUpdateTime(DateUtil.getDateC());
                productsDTO.setCost(Utils.formatCurrencyToBigDecimal(productsDTO.getCost()));
                productsService.save(productsDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /products} : Updates an existing products.
     *
     * @param productsDTO the productsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productsDTO,
     * or with status {@code 400 (Bad Request)} if the productsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateProducts(@Valid @RequestBody ProductsDTO productsDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to update Products : {}", productsDTO);
        if (productsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                Optional<Products> users = productsService.findOne(productsDTO.getId());
                Products products = users.get();
                if (!products.getCode().equals(productsDTO.getCode())) {
                    Products roles = productsService.findByCode(productsDTO.getCode());
                    if (roles != null) {
                        throw new IllegalArgumentException("Mã sản phẩm đã tồn tại");
                    }
                }
                productsDTO.setUpdateTime(DateUtil.getDateC());
                productsDTO.setCost(Utils.formatCurrencyToBigDecimal(productsDTO.getCost()));
                productsService.save(productsDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /products} : get all the products.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of products in body.
     */
    @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllProducts(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            ProductsCriteria criteria = Utils.mappingCriteria(ProductsCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Users by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Products_.ID);
            Page<Products> page = productsQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Products item : page) {
                ProductsDTO productsDTO = productsMapper.toDto(item);
                if (item.getObjects() != null) {
                    productsDTO.setParenObject(item.getObjects().getName());
                }
                if (productsDTO.getCost() != null){
                    productsDTO.setCost(Utils.formatCurrency(new BigDecimal(productsDTO.getCost())));
                }
                JSONObject lstRoles = Utils.convertEntityToJSONObject(productsDTO);
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", productsQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }


    @GetMapping(value = "/client/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getProductsClient(@RequestParam Map<String, String> paramSearch) {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            ProductsCriteria criteria = Utils.mappingCriteria(ProductsCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            LongFilter longFilter = new LongFilter();
            longFilter.setEquals(0L);
            criteria.setStatus(longFilter);
            log.debug("REST request to get Users by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Products_.ID);
            Page<Products> page = productsQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Products item : page) {
                ProductsDTO productsDTO = productsMapper.toDto(item);
                if (item.getObjects() != null) {
                    productsDTO.setParenObject(item.getObjects().getName());
                }
                if (productsDTO.getCost() != null) {
                    productsDTO.setCost(Utils.formatCurrency(new BigDecimal(productsDTO.getCost())));
                }
                JSONObject lstRoles = Utils.convertEntityToJSONObject(productsDTO);
                Set<ImageLink> dsImageLinks = item.getDsImageLinks();
                ArrayList<ImageLink> lists = Lists.newArrayList(dsImageLinks.iterator());
                List list = new ArrayList();
                for (ImageLink imageLink : lists) {
                    if (imageLink.getStatus() != 0) {
                        list.add(imageLinkMapper.toDto(imageLink));
                    }
                }
                if (dsImageLinks.isEmpty()) {
                    lstRoles.put("DS_Image", new JSONArray());
                } else {
                    lstRoles.put("DS_Image",
                        Utils.convertListEntityToJSONArray(list));
                }
                arrayResult.put(lstRoles);
            }
            JSONObject result = new JSONObject();
            result.put("count", productsQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /products/count} : count all the products.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/products/count")
    public ResponseEntity<Long> countProducts(ProductsCriteria criteria) {
        log.debug("REST request to count Products by criteria: {}", criteria);
        return ResponseEntity.ok().body(productsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /products/:id} : get the "id" products.
     *
     * @param id the id of the productsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping(value = "/products-images/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getProducts(@PathVariable Long id) {
        log.debug("REST request to get Products : {}", id);
        try {
            Optional<Products> products = productsService.findOne(id);
            if (products.orElse(null) == null) {
                return ResponseEntity.badRequest().body(
                    Utils.getStatusBadRequest("Không tìm thấy đối tượng"));
            }
            JSONObject lstRoles = Utils.convertEntityToJSONObject(products.get());
            Set<ImageLink> dsImageLinks = products.get().getDsImageLinks();
            ArrayList<ImageLink> lists = Lists.newArrayList(dsImageLinks.iterator());
            List list = new ArrayList();
            for (ImageLink imageLink : lists) {
                list.add(imageLinkMapper.toDto(imageLink));
            }
            if (dsImageLinks.isEmpty()) {
                lstRoles.put("DS_Image", new JSONArray());
            } else {
                lstRoles.put("DS_Image",
                    Utils.convertListEntityToJSONArray(list));
            }
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", lstRoles));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    @GetMapping(value = "/products-size/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getProductsSizeColor(@PathVariable Long id) {
        log.debug("REST request to get Products : {}", id);
        try {
            Optional<Products> products = productsService.findOne(id);
            if (products.orElse(null) == null) {
                return ResponseEntity.badRequest().body(
                    Utils.getStatusBadRequest("Không tìm thấy đối tượng"));
            }
            JSONObject lstRoles = Utils.convertEntityToJSONObject(products.get());
            Set<ProductSize> dsProductSizes = products.get().getDsProductSizes();
            ArrayList<ProductSize> lists = Lists.newArrayList(dsProductSizes.iterator());
            List list = new ArrayList();
            for (ProductSize productSize : lists) {
                ProductSizeDTO productSize1 = productSizeMapper.toDto(productSize);
                if (productSize.getSizes() != null) {
                    productSize1.setNameSize(productSize.getSizes().getName());
                }
                list.add(productSize1);
            }
            if (dsProductSizes.isEmpty()) {
                lstRoles.put("DS_Product_Size", new JSONArray());
            } else {
                lstRoles.put("DS_Product_Size",
                    Utils.convertListEntityToJSONArray(list));
            }
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", lstRoles));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }


    @GetMapping(value = "/products/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getProductsByCode(@PathVariable String code) {
        log.debug("REST request to get Products : {}", code);
        try {
           Products products = productsService.findByCode(code);
            if (products == null) {
                return ResponseEntity.badRequest().body(
                    Utils.getStatusBadRequest("Không tìm thấy đối tượng"));
            }
            JSONObject lstRoles = Utils.convertEntityToJSONObject(products);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", lstRoles));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code DELETE  /products/:id} : delete the "id" products.
     *
     * @param id the id of the productsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Void> deleteProducts(@PathVariable Long id) {
        log.debug("REST request to delete Products : {}", id);
        productsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package mta.ato.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import mta.ato.domain.Supplier;
import mta.ato.domain.Supplier_;
import mta.ato.service.SupplierQueryService;
import mta.ato.service.SupplierService;
import mta.ato.service.dto.SupplierCriteria;
import mta.ato.service.dto.SupplierDTO;
import mta.ato.utils.DateUtil;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.Supplier}.
 */
@RestController
@RequestMapping("/api")
public class SupplierResource {

    private final Logger log = LoggerFactory.getLogger(SupplierResource.class);

    private static final String ENTITY_NAME = "supplier";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SupplierService supplierService;

    private final SupplierQueryService supplierQueryService;

    public SupplierResource(SupplierService supplierService, SupplierQueryService supplierQueryService) {
        this.supplierService = supplierService;
        this.supplierQueryService = supplierQueryService;
    }

    /**
     * {@code POST  /suppliers} : Create a new supplier.
     *
     * @param supplierDTO the supplierDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new supplierDTO, or with status {@code 400 (Bad Request)} if the supplier has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(value = "/suppliers", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createSupplier(@Valid @RequestBody SupplierDTO supplierDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save Sizes : {}", supplierDTO);
        if (supplierDTO.getId() != null) {
            throw new BadRequestAlertException("A new sizes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                Supplier supplier = supplierService.findByCode(supplierDTO.getCode());
                if (supplier != null) {
                    throw new IllegalArgumentException("Mã nhà cung cấp đã tồn tại");
                }
                supplierDTO.setUpdateTime(DateUtil.getDateC());
                supplierService.save(supplierDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }


    /**
     * {@code PUT  /suppliers} : Updates an existing supplier.
     *
     * @param supplierDTO the supplierDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated supplierDTO,
     * or with status {@code 400 (Bad Request)} if the supplierDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the supplierDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping(value = "/suppliers", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateSupplier(@Valid @RequestBody SupplierDTO supplierDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to update Sizes : {}", supplierDTO);
        if (supplierDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                Optional<SupplierDTO> users = supplierService.findOne(supplierDTO.getId());
                SupplierDTO supplierDTO1 = users.get();
                if (!supplierDTO1.getCode().equals(supplierDTO.getCode())) {
                    Supplier supplier = supplierService.findByCode(supplierDTO.getCode());
                    if (supplier != null) {
                        throw new IllegalArgumentException("Mã nhà cung cấp đã tồn tại");
                    }
                }
                supplierDTO.setUpdateTime(DateUtil.getDateC());
                supplierService.save(supplierDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /suppliers} : get all the suppliers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of suppliers in body.
     */

    @GetMapping(value = "/suppliers", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllSuppliers(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            SupplierCriteria criteria = Utils.mappingCriteria(SupplierCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Users by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Supplier_.ID);
            Page<Supplier> page = supplierQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Supplier item : page) {
                JSONObject usersJson = Utils.convertEntityToJSONObject(item);
                arrayResult.put(usersJson);
            }
            JSONObject result = new JSONObject();
            result.put("count", supplierQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        }catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /suppliers/count} : count all the suppliers.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/suppliers/count")
    public ResponseEntity<Long> countSuppliers(SupplierCriteria criteria) {
        log.debug("REST request to count Suppliers by criteria: {}", criteria);
        return ResponseEntity.ok().body(supplierQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /suppliers/:id} : get the "id" supplier.
     *
     * @param id the id of the supplierDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the supplierDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/suppliers/{id}")
    public ResponseEntity<SupplierDTO> getSupplier(@PathVariable Long id) {
        log.debug("REST request to get Supplier : {}", id);
        Optional<SupplierDTO> supplierDTO = supplierService.findOne(id);
        return ResponseUtil.wrapOrNotFound(supplierDTO);
    }

    /**
     * {@code DELETE  /suppliers/:id} : delete the "id" supplier.
     *
     * @param id the id of the supplierDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
//    @DeleteMapping("/suppliers/{id}")
//    public ResponseEntity<Void> deleteSupplier(@PathVariable Long id) {
//        log.debug("REST request to delete Supplier : {}", id);
//        supplierService.delete(id);
//        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
//    }

    @DeleteMapping(value = "/suppliers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteSupplier(@PathVariable Long id) {
        log.debug("REST request to delete Sizes : {}", id);
        try {
            Optional<SupplierDTO> colorsDTO = supplierService.findOne(id);
            if (!colorsDTO.isPresent()) {
                throw new IllegalArgumentException("Mã nhà cung cấp không tồn tại");
            }
            supplierService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}

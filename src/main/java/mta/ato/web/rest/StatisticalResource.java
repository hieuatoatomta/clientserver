package mta.ato.web.rest;

import mta.ato.domain.ImportCoupon;
import mta.ato.service.StatisticalService;
import mta.ato.service.dto.CustomersTopProductsDTO;
import mta.ato.service.dto.StatisticsDTO;
import mta.ato.service.dto.StatisticsTopProductsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing {@link ImportCoupon}.
 */
@RestController
@RequestMapping("/api")
public class StatisticalResource {

    private final Logger log = LoggerFactory.getLogger(StatisticalResource.class);

    private static final String ENTITY_NAME = "statistical";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StatisticalService statisticalService;


    public StatisticalResource(
        StatisticalService statisticalService
    ) {

        this.statisticalService = statisticalService;
    }


    @PostMapping("/statistical/send-count")
    public ResponseEntity<Object> getSendCount(@RequestBody StatisticsDTO statisticsDTO) {
        List<StatisticsDTO> lst = statisticalService.getSendCount(statisticsDTO.getFromTime(), statisticsDTO.getToTime());
        return ResponseEntity.ok(lst);
    }

    @PostMapping("/statistical/orders-count")
    public ResponseEntity<Object> getOrdersCount(@RequestBody StatisticsDTO statisticsDTO) {
        List<StatisticsDTO> lst = statisticalService.getOrdersCount(statisticsDTO.getFromTime(), statisticsDTO.getToTime());
        return ResponseEntity.ok(lst);
    }

    @PostMapping("/statistical/doanh-thu")
    public ResponseEntity<Object> getDoanhThu(@RequestBody StatisticsDTO statisticsDTO) {
        List<StatisticsDTO> lst = statisticalService.getDoanhThu(statisticsDTO.getFromTime(), statisticsDTO.getToTime());
        return ResponseEntity.ok(lst);
    }


    @PostMapping("/statistical/top-san-pham")
    public ResponseEntity<Object> getSanPhamBanChay(@RequestBody StatisticsDTO statisticsDTO) {
        List<StatisticsTopProductsDTO> lst = statisticalService.getSanPhamBanChay(statisticsDTO.getSoLuong(), statisticsDTO.getFromTime(), statisticsDTO.getToTime());
        return ResponseEntity.ok(lst);
    }

    @PostMapping("/statistical/top-khach-hang")
    public ResponseEntity<Object> getTopKhachHang(@RequestBody StatisticsDTO statisticsDTO) {
        List<CustomersTopProductsDTO> lst = statisticalService.getTopKhachHang(statisticsDTO.getSoLuong(), statisticsDTO.getFromTime(), statisticsDTO.getToTime());
        return ResponseEntity.ok(lst);
    }
}

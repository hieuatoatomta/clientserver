package mta.ato.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import mta.ato.domain.Products;
import mta.ato.domain.Sizes;
import mta.ato.domain.Sizes_;
import mta.ato.service.ProductSizeService;
import mta.ato.service.ProductsService;
import mta.ato.service.SizesQueryService;
import mta.ato.service.SizesService;
import mta.ato.service.dto.SizesCriteria;
import mta.ato.service.dto.SizesDTO;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.Sizes}.
 */
@RestController
@RequestMapping("/api")
public class SizesResource {

    private final Logger log = LoggerFactory.getLogger(SizesResource.class);

    private static final String ENTITY_NAME = "sizes";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SizesService sizesService;
    private final ProductSizeService productSizeService;
    private final ProductsService productsService;

    private final SizesQueryService sizesQueryService;

    public SizesResource(SizesService sizesService,
                         SizesQueryService sizesQueryService,
                         ProductsService productsService,
                         ProductSizeService productSizeService) {
        this.sizesService = sizesService;
        this.sizesQueryService = sizesQueryService;
        this.productSizeService = productSizeService;
        this.productsService = productsService;
    }

    /**
     * {@code POST  /sizes} : Create a new sizes.
     *
     * @param sizesDTO the sizesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sizesDTO, or with status {@code 400 (Bad Request)} if the sizes has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(value = "/sizes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createSizes(@Valid @RequestBody SizesDTO sizesDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save Sizes : {}", sizesDTO);
        if (sizesDTO.getId() != null) {
            throw new BadRequestAlertException("A new sizes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                Sizes sizes = sizesService.findByCode(sizesDTO.getCode());
                if (sizes != null){
                    throw new IllegalArgumentException("Mã size đã tồn tại");
                }
                sizesService.save(sizesDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /sizes} : Updates an existing sizes.
     *
     * @param sizesDTO the sizesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sizesDTO,
     * or with status {@code 400 (Bad Request)} if the sizesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sizesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping(value = "/sizes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateSizes(@Valid @RequestBody SizesDTO sizesDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to update Sizes : {}", sizesDTO);
        if (sizesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                Optional<Sizes> users = sizesService.findOne(sizesDTO.getId());
                Sizes sizesDTO1 = users.get();
                if (!sizesDTO1.getCode().equals(sizesDTO.getCode())) {
                    Sizes roles = sizesService.findByCode(sizesDTO.getCode());
                    if (roles != null) {
                        throw new IllegalArgumentException("Mã quyền đã tồn tại");
                    }
                }
                sizesService.save(sizesDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /sizes} : get all the sizes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sizes in body.
     */

    @GetMapping(value = "/sizes", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllSizes(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            SizesCriteria criteria = Utils.mappingCriteria(SizesCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Users by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Sizes_.ID);
            Page<Sizes> page = sizesQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Sizes item : page) {
                JSONObject usersJson = Utils.convertEntityToJSONObject(item);
                arrayResult.put(usersJson);
            }
            JSONObject result = new JSONObject();
            result.put("count", sizesQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        }catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /sizes/count} : count all the sizes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/sizes/count")
    public ResponseEntity<Long> countSizes(SizesCriteria criteria) {
        log.debug("REST request to count Sizes by criteria: {}", criteria);
        return ResponseEntity.ok().body(sizesQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /sizes/:id} : get the "id" sizes.
     *
     * @param id the id of the sizesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sizesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping(value = "/sizes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getSizes(@PathVariable String id) {
        try {
            Products products = productsService.findByCode(id);
            log.debug("REST request to get Sizes : {}", id);
            List<Sizes> sizes = sizesService.searchSize(products.getId());
            JSONObject result = new JSONObject();
            result.put("list", sizes);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));

        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }

    }

    /**
     * {@code DELETE  /sizes/:id} : delete the "id" sizes.
     *
     * @param id the id of the sizesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(value = "/sizes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteSizes(@PathVariable Long id) {
        log.debug("REST request to delete Sizes : {}", id);
        try {
            Optional<Sizes> colorsDTO = sizesService.findOne(id);
            if (!colorsDTO.isPresent()) {
                throw new IllegalArgumentException("Mã sizes không tồn tại");
            }
            sizesService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}

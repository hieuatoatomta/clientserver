package mta.ato.web.rest;

import mta.ato.service.DetailedOrdersService;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.dto.DetailedOrdersDTO;
import mta.ato.service.dto.DetailedOrdersCriteria;
import mta.ato.service.DetailedOrdersQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.DetailedOrders}.
 */
@RestController
@RequestMapping("/api")
public class DetailedOrdersResource {

    private final Logger log = LoggerFactory.getLogger(DetailedOrdersResource.class);

    private static final String ENTITY_NAME = "detailedOrders";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailedOrdersService detailedOrdersService;

    private final DetailedOrdersQueryService detailedOrdersQueryService;

    public DetailedOrdersResource(DetailedOrdersService detailedOrdersService, DetailedOrdersQueryService detailedOrdersQueryService) {
        this.detailedOrdersService = detailedOrdersService;
        this.detailedOrdersQueryService = detailedOrdersQueryService;
    }

    /**
     * {@code POST  /detailed-orders} : Create a new detailedOrders.
     *
     * @param detailedOrdersDTO the detailedOrdersDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new detailedOrdersDTO, or with status {@code 400 (Bad Request)} if the detailedOrders has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/detailed-orders")
    public ResponseEntity<DetailedOrdersDTO> createDetailedOrders(@RequestBody DetailedOrdersDTO detailedOrdersDTO) throws URISyntaxException {
        log.debug("REST request to save DetailedOrders : {}", detailedOrdersDTO);
        if (detailedOrdersDTO.getId() != null) {
            throw new BadRequestAlertException("A new detailedOrders cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DetailedOrdersDTO result = detailedOrdersService.save(detailedOrdersDTO);
        return ResponseEntity.created(new URI("/api/detailed-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /detailed-orders} : Updates an existing detailedOrders.
     *
     * @param detailedOrdersDTO the detailedOrdersDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailedOrdersDTO,
     * or with status {@code 400 (Bad Request)} if the detailedOrdersDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the detailedOrdersDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/detailed-orders")
    public ResponseEntity<DetailedOrdersDTO> updateDetailedOrders(@RequestBody DetailedOrdersDTO detailedOrdersDTO) throws URISyntaxException {
        log.debug("REST request to update DetailedOrders : {}", detailedOrdersDTO);
        if (detailedOrdersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DetailedOrdersDTO result = detailedOrdersService.save(detailedOrdersDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, detailedOrdersDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /detailed-orders} : get all the detailedOrders.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailedOrders in body.
     */
    @GetMapping("/detailed-orders")
    public ResponseEntity<List<DetailedOrdersDTO>> getAllDetailedOrders(DetailedOrdersCriteria criteria, Pageable pageable) {
        log.debug("REST request to get DetailedOrders by criteria: {}", criteria);
        Page<DetailedOrdersDTO> page = detailedOrdersQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /detailed-orders/count} : count all the detailedOrders.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/detailed-orders/count")
    public ResponseEntity<Long> countDetailedOrders(DetailedOrdersCriteria criteria) {
        log.debug("REST request to count DetailedOrders by criteria: {}", criteria);
        return ResponseEntity.ok().body(detailedOrdersQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /detailed-orders/:id} : get the "id" detailedOrders.
     *
     * @param id the id of the detailedOrdersDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailedOrdersDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/detailed-orders/{id}")
    public ResponseEntity<DetailedOrdersDTO> getDetailedOrders(@PathVariable Long id) {
        log.debug("REST request to get DetailedOrders : {}", id);
        Optional<DetailedOrdersDTO> detailedOrdersDTO = detailedOrdersService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailedOrdersDTO);
    }

    /**
     * {@code DELETE  /detailed-orders/:id} : delete the "id" detailedOrders.
     *
     * @param id the id of the detailedOrdersDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/detailed-orders/{id}")
    public ResponseEntity<Void> deleteDetailedOrders(@PathVariable Long id) {
        log.debug("REST request to delete DetailedOrders : {}", id);
        detailedOrdersService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

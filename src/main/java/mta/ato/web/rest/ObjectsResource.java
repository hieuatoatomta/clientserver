package mta.ato.web.rest;

import io.github.jhipster.web.util.PaginationUtil;
import mta.ato.config.Constants;
import mta.ato.domain.Objects;
import mta.ato.domain.Objects_;
import mta.ato.service.ObjectsQueryService;
import mta.ato.service.ObjectsService;
import mta.ato.service.RolesService;
import mta.ato.service.dto.ObjectsCriteria;
import mta.ato.service.dto.ObjectsDTO;
import mta.ato.service.dto.RolesDTO;
import mta.ato.utils.DateUtil;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.Objects}.
 */
@RestController
@RequestMapping("/api")
public class ObjectsResource {

    private final Logger log = LoggerFactory.getLogger(ObjectsResource.class);

    private static final String ENTITY_NAME = "objects";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ObjectsService objectsService;

    private final ObjectsQueryService objectsQueryService;
    private final RolesService rolesService;

    public ObjectsResource(ObjectsService objectsService, ObjectsQueryService objectsQueryService,
                           RolesService rolesService) {
        this.objectsService = objectsService;
        this.objectsQueryService = objectsQueryService;
        this.rolesService = rolesService;
    }

    /**
     * {@code POST  /objects} : Create a new objects.
     *
     * @param objectsDTO the objectsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new objectsDTO, or with status {@code 400 (Bad Request)} if the objects has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(value = "/objects",  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createObjects(@Valid @RequestBody ObjectsDTO objectsDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save Objects : {}", objectsDTO);
        if (objectsDTO.getId() != null) {
            throw new BadRequestAlertException("A new objects cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                Objects objects = objectsService.findByCode(objectsDTO.getCode());
                if (objects != null) {
                    throw new IllegalArgumentException("Mã chức năng đã tồn tại");
                }
                objectsDTO.setUpdateTime(DateUtil.getDateC());
                objectsService.save(objectsDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /objects} : Updates an existing objects.
     *
     * @param objectsDTO the objectsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated objectsDTO,
     * or with status {@code 400 (Bad Request)} if the objectsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the objectsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping(value = "/objects", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateObjects(@Valid @RequestBody ObjectsDTO objectsDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to update Objects : {}", objectsDTO);
        if (objectsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                Optional<ObjectsDTO> users = objectsService.findOne(objectsDTO.getId());
                ObjectsDTO products = users.get();
                if (!products.getCode().equals(objectsDTO.getCode())) {
                    Objects roles = objectsService.findByCode(objectsDTO.getCode());
                    if (roles != null) {
                        throw new IllegalArgumentException("Mã chức năng đã tồn tại");
                    }
                }
                objectsDTO.setUpdateTime(DateUtil.getDateC());
                objectsService.save(objectsDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /objects} : get all the objects.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of objects in body.
     */
    @GetMapping(value = "/objects", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllObjects(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            ObjectsCriteria criteria = Utils.mappingCriteria(ObjectsCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Objects by criteria: {}", criteria);
            Pageable pageable = null;
            if (!reqParamObj.isNull("page") && !ObjectUtils.isEmpty(reqParamObj.get("page")) &&
                !reqParamObj.isNull("page_size") && !ObjectUtils.isEmpty(reqParamObj.get("page_size"))
            ) {
                pageable = PageRequest.of(reqParamObj.getInt("page"), reqParamObj.getInt("page_size"), Sort.by(Objects_.ID));
            } else {
                pageable =  PageRequest.of(0, 200, Sort.by(Objects_.ID));
            }
            Page<Objects> page = objectsQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Objects item : page) {
                JSONObject usersJson = Utils.convertEntityToJSONObject(item);
                arrayResult.put(usersJson);
            }
            JSONObject result = new JSONObject();
            result.put("count", objectsQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        }catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /objects/count} : count all the objects.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/objects/count")
    public ResponseEntity<Long> countObjects(ObjectsCriteria criteria) {
        log.debug("REST request to count Objects by criteria: {}", criteria);
        return ResponseEntity.ok().body(objectsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /objects/:id} : get the "id" objects.
     *
     * @param id the id of the objectsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the objectsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/objects/{id}")
    public ResponseEntity<Object> getObjects(@PathVariable Long id) {
        try {
            Optional<RolesDTO> rolesDTO = rolesService.findOne(id);
            Long type = rolesDTO.get().getType();
            return ResponseEntity.status(200).body(Collections.singletonMap(Constants.DATA, objectsService.getAllObjRoleAction(id,type)));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(400).body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code DELETE  /objects/:id} : delete the "id" objects.
     *
     * @param id the id of the objectsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(value = "/objects/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteObjects(@PathVariable Long id) {
        try {
            Optional<ObjectsDTO> usersDTO = objectsService.findOne(id);
            if (!usersDTO.isPresent()) {
                throw new IllegalArgumentException("Chức năng không tồn tại");
            }
            objectsService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
        } catch (Exception e){
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
//        log.debug("REST request to delete Objects : {}", id);
//        objectsService.delete(id);
//        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

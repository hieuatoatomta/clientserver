package mta.ato.web.rest;

import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.PaginationUtil;
import mta.ato.config.Constants;
import mta.ato.domain.UserRole;
import mta.ato.domain.Users;
import mta.ato.domain.Users_;
import mta.ato.service.UserRoleService;
import mta.ato.service.UsersQueryService;
import mta.ato.service.UsersService;
import mta.ato.service.dto.UsersCriteria;
import mta.ato.service.dto.UsersDTO;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.apache.commons.compress.utils.Lists;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing {@link mta.ato.domain.Users}.
 */
@RestController
@RequestMapping("/api")
public class UsersResource {

    private final Logger log = LoggerFactory.getLogger(UsersResource.class);

    private static final String ENTITY_NAME = "users";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UsersService usersService;

    private final UsersQueryService usersQueryService;

    private final UserRoleService userRoleService;


    public UsersResource(UsersService usersService, UsersQueryService usersQueryService,
                         UserRoleService userRoleService) {
        this.usersService = usersService;
        this.usersQueryService = usersQueryService;
        this.userRoleService = userRoleService;
    }

    /**
     * {@code POST  /users} : Create a new users.
     *
     * @param usersDTO the usersDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new usersDTO, or with status {@code 400 (Bad Request)} if the users has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createUsers(@Valid @RequestBody UsersDTO usersDTO, BindingResult result) {
        log.debug("REST request to save Users : {}", usersDTO);
        if (usersDTO.getId() != null) {
            throw new BadRequestAlertException("A new users cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                usersService.save(usersDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code PUT  /users} : Updates an existing users.
     *
     * @param usersDTO the usersDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated usersDTO,
     * or with status {@code 400 (Bad Request)} if the usersDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the usersDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateUsers(@Valid @RequestBody UsersDTO usersDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to update Users : {}", usersDTO);
        if (usersDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                usersService.update(usersDTO);
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /users} : get all the users.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of users in body.
     */
    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllUsers(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            UsersCriteria criteria = Utils.mappingCriteria(UsersCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Users by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, Users_.ID);
            Page<Users> page = usersQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (Users item : page) {
                item.setPass(null);
                JSONObject usersJson = Utils.convertEntityToJSONObject(item);
                arrayResult.put(usersJson);
            }
            JSONObject result = new JSONObject();
            result.put("count", usersQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        }catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }

    }

    /**
     * {@code GET  /users/count} : count all the users.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/users/count")
    public ResponseEntity<Long> countUsers(UsersCriteria criteria) {
        log.debug("REST request to count Users by criteria: {}", criteria);
        return ResponseEntity.ok().body(usersQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /users/:id} : get the "id" users.
     *
     * @param id the id of the usersDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the usersDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping(value = "/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getUsers(@PathVariable Long id) {
        log.debug("REST request to get Users : {}", id);
        UsersCriteria criteria = new UsersCriteria();

        LongFilter maUser = new LongFilter();
        maUser.setEquals(id);
        criteria.setId(maUser);

        Optional<Users> users = usersQueryService.findOneByCriteria(criteria);
        if (users.orElse(null) == null) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Không tìm thấy đối tượng"));
        }
        JSONObject result = new JSONObject();
        Users users1 = users.get();
        users1.setPass(null);
        result.put(Constants.DATA, Utils.convertEntityToJSONObject(users1));
        Set<UserRole> dsUserRoles = users1.getDsUserRoles();
        ArrayList<UserRole> lists = Lists.newArrayList(dsUserRoles.iterator());
        List list = new ArrayList();
        for (int i =0; i < lists.size(); i++){
            list.add(lists.get(i).getRoles().getId());
        }
        if (dsUserRoles.isEmpty() || list.isEmpty()) {
            result.put("DS_ROLES", new JSONArray());
        } else {
            result.put("DS_ROLES",
                list);
        }
        return ResponseEntity.status(HttpStatus.OK).body(result.toString());
    }

    /**
     * {@code DELETE  /users/:id} : delete the "id" users.
     *
     * @param id the id of the usersDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping(value = "/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteUsers(@PathVariable Long id) {
        try {
            log.debug("REST request to delete Users : {}", id);
            Optional<UsersDTO> usersDTO = usersService.findOne(id);
            if (!usersDTO.isPresent()) {
                throw new IllegalArgumentException("Nguời dùng không tồn tại");
            }
            userRoleService.deleteAllByUsersId(id);
            usersService.delete(id);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null).toString());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong xoá: " + e.getMessage()));
        }
    }
}

package mta.ato.web.rest;

import com.sun.mail.imap.protocol.Item;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import mta.ato.config.CustomUserDetails;
import mta.ato.domain.*;
import mta.ato.service.*;
import mta.ato.service.dto.*;
import mta.ato.service.mapper.DetailedImportCouponMapper;
import mta.ato.service.mapper.ImportCouponMapper;
import mta.ato.utils.DateUtil;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link mta.ato.domain.ImportCoupon}.
 */
@RestController
@RequestMapping("/api")
public class ImportCouponResource {

    private final Logger log = LoggerFactory.getLogger(ImportCouponResource.class);

    private static final String ENTITY_NAME = "importCoupon";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ImportCouponService importCouponService;

    private final ImportCouponMapper importCouponMapper;

    private final DetailedImportCouponService detailedImportCouponService;

    private final ProductSizeService productSizeService;

    private final ProductsService productsService;

    private final ImportCouponQueryService importCouponQueryService;

    private final ProductSizeQueryService productSizeQueryService;

    private final DetailedImportCouponMapper detailedImportCouponMapper;


    public ImportCouponResource(ImportCouponService importCouponService,
                                ImportCouponQueryService importCouponQueryService,
                                DetailedImportCouponService detailedImportCouponService,
                                ProductsService productsService,
                                ProductSizeService productSizeService,
                                ProductSizeQueryService productSizeQueryService,
                                DetailedImportCouponMapper detailedImportCouponMapper,
                                ImportCouponMapper importCouponMapper) {
        this.importCouponService = importCouponService;
        this.importCouponQueryService = importCouponQueryService;
        this.detailedImportCouponService = detailedImportCouponService;
        this.productsService = productsService;
        this.importCouponMapper = importCouponMapper;
        this.productSizeService = productSizeService;
        this.productSizeQueryService = productSizeQueryService;
        this.detailedImportCouponMapper = detailedImportCouponMapper;
    }

    /**
     * {@code POST  /import-coupons} : Create a new importCoupon.
     *
     * @param importCouponDTO the importCouponDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new importCouponDTO, or with status {@code 400 (Bad Request)} if the importCoupon has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */

    @PostMapping(value = "/import-coupons", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> createImportCoupon(@Valid @RequestBody ImportCouponDTO importCouponDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to save importCoupon : {}", importCouponDTO);
        if (importCouponDTO.getId() != null) {
            throw new BadRequestAlertException("A new importCoupon cannot already have an ID", ENTITY_NAME, "idexists");
        }
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body(result.getFieldError());
            } else {
                CustomUserDetails principal = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                importCouponDTO.setCode(RandomStringUtils.randomNumeric(4) + DateUtil.getDateToString());
                importCouponDTO.setUpdateTime(DateUtil.getDateC());
                importCouponDTO.setUsersId(principal.getUser().getId());
                importCouponService.save(importCouponDTO);
                ImportCoupon importCoupon = importCouponService.findByCode(importCouponDTO.getCode());
                if (!importCouponDTO.getImportCustomDTOList().isEmpty()) {
                    for (int i = 0; i < importCouponDTO.getImportCustomDTOList().size(); i++) {
                        Products products = productsService.findByCode(importCouponDTO.getImportCustomDTOList().get(i).getCode());
                        if (products != null) {
                            // th1: Sản phẩm đã tồn tại
                            // b1: Thêm vào chi tiết phiếu nhập
                            // b2: Cập nhật lại bảng productsSize để cập nhật số lượng
                            // b2.2: Nếu sản phẩm đã tồn tại ( idprodcut vs idSize) trong bảng producst thì cập nhật số lượng
                            DetailedImportCouponDTO detailedImportCouponDTO = new DetailedImportCouponDTO();
                            detailedImportCouponDTO.setImportPrice(Utils.formatCurrencyToBigDecimal(importCouponDTO.getImportCustomDTOList().get(i).getImportPrice()));
                            detailedImportCouponDTO.setAmount(importCouponDTO.getImportCustomDTOList().get(i).getAmount());
                            detailedImportCouponDTO.setSizesId(importCouponDTO.getImportCustomDTOList().get(i).getIdSize());
                            detailedImportCouponDTO.setUpdateTime(DateUtil.getDateC());
                            detailedImportCouponDTO.setImportCouponId(importCoupon.getId());
                            detailedImportCouponDTO.setProductsId(products.getId());
                            detailedImportCouponService.save(detailedImportCouponDTO);

                            ProductSizeCriteria ProductSizeCriteria = new ProductSizeCriteria();
                            if (!products.getId().equals(null)) {
                                LongFilter longFilter = new LongFilter();
                                longFilter.setEquals(products.getId());
                                ProductSizeCriteria.setProductsId(longFilter);
                            }
                            if (!importCouponDTO.getImportCustomDTOList().get(i).getIdSize().equals(null)) {
                                LongFilter longFilter = new LongFilter();
                                longFilter.setEquals(importCouponDTO.getImportCustomDTOList().get(i).getIdSize());
                                ProductSizeCriteria.setSizesId(longFilter);
                            }
                            List<ProductSizeDTO> page = productSizeQueryService.findByCriteria(ProductSizeCriteria);
                            if (page.size() == 1) {
                                page.get(0).setAmount(importCouponDTO.getImportCustomDTOList().get(i).getAmount() + page.get(0).getAmount());
                                productSizeService.save(page.get(0));
                            } else if (page.size() == 0) {
                                ProductSizeDTO productSizeDTO = new ProductSizeDTO();
                                productSizeDTO.setProductsId(products.getId());
                                productSizeDTO.setSizesId(importCouponDTO.getImportCustomDTOList().get(i).getIdSize());
                                productSizeDTO.setAmount(importCouponDTO.getImportCustomDTOList().get(i).getAmount());
                                productSizeService.save(productSizeDTO);
                            } else {
                                throw new IllegalArgumentException("Có lỗi xảy ra trong quá trình thêm mới");
                            }
                        } else {
                            // th2: Sản phẩm chưa tồn tại
                            // b1: Thêm mới sản phẩm
                            // b2: Thêm vào chi tiết phíếu nhập
                            // b3: Cập nhật vào bảng productsize de cập nhật số lượng sản phẩm
                            ProductsDTO productsDTO = new ProductsDTO();
                            productsDTO.setCode(importCouponDTO.getImportCustomDTOList().get(i).getCode());
                            productsDTO.setName(importCouponDTO.getImportCustomDTOList().get(i).getName());
                            productsDTO.setStatus(4L);
                            productsDTO.setUpdateTime(DateUtil.getDateC());
                            productsDTO.setObjectsId(importCouponDTO.getImportCustomDTOList().get(i).getIdObjects());
                            productsService.save(productsDTO);

                            Products products1 = productsService.findByCode(importCouponDTO.getImportCustomDTOList().get(i).getCode());
                            if (products1 != null) {
                                DetailedImportCouponDTO detailedImportCouponDTO = new DetailedImportCouponDTO();
                                detailedImportCouponDTO.setImportPrice(Utils.formatCurrencyToBigDecimal(importCouponDTO.getImportCustomDTOList().get(i).getImportPrice()));
                                detailedImportCouponDTO.setAmount(importCouponDTO.getImportCustomDTOList().get(i).getAmount());
                                detailedImportCouponDTO.setSizesId(importCouponDTO.getImportCustomDTOList().get(i).getIdSize());
                                detailedImportCouponDTO.setUpdateTime(DateUtil.getDateC());
                                detailedImportCouponDTO.setImportCouponId(importCoupon.getId());
                                detailedImportCouponDTO.setProductsId(products1.getId());
                                detailedImportCouponService.save(detailedImportCouponDTO);

                                ProductSizeDTO productSizeDTO = new ProductSizeDTO();
                                productSizeDTO.setProductsId(products1.getId());
                                productSizeDTO.setSizesId(importCouponDTO.getImportCustomDTOList().get(i).getIdSize());
                                productSizeDTO.setAmount(importCouponDTO.getImportCustomDTOList().get(i).getAmount());
                                productSizeService.save(productSizeDTO);
                            }
                        }
                    }
                }
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body(Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    private void writeHeaderLine(XSSFSheet sheet) {

        Row headerRow = sheet.createRow(0);

        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellValue("Course Name");

        headerCell = headerRow.createCell(1);
        headerCell.setCellValue("Student Name");

        headerCell = headerRow.createCell(2);
        headerCell.setCellValue("Timestamp");

        headerCell = headerRow.createCell(3);
        headerCell.setCellValue("Rating");

        headerCell = headerRow.createCell(4);
        headerCell.setCellValue("Comment");
    }


    @PutMapping("/import-coupons/export-data")
    public ResponseEntity<ImportCouponDTO> ExportDataImportCoupon(@Valid @RequestBody ImportCouponDTO importCouponDTO) throws URISyntaxException {
        log.debug("REST request to update ImportCoupon : {}", importCouponDTO);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Reviews");

        writeHeaderLine(sheet);
//        File fileOut = configReportService.exportDataConfigReports(configReportDataForm);
//
//        FileOutputStream outputStream = new FileOutputStream(excelFilePath);
//        workbook.write(outputStream);
//        workbook.close();


        if (importCouponDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ImportCouponDTO result = importCouponService.save(importCouponDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, importCouponDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /import-coupons} : Updates an existing importCoupon.
     *
     * @param importCouponDTO the importCouponDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated importCouponDTO,
     * or with status {@code 400 (Bad Request)} if the importCouponDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the importCouponDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/import-coupons")
    public ResponseEntity<ImportCouponDTO> updateImportCoupon(@Valid @RequestBody ImportCouponDTO importCouponDTO) throws URISyntaxException {
        log.debug("REST request to update ImportCoupon : {}", importCouponDTO);
        if (importCouponDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ImportCouponDTO result = importCouponService.save(importCouponDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, importCouponDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /import-coupons} : get all the importCoupons.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of importCoupons in body.
     */

    @GetMapping(value = "/import-coupons", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllImportCoupons(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            ImportCouponCriteria criteria = Utils.mappingCriteria(ImportCouponCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Users by criteria: {}", criteria);
            Pageable pageable = Utils.getPage(reqParamObj, ImportCoupon_.ID);
            Page<ImportCoupon> page = importCouponQueryService.findByCriteria(criteria, pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            JSONArray arrayResult = new JSONArray();
            for (ImportCoupon item : page) {
                ImportCouponDTO importCouponDTO = importCouponMapper.toDto(item);
                if (item.getSupplier() != null) {
                    importCouponDTO.setNameSupplier(item.getSupplier().getName());
                }
                JSONObject usersJson = Utils.convertEntityToJSONObject(importCouponDTO);
                arrayResult.put(usersJson);
            }
            JSONObject result = new JSONObject();
            result.put("count", importCouponQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().headers(headers).body(Utils.getStatusOk("Thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    @GetMapping(value = "/detail-import-coupons/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getDetailedImport(@PathVariable Long id) {
        log.debug("REST request to get Products : {}", id);
        try {
            Optional<ImportCoupon> importCoupon = importCouponService.findOne(id);
            List<DetailedImportCoupon> detailedImportCouponList = detailedImportCouponService.findById(importCoupon.get());
//            List list = new ArrayList();
//            for (DetailedImportCoupon detailedImportDTO : detailedImportCouponList) {
//                list.add(detailedImportCouponMapper.toDto(detailedImportDTO));
//            }
            List<Object> list1 = new ArrayList<>();
            for (int i = 0; i < detailedImportCouponList.size(); i++){
                List<Object> arr = new ArrayList<>();
                arr.add(detailedImportCouponList.get(i).getId());
                if(detailedImportCouponList.get(i).getProducts() != null){
                    arr.add(detailedImportCouponList.get(i).getProducts().getCode());
                    arr.add(detailedImportCouponList.get(i).getProducts().getName());
                } else {
                    arr.add(null);
                    arr.add(null);
                }
                if (detailedImportCouponList.get(i).getImportPrice() != null){
                    arr.add( detailedImportCouponList.get(i).getImportPrice());
                }else {
                    arr.add(null);
                }
                if (detailedImportCouponList.get(i).getProducts() != null){
                    arr.add( detailedImportCouponList.get(i).getProducts().getObjects().getId());
                }else {
                    arr.add(null);
                }
                if (detailedImportCouponList.get(i).getSizes() != null){
                    arr.add( detailedImportCouponList.get(i).getSizes().getId());
                }else {
                    arr.add(null);
                }
                if (detailedImportCouponList.get(i).getAmount() != null){
                    arr.add( detailedImportCouponList.get(i).getAmount());
                }else {
                    arr.add(null);
                }
                list1.add(arr);

            }
            return ResponseEntity.ok().body(list1);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /import-coupons/count} : count all the importCoupons.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/import-coupons/count")
    public ResponseEntity<Long> countImportCoupons(ImportCouponCriteria criteria) {
        log.debug("REST request to count ImportCoupons by criteria: {}", criteria);
        return ResponseEntity.ok().body(importCouponQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /import-coupons/:id} : get the "id" importCoupon.
     *
     * @param id the id of the importCouponDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the importCouponDTO, or with status {@code 404 (Not Found)}.
     */
//    @GetMapping("/import-coupons/{id}")
//    public ResponseEntity<ImportCouponDTO> getImportCoupon(@PathVariable Long id) {
//        log.debug("REST request to get ImportCoupon : {}", id);
//        Optional<ImportCoupon> importCouponDTO = importCouponService.findOne(id);
//        return ResponseUtil.wrapOrNotFound(importCouponDTO);
//    }

    /**
     * {@code DELETE  /import-coupons/:id} : delete the "id" importCoupon.
     *
     * @param id the id of the importCouponDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/import-coupons/{id}")
    public ResponseEntity<Void> deleteImportCoupon(@PathVariable Long id) {
        log.debug("REST request to delete ImportCoupon : {}", id);
        importCouponService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package mta.ato.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import mta.ato.service.ObjectActionQueryService;
import mta.ato.service.ObjectActionService;
import mta.ato.service.dto.ObjectActionCriteria;
import mta.ato.service.dto.ObjectActionDTO;
import mta.ato.utils.JsonUtils;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.ObjectAction}.
 */
@RestController
@RequestMapping("/api")
public class ObjectActionResource {

    private final Logger log = LoggerFactory.getLogger(ObjectActionResource.class);

    private static final String ENTITY_NAME = "objectAction";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ObjectActionService objectActionService;

    private final ObjectActionQueryService objectActionQueryService;

    public ObjectActionResource(ObjectActionService objectActionService, ObjectActionQueryService objectActionQueryService) {
        this.objectActionService = objectActionService;
        this.objectActionQueryService = objectActionQueryService;
    }

    /**
     * {@code POST  /object-actions} : Create a new objectAction.
     *
     * @param objectActionDTO the objectActionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new objectActionDTO, or with status {@code 400 (Bad Request)} if the objectAction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/object-actions")
    public ResponseEntity<ObjectActionDTO> createObjectAction(@RequestBody ObjectActionDTO objectActionDTO) throws URISyntaxException {
        log.debug("REST request to save ObjectAction : {}", objectActionDTO);
        if (objectActionDTO.getId() != null) {
            throw new BadRequestAlertException("A new objectAction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ObjectActionDTO result = objectActionService.save(objectActionDTO);
        return ResponseEntity.created(new URI("/api/object-actions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /object-actions} : Updates an existing objectAction.
     *
     * @param objectActionDTO the objectActionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated objectActionDTO,
     * or with status {@code 400 (Bad Request)} if the objectActionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the objectActionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/object-actions")
    public ResponseEntity<ObjectActionDTO> updateObjectAction(@RequestBody ObjectActionDTO objectActionDTO) throws URISyntaxException {
        log.debug("REST request to update ObjectAction : {}", objectActionDTO);
        if (objectActionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ObjectActionDTO result = objectActionService.save(objectActionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, objectActionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /object-actions} : get all the objectActions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of objectActions in body.
     */
//    @GetMapping("/object-actions")
//    public ResponseEntity<List<ObjectActionDTO>> getAllObjectActions(ObjectActionCriteria criteria, Pageable pageable) {
//        log.debug("REST request to get ObjectActions by criteria: {}", criteria);
//        Page<ObjectActionDTO> page = objectActionQueryService.findByCriteria(criteria, pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }
    @GetMapping(value = "/object-actions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getAllObjectActions(@RequestParam Map<String, String> paramSearch) throws InstantiationException, IllegalAccessException {
        try {
            JSONObject reqParamObj = JsonUtils.transferJsonKey(new JSONObject(paramSearch), true);
            ObjectActionCriteria criteria = Utils.mappingCriteria(ObjectActionCriteria.class, Utils.updateJsontoCamelCase(reqParamObj, true));
            log.debug("REST request to get Objects by criteria: {}", criteria);
            List<ObjectActionDTO> page = objectActionQueryService.findByCriteria(criteria);
            JSONArray arrayResult = new JSONArray();
            for (ObjectActionDTO item : page) {
                JSONObject usersJson = Utils.convertEntityToJSONObject(item);
                arrayResult.put(usersJson);
            }
            JSONObject result = new JSONObject();
            result.put("count", objectActionQueryService.countByCriteria(criteria));
            result.put("list", arrayResult);
            return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", result));
        }catch (Exception e) {
            return ResponseEntity.badRequest().body(
                Utils.getStatusBadRequest("Có lỗi xảy ra trong quá trình tìm kiếm " + e.getMessage()));
        }
    }

    /**
     * {@code GET  /object-actions/count} : count all the objectActions.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/object-actions/count")
    public ResponseEntity<Long> countObjectActions(ObjectActionCriteria criteria) {
        log.debug("REST request to count ObjectActions by criteria: {}", criteria);
        return ResponseEntity.ok().body(objectActionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /object-actions/:id} : get the "id" objectAction.
     *
     * @param id the id of the objectActionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the objectActionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/object-actions/{id}")
    public ResponseEntity<ObjectActionDTO> getObjectAction(@PathVariable Long id) {
        log.debug("REST request to get ObjectAction : {}", id);
        Optional<ObjectActionDTO> objectActionDTO = objectActionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(objectActionDTO);
    }

    /**
     * {@code DELETE  /object-actions/:id} : delete the "id" objectAction.
     *
     * @param id the id of the objectActionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/object-actions/{id}")
    public ResponseEntity<Void> deleteObjectAction(@PathVariable Long id) {
        log.debug("REST request to delete ObjectAction : {}", id);
        objectActionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

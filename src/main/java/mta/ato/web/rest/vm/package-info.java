/**
 * View Models used by Spring MVC REST controllers.
 */
package mta.ato.web.rest.vm;

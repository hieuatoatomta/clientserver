package mta.ato.web.rest;

import io.github.jhipster.service.filter.LongFilter;
import mta.ato.service.ProductSizeService;
import mta.ato.utils.Utils;
import mta.ato.web.rest.errors.BadRequestAlertException;
import mta.ato.service.dto.ProductSizeDTO;
import mta.ato.service.dto.ProductSizeCriteria;
import mta.ato.service.ProductSizeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link mta.ato.domain.ProductSize}.
 */
@RestController
@RequestMapping("/api")
public class ProductSizeResource {

    private final Logger log = LoggerFactory.getLogger(ProductSizeResource.class);

    private static final String ENTITY_NAME = "ProductSize";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductSizeService ProductSizeService;

    private final ProductSizeQueryService ProductSizeQueryService;

    public ProductSizeResource(ProductSizeService ProductSizeService, ProductSizeQueryService ProductSizeQueryService) {
        this.ProductSizeService = ProductSizeService;
        this.ProductSizeQueryService = ProductSizeQueryService;
    }

    /**
     * {@code POST  /product-size-colors} : Create a new ProductSize.
     *
     * @param ProductSizeDTO the ProductSizeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ProductSizeDTO, or with status {@code 400 (Bad Request)} if the ProductSize has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-size")
    public ResponseEntity<ProductSizeDTO> createProductSize(@Valid @RequestBody ProductSizeDTO ProductSizeDTO) throws URISyntaxException {
        log.debug("REST request to save ProductSize : {}", ProductSizeDTO);
        if (ProductSizeDTO.getId() != null) {
            throw new BadRequestAlertException("A new ProductSize cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductSizeDTO result = ProductSizeService.save(ProductSizeDTO);
        return ResponseEntity.created(new URI("/api/product-size-colors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-size-colors} : Updates an existing ProductSize.
     *
     * @param ProductSizeDTO the ProductSizeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ProductSizeDTO,
     * or with status {@code 400 (Bad Request)} if the ProductSizeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ProductSizeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping(value = "/product-size", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateProductSize(@Valid @RequestBody ProductSizeDTO ProductSizeDTO, BindingResult result) throws URISyntaxException {
        log.debug("REST request to update ProductSize : {}", ProductSizeDTO);
        try {
            if (result.hasErrors()) {
                return ResponseEntity.status(400).body( result.getFieldError());
            } else {
                ProductSizeCriteria ProductSizeCriteria = new ProductSizeCriteria();
                if (!ProductSizeDTO.getProductsId().equals(null)) {
                    LongFilter longFilter = new LongFilter();
                    longFilter.setEquals(ProductSizeDTO.getProductsId());
                    ProductSizeCriteria.setProductsId(longFilter);
                }
                if (!ProductSizeDTO.getSizesId().equals(null)) {
                    LongFilter longFilter = new LongFilter();
                    longFilter.setEquals(ProductSizeDTO.getSizesId());
                    ProductSizeCriteria.setSizesId(longFilter);
                }
                List<ProductSizeDTO> page = ProductSizeQueryService.findByCriteria(ProductSizeCriteria);
                if (page.size() == 1) {
                    page.get(0).setAmount(ProductSizeDTO.getAmount() + page.get(0).getAmount());
                    ProductSizeService.save(page.get(0));
                } else if (page.size() == 0) {
                    ProductSizeService.save(ProductSizeDTO);
                } else {
                    throw new IllegalArgumentException("Có lỗi xảy ra trong quá trình thêm mới");
                }
                return ResponseEntity.ok().body(Utils.getStatusOk("Thành công", null));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().body( Utils.getStatusBadRequest(e.getMessage()));
        }
    }

    /**
     * {@code GET  /product-size-colors} : get all the ProductSizes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ProductSizes in body.
     */
    @GetMapping("/product-size")
    public ResponseEntity<List<ProductSizeDTO>> getAllProductSizes(ProductSizeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ProductSizes by criteria: {}", criteria);
        Page<ProductSizeDTO> page = ProductSizeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /product-size-colors/count} : count all the ProductSizes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/product-size/count")
    public ResponseEntity<Long> countProductSizes(ProductSizeCriteria criteria) {
        log.debug("REST request to count ProductSizes by criteria: {}", criteria);
        return ResponseEntity.ok().body(ProductSizeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /product-size-colors/:id} : get the "id" ProductSize.
     *
     * @param id the id of the ProductSizeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ProductSizeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-size/{id}")
    public ResponseEntity<ProductSizeDTO> getProductSize(@PathVariable Long id) {
        log.debug("REST request to get ProductSize : {}", id);
        Optional<ProductSizeDTO> ProductSizeDTO = ProductSizeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ProductSizeDTO);
    }

    /**
     * {@code DELETE  /product-size-colors/:id} : delete the "id" ProductSize.
     *
     * @param id the id of the ProductSizeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-size/{id}")
    public ResponseEntity<Void> deleteProductSize(@PathVariable Long id) {
        log.debug("REST request to delete ProductSize : {}", id);
        ProductSizeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

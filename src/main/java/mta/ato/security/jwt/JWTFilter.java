package mta.ato.security.jwt;

import mta.ato.config.*;
import mta.ato.domain.SysLogs;
import mta.ato.security.UserDetailsServiceImpl;
import mta.ato.service.dto.UsersDTO;
import mta.ato.utils.DateUtil;
import mta.ato.utils.Translator;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.service.spi.ServiceException;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.ZonedDateTime;

public class JWTFilter extends OncePerRequestFilter {
    private final Logger log = LoggerFactory.getLogger(JWTFilter.class);

    public static final String AUTHORIZATION_HEADER = "Authorization";

    UserDetailsServiceImpl jwtUserDetailsService1 = ApplicationContextHolder.getContext().getBean(UserDetailsServiceImpl.class);
    CustomAuthenticationProvider authenticationProvider = ApplicationContextHolder.getContext().getBean(CustomAuthenticationProvider.class);
    LogConfig logConfig = ApplicationContextHolder.getContext().getBean(LogConfig.class);
    private TokenProvider tokenProvider;

    public JWTFilter(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Override
    protected void initFilterBean() throws ServletException {
        super.initFilterBean();
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
        throws ServletException, IOException, ServiceException {
        ZonedDateTime dateTime = DateUtil.getDateC();
        log.info("url:" + request.getRequestURI());
        final String requestTokenHeader = request.getHeader(JWTFilter.AUTHORIZATION_HEADER);
        String username = null;
        String jwtToken = null;
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(requestTokenHeader) && requestTokenHeader.startsWith(Constants.BEARER)) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = tokenProvider.getAuthentication(jwtToken);
            } catch (Exception e) {
                logger.info(e.getMessage(), e);
                response.setContentType("application/json;charset=UTF-8");
                response.sendError(401, Encode.forHtml(Translator.toLocale(Constants.URL_PART_FALSE)));
                return;
            }
        }

        if (StringUtils.isNotEmpty(username) && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = jwtUserDetailsService1.loadUserByUsername(username);
            CustomUserDetails customUserDetails = (CustomUserDetails) userDetails;
            UsersDTO userDTO = customUserDetails.getUser();

            if (tokenProvider.validateToken(jwtToken, userDetails)) {
                String refreshToken = tokenProvider.generateToken(userDetails);
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDTO, userDTO.getPass());
                usernamePasswordAuthenticationToken
                    .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                Authentication authentication = authenticationProvider
                    .authenticate(usernamePasswordAuthenticationToken);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                response.setHeader(JWTFilter.AUTHORIZATION_HEADER, Constants.BEARER + refreshToken);
            }
        }
        chain.doFilter(request, response);

        // sau khi thuc hien xong request thi ghi lai log
        if(!request.getRequestURI().equals("/api/sys-logs") && !request.getRequestURI().equals("/api/authenticate")){
            SysLogs logs1 = new SysLogs(dateTime, request, username, response);
            logConfig.submit(logs1);
        }
    }

}

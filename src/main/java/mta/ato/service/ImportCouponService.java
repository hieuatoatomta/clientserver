package mta.ato.service;

import mta.ato.domain.ImportCoupon;
import mta.ato.domain.Sizes;
import mta.ato.repository.ImportCouponRepository;
import mta.ato.service.dto.ImportCouponDTO;
import mta.ato.service.mapper.ImportCouponMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ImportCoupon}.
 */
@Service
@Transactional
public class ImportCouponService {

    private final Logger log = LoggerFactory.getLogger(ImportCouponService.class);

    private final ImportCouponRepository importCouponRepository;

    private final ImportCouponMapper importCouponMapper;

    public ImportCouponService(ImportCouponRepository importCouponRepository, ImportCouponMapper importCouponMapper) {
        this.importCouponRepository = importCouponRepository;
        this.importCouponMapper = importCouponMapper;
    }

    /**
     * Save a importCoupon.
     *
     * @param importCouponDTO the entity to save.
     * @return the persisted entity.
     */
    public ImportCouponDTO save(ImportCouponDTO importCouponDTO) {
        log.debug("Request to save ImportCoupon : {}", importCouponDTO);
        ImportCoupon importCoupon = importCouponMapper.toEntity(importCouponDTO);
        importCoupon = importCouponRepository.save(importCoupon);
        return importCouponMapper.toDto(importCoupon);
    }

    /**
     * Get all the importCoupons.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ImportCouponDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ImportCoupons");
        return importCouponRepository.findAll(pageable)
            .map(importCouponMapper::toDto);
    }


    /**
     * Get one importCoupon by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ImportCoupon> findOne(Long id) {
        log.debug("Request to get ImportCoupon : {}", id);
        return importCouponRepository.findById(id);
    }

    /**
     * Delete the importCoupon by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ImportCoupon : {}", id);
        importCouponRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public ImportCoupon findByCode(String code) {
        log.debug("Request to get all ImportCoupon");
        return importCouponRepository.findByCode(code);
    }
}

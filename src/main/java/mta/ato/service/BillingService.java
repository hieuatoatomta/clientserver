package mta.ato.service;

import mta.ato.domain.Billing;
import mta.ato.repository.BillingRepository;
import mta.ato.service.dto.BillingDTO;
import mta.ato.service.mapper.BillingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Billing}.
 */
@Service
@Transactional
public class BillingService {

    private final Logger log = LoggerFactory.getLogger(BillingService.class);

    private final BillingRepository billingRepository;

    private final BillingMapper billingMapper;

    public BillingService(BillingRepository billingRepository, BillingMapper billingMapper) {
        this.billingRepository = billingRepository;
        this.billingMapper = billingMapper;
    }

    /**
     * Save a billing.
     *
     * @param billingDTO the entity to save.
     * @return the persisted entity.
     */
    public BillingDTO save(BillingDTO billingDTO) {
        log.debug("Request to save Billing : {}", billingDTO);
        Billing billing = billingMapper.toEntity(billingDTO);
        billing = billingRepository.save(billing);
        return billingMapper.toDto(billing);
    }

    /**
     * Get all the billings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BillingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Billings");
        return billingRepository.findAll(pageable)
            .map(billingMapper::toDto);
    }


    /**
     * Get one billing by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BillingDTO> findOne(Long id) {
        log.debug("Request to get Billing : {}", id);
        return billingRepository.findById(id)
            .map(billingMapper::toDto);
    }

    /**
     * Delete the billing by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Billing : {}", id);
        billingRepository.deleteById(id);
    }
}

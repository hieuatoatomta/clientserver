package mta.ato.service;

import mta.ato.domain.ProductSize;
import mta.ato.domain.Products;
import mta.ato.domain.Sizes;
import mta.ato.repository.ProductSizeRepository;
import mta.ato.service.dto.ProductSizeDTO;
import mta.ato.service.mapper.ProductSizeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProductSize}.
 */
@Service
@Transactional
public class ProductSizeService {

    private final Logger log = LoggerFactory.getLogger(ProductSizeService.class);

    private final ProductSizeRepository productSizeRepository;

    private final ProductSizeMapper productSizeMapper;

    public ProductSizeService(ProductSizeRepository productSizeRepository, ProductSizeMapper productSizeMapper) {
        this.productSizeRepository = productSizeRepository;
        this.productSizeMapper = productSizeMapper;
    }

    /**
     * Save a productSize.
     *
     * @param productSizeDTO the entity to save.
     * @return the persisted entity.
     */
    public ProductSizeDTO save(ProductSizeDTO productSizeDTO) {
        log.debug("Request to save ProductSize : {}", productSizeDTO);
        ProductSize productSize = productSizeMapper.toEntity(productSizeDTO);
        productSize = productSizeRepository.save(productSize);
        return productSizeMapper.toDto(productSize);
    }


    public ProductSize saveEntity(ProductSize productSize) {
        return productSizeRepository.save(productSize);
    }

    /**
     * Get all the productSizes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ProductSizeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductSizes");
        return productSizeRepository.findAll(pageable)
            .map(productSizeMapper::toDto);
    }


    /**
     * Get one productSize by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProductSizeDTO> findOne(Long id) {
        log.debug("Request to get ProductSize : {}", id);
        return productSizeRepository.findById(id)
            .map(productSizeMapper::toDto);
    }

    @Transactional(readOnly = true)
    public ProductSize findByProductsAndSizes(Products products, Sizes sizes) {
        return productSizeRepository.findByProductsAndSizes(products, sizes);
    }


    /**
     * Delete the productSize by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProductSize : {}", id);
        productSizeRepository.deleteById(id);
    }
}

package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.ImportCoupon;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.ImportCouponRepository;
import mta.ato.service.dto.ImportCouponCriteria;
import mta.ato.service.dto.ImportCouponDTO;
import mta.ato.service.mapper.ImportCouponMapper;

/**
 * Service for executing complex queries for {@link ImportCoupon} entities in the database.
 * The main input is a {@link ImportCouponCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ImportCouponDTO} or a {@link Page} of {@link ImportCouponDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ImportCouponQueryService extends QueryService<ImportCoupon> {

    private final Logger log = LoggerFactory.getLogger(ImportCouponQueryService.class);

    private final ImportCouponRepository importCouponRepository;

    private final ImportCouponMapper importCouponMapper;

    public ImportCouponQueryService(ImportCouponRepository importCouponRepository, ImportCouponMapper importCouponMapper) {
        this.importCouponRepository = importCouponRepository;
        this.importCouponMapper = importCouponMapper;
    }

    /**
     * Return a {@link List} of {@link ImportCouponDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ImportCouponDTO> findByCriteria(ImportCouponCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ImportCoupon> specification = createSpecification(criteria);
        return importCouponMapper.toDto(importCouponRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ImportCouponDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ImportCoupon> findByCriteria(ImportCouponCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ImportCoupon> specification = createSpecification(criteria);
        return importCouponRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ImportCouponCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ImportCoupon> specification = createSpecification(criteria);
        return importCouponRepository.count(specification);
    }

    /**
     * Function to convert {@link ImportCouponCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ImportCoupon> createSpecification(ImportCouponCriteria criteria) {
        Specification<ImportCoupon> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ImportCoupon_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), ImportCoupon_.code));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), ImportCoupon_.description));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), ImportCoupon_.updateTime));
            }
            if (criteria.getDsReceiptId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsReceiptId(),
                    root -> root.join(ImportCoupon_.dsReceipts, JoinType.LEFT).get(DetailedImportCoupon_.id)));
            }
            if (criteria.getUsersId() != null) {
                specification = specification.and(buildSpecification(criteria.getUsersId(),
                    root -> root.join(ImportCoupon_.users, JoinType.LEFT).get(Users_.id)));
            }
            if (criteria.getSupplierId() != null) {
                specification = specification.and(buildSpecification(criteria.getSupplierId(),
                    root -> root.join(ImportCoupon_.supplier, JoinType.LEFT).get(Supplier_.id)));
            }
        }
        return specification;
    }
}

package mta.ato.service;

import mta.ato.domain.ObjectAction;
import mta.ato.repository.ObjectActionRepository;
import mta.ato.service.dto.ObjectActionDTO;
import mta.ato.service.mapper.ObjectActionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ObjectAction}.
 */
@Service
@Transactional
public class ObjectActionService {

    private final Logger log = LoggerFactory.getLogger(ObjectActionService.class);

    private final ObjectActionRepository objectActionRepository;

    private final ObjectActionMapper objectActionMapper;

    public ObjectActionService(ObjectActionRepository objectActionRepository, ObjectActionMapper objectActionMapper) {
        this.objectActionRepository = objectActionRepository;
        this.objectActionMapper = objectActionMapper;
    }

    /**
     * Save a objectAction.
     *
     * @param objectActionDTO the entity to save.
     * @return the persisted entity.
     */
    public ObjectActionDTO save(ObjectActionDTO objectActionDTO) {
        log.debug("Request to save ObjectAction : {}", objectActionDTO);
        ObjectAction objectAction = objectActionMapper.toEntity(objectActionDTO);
        objectAction = objectActionRepository.save(objectAction);
        return objectActionMapper.toDto(objectAction);
    }

    /**
     * Get all the objectActions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ObjectActionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ObjectActions");
        return objectActionRepository.findAll(pageable)
            .map(objectActionMapper::toDto);
    }


    /**
     * Get one objectAction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ObjectActionDTO> findOne(Long id) {
        log.debug("Request to get ObjectAction : {}", id);
        return objectActionRepository.findById(id)
            .map(objectActionMapper::toDto);
    }

    /**
     * Delete the objectAction by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ObjectAction : {}", id);
        objectActionRepository.deleteById(id);
    }
}

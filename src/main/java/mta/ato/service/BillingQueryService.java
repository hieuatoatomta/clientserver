package mta.ato.service;

import io.github.jhipster.service.QueryService;
import mta.ato.domain.Billing;
import mta.ato.domain.Billing_;
import mta.ato.repository.BillingRepository;
import mta.ato.service.dto.BillingCriteria;
import mta.ato.service.dto.BillingDTO;
import mta.ato.service.mapper.BillingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for executing complex queries for {@link Billing} entities in the database.
 * The main input is a {@link BillingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BillingDTO} or a {@link Page} of {@link BillingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BillingQueryService extends QueryService<Billing> {

    private final Logger log = LoggerFactory.getLogger(BillingQueryService.class);

    private final BillingRepository billingRepository;

    private final BillingMapper billingMapper;

    public BillingQueryService(BillingRepository billingRepository, BillingMapper billingMapper) {
        this.billingRepository = billingRepository;
        this.billingMapper = billingMapper;
    }

    /**
     * Return a {@link List} of {@link BillingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BillingDTO> findByCriteria(BillingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Billing> specification = createSpecification(criteria);
        return billingMapper.toDto(billingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BillingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BillingDTO> findByCriteria(BillingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Billing> specification = createSpecification(criteria);
        return billingRepository.findAll(specification, page)
            .map(billingMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BillingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Billing> specification = createSpecification(criteria);
        return billingRepository.count(specification);
    }

    /**
     * Function to convert {@link BillingCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Billing> createSpecification(BillingCriteria criteria) {
        Specification<Billing> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Billing_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Billing_.code));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), Billing_.status));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), Billing_.updateTime));
            }
        }
        return specification;
    }
}

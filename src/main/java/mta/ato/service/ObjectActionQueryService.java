package mta.ato.service;

import io.github.jhipster.service.QueryService;
import mta.ato.domain.Actions_;
import mta.ato.domain.ObjectAction;
import mta.ato.domain.ObjectAction_;
import mta.ato.domain.Objects_;
import mta.ato.repository.ObjectActionRepository;
import mta.ato.service.dto.ObjectActionCriteria;
import mta.ato.service.dto.ObjectActionDTO;
import mta.ato.service.mapper.ObjectActionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.List;

/**
 * Service for executing complex queries for {@link ObjectAction} entities in the database.
 * The main input is a {@link ObjectActionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ObjectActionDTO} or a {@link Page} of {@link ObjectActionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ObjectActionQueryService extends QueryService<ObjectAction> {

    private final Logger log = LoggerFactory.getLogger(ObjectActionQueryService.class);

    private final ObjectActionRepository objectActionRepository;

    private final ObjectActionMapper objectActionMapper;

    public ObjectActionQueryService(ObjectActionRepository objectActionRepository, ObjectActionMapper objectActionMapper) {
        this.objectActionRepository = objectActionRepository;
        this.objectActionMapper = objectActionMapper;
    }

    /**
     * Return a {@link List} of {@link ObjectActionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ObjectActionDTO> findByCriteria(ObjectActionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ObjectAction> specification = createSpecification(criteria);
        return objectActionMapper.toDto(objectActionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ObjectActionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ObjectActionDTO> findByCriteria(ObjectActionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ObjectAction> specification = createSpecification(criteria);
        return objectActionRepository.findAll(specification, page)
            .map(objectActionMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ObjectActionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ObjectAction> specification = createSpecification(criteria);
        return objectActionRepository.count(specification);
    }

    /**
     * Function to convert {@link ObjectActionCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ObjectAction> createSpecification(ObjectActionCriteria criteria) {
        Specification<ObjectAction> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ObjectAction_.id));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), ObjectAction_.updateTime));
            }
            if (criteria.getObjectsId() != null) {
                specification = specification.and(buildSpecification(criteria.getObjectsId(),
                    root -> root.join(ObjectAction_.objects, JoinType.LEFT).get(Objects_.id)));
            }
            if (criteria.getActionsId() != null) {
                specification = specification.and(buildSpecification(criteria.getActionsId(),
                    root -> root.join(ObjectAction_.actions, JoinType.LEFT).get(Actions_.id)));
            }
        }
        return specification;
    }
}

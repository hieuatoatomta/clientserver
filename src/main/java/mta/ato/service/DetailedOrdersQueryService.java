package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.DetailedOrders;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.DetailedOrdersRepository;
import mta.ato.service.dto.DetailedOrdersCriteria;
import mta.ato.service.dto.DetailedOrdersDTO;
import mta.ato.service.mapper.DetailedOrdersMapper;

/**
 * Service for executing complex queries for {@link DetailedOrders} entities in the database.
 * The main input is a {@link DetailedOrdersCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DetailedOrdersDTO} or a {@link Page} of {@link DetailedOrdersDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DetailedOrdersQueryService extends QueryService<DetailedOrders> {

    private final Logger log = LoggerFactory.getLogger(DetailedOrdersQueryService.class);

    private final DetailedOrdersRepository detailedOrdersRepository;

    private final DetailedOrdersMapper detailedOrdersMapper;

    public DetailedOrdersQueryService(DetailedOrdersRepository detailedOrdersRepository, DetailedOrdersMapper detailedOrdersMapper) {
        this.detailedOrdersRepository = detailedOrdersRepository;
        this.detailedOrdersMapper = detailedOrdersMapper;
    }

    /**
     * Return a {@link List} of {@link DetailedOrdersDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DetailedOrdersDTO> findByCriteria(DetailedOrdersCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DetailedOrders> specification = createSpecification(criteria);
        return detailedOrdersMapper.toDto(detailedOrdersRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DetailedOrdersDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DetailedOrdersDTO> findByCriteria(DetailedOrdersCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DetailedOrders> specification = createSpecification(criteria);
        return detailedOrdersRepository.findAll(specification, page)
            .map(detailedOrdersMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DetailedOrdersCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DetailedOrders> specification = createSpecification(criteria);
        return detailedOrdersRepository.count(specification);
    }

    /**
     * Function to convert {@link DetailedOrdersCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DetailedOrders> createSpecification(DetailedOrdersCriteria criteria) {
        Specification<DetailedOrders> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DetailedOrders_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), DetailedOrders_.amount));
            }
            if (criteria.getPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPrice(), DetailedOrders_.price));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), DetailedOrders_.updateTime));
            }
            if (criteria.getOrdersId() != null) {
                specification = specification.and(buildSpecification(criteria.getOrdersId(),
                    root -> root.join(DetailedOrders_.orders, JoinType.LEFT).get(Orders_.id)));
            }
            if (criteria.getProductSizeId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductSizeId(),
                    root -> root.join(DetailedOrders_.productSize, JoinType.LEFT).get(ProductSize_.id)));
            }
        }
        return specification;
    }
}

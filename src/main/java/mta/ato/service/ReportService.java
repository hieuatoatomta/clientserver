package mta.ato.service;

import mta.ato.domain.Actions;
import mta.ato.repository.ActionsRepository;
import mta.ato.service.mapper.ActionsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Actions}.
 */
@Service
@Transactional
public class ReportService {

    private final Logger log = LoggerFactory.getLogger(ReportService.class);

//    private final ExcelUtils excelUtils;
    private final ActionsRepository actionsRepository;

    private final ActionsMapper actionsMapper;

    public ReportService(ActionsRepository actionsRepository, ActionsMapper actionsMapper
//                         ExcelUtils excelUtils
    ) {
        this.actionsRepository = actionsRepository;
        this.actionsMapper = actionsMapper;
//        this.excelUtils = excelUtils;
    }
//    public File exportDataConfigReports(ConfigReportDataForm configReportDataForm) throws Exception {
//        List<ConfigReportColumnDTO> lstAllColumn = configReportColumnService.findAllByReportId(configReportDataForm.getReportId());
//
//        List<ConfigReportColumnDTO> lstColumnCurrent = DataUtil.getListShowColumByReportIdIgnoreKey(lstAllColumn);
//
//        List<ExcelColumn> lstColumn = DataUtil.getListExcelColumn(lstColumnCurrent);
//
//        ConfigReport configReport = configReportRepository.findById(configReportDataForm.getReportId()).get();
//
//        //Lay cac bang cau hinh luu time
//        ConfigReportColumnDTO columnTime = configReportUtilsService.getColumTimeName(configReportDataForm.getReportId());
//
//        ConfigReportColumnDTO primaryKey = getPrimaryKeyColumn(configReportMapper.toDto(configReport));
//        String primaryKeyColumn = null;
//        if (primaryKey != null) {
//            primaryKeyColumn = primaryKey.getColumnName();
//        }
//
//        List<Object> lstData = configReportImportRepository.findDataConfigReport(configReportDataForm, lstColumnCurrent, configReport, null, columnTime.getColumnName(), false, primaryKeyColumn);
//
//        File file = excelUtils.exportData(lstColumn, lstData, configReport.getTitle(), "export_" + configReport.getTableName(), Constants.FILE_IMPORT.START_ROW, Constants.FILE_IMPORT.START_COL);
//        return file;
//    }


}

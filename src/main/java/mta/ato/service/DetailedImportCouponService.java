package mta.ato.service;

import mta.ato.domain.DetailedImportCoupon;
import mta.ato.domain.ImportCoupon;
import mta.ato.repository.DetailedImportCouponRepository;
import mta.ato.service.dto.DetailedImportCouponDTO;
import mta.ato.service.mapper.DetailedImportCouponMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DetailedImportCoupon}.
 */
@Service
@Transactional
public class DetailedImportCouponService {

    private final Logger log = LoggerFactory.getLogger(DetailedImportCouponService.class);

    private final DetailedImportCouponRepository detailedImportCouponRepository;

    private final DetailedImportCouponMapper detailedImportCouponMapper;

    public DetailedImportCouponService(DetailedImportCouponRepository detailedImportCouponRepository, DetailedImportCouponMapper detailedImportCouponMapper) {
        this.detailedImportCouponRepository = detailedImportCouponRepository;
        this.detailedImportCouponMapper = detailedImportCouponMapper;
    }

    /**
     * Save a detailedImportCoupon.
     *
     * @param detailedImportCouponDTO the entity to save.
     * @return the persisted entity.
     */
    public DetailedImportCouponDTO save(DetailedImportCouponDTO detailedImportCouponDTO) {
        log.debug("Request to save DetailedImportCoupon : {}", detailedImportCouponDTO);
        DetailedImportCoupon detailedImportCoupon = detailedImportCouponMapper.toEntity(detailedImportCouponDTO);
        detailedImportCoupon = detailedImportCouponRepository.save(detailedImportCoupon);
        return detailedImportCouponMapper.toDto(detailedImportCoupon);
    }

    /**
     * Get all the detailedImportCoupons.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DetailedImportCouponDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DetailedImportCoupons");
        return detailedImportCouponRepository.findAll(pageable)
            .map(detailedImportCouponMapper::toDto);
    }


    /**
     * Get one detailedImportCoupon by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DetailedImportCouponDTO> findOne(Long id) {
        log.debug("Request to get DetailedImportCoupon : {}", id);
        return detailedImportCouponRepository.findById(id)
            .map(detailedImportCouponMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<DetailedImportCoupon> findById(ImportCoupon importCoupon) {
        log.debug("Request to get DetailedImportCoupon : {}", importCoupon);
        return detailedImportCouponRepository.findAllByImportCoupon(importCoupon);
    }

    /**
     * Delete the detailedImportCoupon by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DetailedImportCoupon : {}", id);
        detailedImportCouponRepository.deleteById(id);
    }
}

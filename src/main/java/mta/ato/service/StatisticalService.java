package mta.ato.service;

import mta.ato.domain.Billing;
import mta.ato.repository.BillingRepository;
import mta.ato.repository.OrdersRepository;
import mta.ato.service.dto.CustomersTopProductsDTO;
import mta.ato.service.dto.StatisticsDTO;
import mta.ato.service.dto.StatisticsTopProductsDTO;
import mta.ato.service.mapper.BillingMapper;
import mta.ato.utils.DataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Billing}.
 */
@Service
@Transactional
public class StatisticalService {

    private final Logger log = LoggerFactory.getLogger(StatisticalService.class);

    private final BillingRepository billingRepository;

    private final BillingMapper billingMapper;

    private final OrdersRepository ordersRepository;

    public StatisticalService(BillingRepository billingRepository,
                              OrdersRepository ordersRepository,
                              BillingMapper billingMapper) {
        this.billingRepository = billingRepository;
        this.billingMapper = billingMapper;
        this.ordersRepository = ordersRepository;
    }

    public List<StatisticsDTO> getSendCount(String fromTime, String toTime) {
        List<Object[]> lstDatas3 = ordersRepository.getSendCount(fromTime, toTime);
        return lstDatas3.stream().map(e -> {
            StatisticsDTO dto3 = new StatisticsDTO();
            dto3.setTotalImport(DataUtil.safeToInt(e[1]));
            dto3.setUpdateTime(DataUtil.safeToString(e[0]));
            return dto3;
        }).collect(Collectors.toList());
    }

    public List<StatisticsDTO> getOrdersCount(String fromTime, String toTime) {
        List<Object[]> lstDatas3 = ordersRepository.getOrdersCount(fromTime, toTime);
        return lstDatas3.stream().map(e -> {
            StatisticsDTO dto3 = new StatisticsDTO();
            dto3.setUpdateTime(DataUtil.safeToString(e[0]));
            dto3.setTotalImport(DataUtil.safeToInt(e[1]));
            return dto3;
        }).collect(Collectors.toList());
    }

    public List<StatisticsDTO> getDoanhThu(String fromTime, String toTime) {
        List<Object[]> lstDatas3 = ordersRepository.getDoanhThu(fromTime, toTime);
        return lstDatas3.stream().map(e -> {
            StatisticsDTO dto3 = new StatisticsDTO();
            dto3.setTotalImports(DataUtil.safeToLong(e[0]));
            dto3.setTotalOrders(DataUtil.safeToLong(e[1]));
            return dto3;
        }).collect(Collectors.toList());
    }

    public List<StatisticsTopProductsDTO> getSanPhamBanChay(Long soLuong, String fromTime, String toTime) {
        if ( soLuong == null){
            soLuong = 10L;
        }
        List<Object[]> lstDatas3 = ordersRepository.getSanPhamBanChay(soLuong, fromTime, toTime);
        return lstDatas3.stream().map(e -> {
            StatisticsTopProductsDTO dto3 = new StatisticsTopProductsDTO();
            dto3.setCode(DataUtil.safeToString(e[0]));
            dto3.setNameProducts(DataUtil.safeToString(e[1]));
            dto3.setNameSize(DataUtil.safeToString(e[2]));
            dto3.setTotalOrder(DataUtil.safeToLong(e[3]));
            return dto3;
        }).collect(Collectors.toList());
    }

    public List<CustomersTopProductsDTO> getTopKhachHang(Long soLuong, String fromTime, String toTime) {
        if ( soLuong == null){
            soLuong = 10L;
        }
        List<Object[]> lstDatas3 = ordersRepository.getTopKhachHang(soLuong, fromTime, toTime);
        return lstDatas3.stream().map(e -> {
            CustomersTopProductsDTO dto3 = new CustomersTopProductsDTO();
            dto3.setFullName(DataUtil.safeToString(e[0]));
            dto3.setMail(DataUtil.safeToString(e[1]));
            dto3.setTotalOrder(DataUtil.safeToLong(e[2]));
            dto3.setTotalPrice(DataUtil.safeToLong(e[3]));
            return dto3;
        }).collect(Collectors.toList());
    }
}

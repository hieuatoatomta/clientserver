package mta.ato.service.dto;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A DTO for the {@link mta.ato.domain.ImageLink} entity.
 */
public class ImageLinkDTO implements Serializable {

    private Long id;

    @Size(max = 60)
    private String name;

    private Long status;

    private String updateTime;

    @Size(max = 1000)
    private String imageLink;


    private Long productsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public Long getProductsId() {
        return productsId;
    }

    public void setProductsId(Long productsId) {
        this.productsId = productsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageLinkDTO)) {
            return false;
        }

        return id != null && id.equals(((ImageLinkDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageLinkDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", status=" + getStatus() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", imageLink='" + getImageLink() + "'" +
            ", productsId=" + getProductsId() +
            "}";
    }
}

package mta.ato.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A DTO for the {@link mta.ato.domain.DetailedImportCoupon} entity.
 */
public class DetailedImportCouponDTO implements Serializable {

    private Long id;

    private Long amount;

    private String importPrice;

    private ZonedDateTime updateTime;


    private Long importCouponId;

    private Long productsId;

    private Long sizesId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(String importPrice) {
        this.importPrice = importPrice;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getImportCouponId() {
        return importCouponId;
    }

    public void setImportCouponId(Long importCouponId) {
        this.importCouponId = importCouponId;
    }

    public Long getProductsId() {
        return productsId;
    }

    public void setProductsId(Long productsId) {
        this.productsId = productsId;
    }

    public Long getSizesId() {
        return sizesId;
    }

    public void setSizesId(Long sizesId) {
        this.sizesId = sizesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailedImportCouponDTO)) {
            return false;
        }

        return id != null && id.equals(((DetailedImportCouponDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailedImportCouponDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", importPrice=" + getImportPrice() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", importCouponId=" + getImportCouponId() +
            ", productsId=" + getProductsId() +
            ", sizesId=" + getSizesId() +
            "}";
    }
}

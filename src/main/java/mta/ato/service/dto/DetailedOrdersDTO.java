package mta.ato.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A DTO for the {@link mta.ato.domain.DetailedOrders} entity.
 */
public class DetailedOrdersDTO implements Serializable {
    
    private Long id;

    private Long amount;

    private BigDecimal price;

    private ZonedDateTime updateTime;


    private Long ordersId;

    private Long productSizeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(Long ordersId) {
        this.ordersId = ordersId;
    }

    public Long getProductSizeId() {
        return productSizeId;
    }

    public void setProductSizeId(Long productSizeId) {
        this.productSizeId = productSizeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailedOrdersDTO)) {
            return false;
        }

        return id != null && id.equals(((DetailedOrdersDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailedOrdersDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", price=" + getPrice() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", ordersId=" + getOrdersId() +
            ", productSizeId=" + getProductSizeId() +
            "}";
    }
}

package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.Orders} entity. This class is used
 * in {@link mta.ato.web.rest.OrdersResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /orders?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OrdersCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter address;

    private LongFilter status;

    private ZonedDateTimeFilter updateTime;

    private StringFilter contact;

    private StringFilter description;

    private LongFilter dsDetailedOrdersId;

    private LongFilter usersId;

    public OrdersCriteria() {
    }

    public OrdersCriteria(OrdersCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.contact = other.contact == null ? null : other.contact.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.dsDetailedOrdersId = other.dsDetailedOrdersId == null ? null : other.dsDetailedOrdersId.copy();
        this.usersId = other.usersId == null ? null : other.usersId.copy();
    }

    @Override
    public OrdersCriteria copy() {
        return new OrdersCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public LongFilter getStatus() {
        return status;
    }

    public void setStatus(LongFilter status) {
        this.status = status;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public StringFilter getContact() {
        return contact;
    }

    public void setContact(StringFilter contact) {
        this.contact = contact;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getDsDetailedOrdersId() {
        return dsDetailedOrdersId;
    }

    public void setDsDetailedOrdersId(LongFilter dsDetailedOrdersId) {
        this.dsDetailedOrdersId = dsDetailedOrdersId;
    }

    public LongFilter getUsersId() {
        return usersId;
    }

    public void setUsersId(LongFilter usersId) {
        this.usersId = usersId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OrdersCriteria that = (OrdersCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(address, that.address) &&
            Objects.equals(status, that.status) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(contact, that.contact) &&
            Objects.equals(description, that.description) &&
            Objects.equals(dsDetailedOrdersId, that.dsDetailedOrdersId) &&
            Objects.equals(usersId, that.usersId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        address,
        status,
        updateTime,
        contact,
        description,
        dsDetailedOrdersId,
        usersId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrdersCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (contact != null ? "contact=" + contact + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (dsDetailedOrdersId != null ? "dsDetailedOrdersId=" + dsDetailedOrdersId + ", " : "") +
                (usersId != null ? "usersId=" + usersId + ", " : "") +
            "}";
    }

}

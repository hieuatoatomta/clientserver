package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.DetailedImportCoupon} entity. This class is used
 * in {@link mta.ato.web.rest.DetailedImportCouponResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /detailed-import-coupons?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DetailedImportCouponCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter amount;

    private BigDecimalFilter importPrice;

    private ZonedDateTimeFilter updateTime;

    private LongFilter importCouponId;

    private LongFilter productsId;

    private LongFilter sizesId;

    public DetailedImportCouponCriteria() {
    }

    public DetailedImportCouponCriteria(DetailedImportCouponCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.importPrice = other.importPrice == null ? null : other.importPrice.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.importCouponId = other.importCouponId == null ? null : other.importCouponId.copy();
        this.productsId = other.productsId == null ? null : other.productsId.copy();
        this.sizesId = other.sizesId == null ? null : other.sizesId.copy();
    }

    @Override
    public DetailedImportCouponCriteria copy() {
        return new DetailedImportCouponCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getAmount() {
        return amount;
    }

    public void setAmount(LongFilter amount) {
        this.amount = amount;
    }

    public BigDecimalFilter getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(BigDecimalFilter importPrice) {
        this.importPrice = importPrice;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getImportCouponId() {
        return importCouponId;
    }

    public void setImportCouponId(LongFilter importCouponId) {
        this.importCouponId = importCouponId;
    }

    public LongFilter getProductsId() {
        return productsId;
    }

    public void setProductsId(LongFilter productsId) {
        this.productsId = productsId;
    }

    public LongFilter getSizesId() {
        return sizesId;
    }

    public void setSizesId(LongFilter sizesId) {
        this.sizesId = sizesId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DetailedImportCouponCriteria that = (DetailedImportCouponCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(importPrice, that.importPrice) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(importCouponId, that.importCouponId) &&
            Objects.equals(productsId, that.productsId) &&
            Objects.equals(sizesId, that.sizesId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        amount,
        importPrice,
        updateTime,
        importCouponId,
        productsId,
        sizesId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailedImportCouponCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (importPrice != null ? "importPrice=" + importPrice + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (importCouponId != null ? "importCouponId=" + importCouponId + ", " : "") +
                (productsId != null ? "productsId=" + productsId + ", " : "") +
                (sizesId != null ? "sizesId=" + sizesId + ", " : "") +
            "}";
    }

}

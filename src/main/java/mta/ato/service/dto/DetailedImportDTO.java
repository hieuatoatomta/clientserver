package mta.ato.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A DTO for the {@link mta.ato.domain.DetailedImportCoupon} entity.
 */
public class DetailedImportDTO implements Serializable {

    private Long id;

    private String code;
    private String name;

    private Long amount;

    private BigDecimal importPrice;

    private Long importCouponId;

    private Long sizesId;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public BigDecimal getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(BigDecimal importPrice) {
        this.importPrice = importPrice;
    }


    public Long getImportCouponId() {
        return importCouponId;
    }

    public void setImportCouponId(Long importCouponId) {
        this.importCouponId = importCouponId;
    }

    public Long getSizesId() {
        return sizesId;
    }

    public void setSizesId(Long sizesId) {
        this.sizesId = sizesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DetailedImportDTO)) {
            return false;
        }

        return id != null && id.equals(((DetailedImportDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailedImportCouponDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", importPrice=" + getImportPrice() +
            ", importCouponId=" + getImportCouponId() +
            ", sizesId=" + getSizesId() +
            "}";
    }
}

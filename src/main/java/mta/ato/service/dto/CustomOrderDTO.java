package mta.ato.service.dto;

public class CustomOrderDTO {
    private Long sizesId;
    private Long productsId;
    private Long amount;

    public Long getSizesId() {
        return sizesId;
    }

    public void setSizesId(Long sizesId) {
        this.sizesId = sizesId;
    }

    public Long getProductsId() {
        return productsId;
    }

    public void setProductsId(Long productsId) {
        this.productsId = productsId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public CustomOrderDTO() {
    }
}

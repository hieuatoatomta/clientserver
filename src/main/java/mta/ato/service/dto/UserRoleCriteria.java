package mta.ato.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link mta.ato.domain.UserRole} entity. This class is used
 * in {@link mta.ato.web.rest.UserRoleResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-roles?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserRoleCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private ZonedDateTimeFilter updateTime;

    private LongFilter usersId;

    private LongFilter rolesId;

    public UserRoleCriteria() {
    }

    public UserRoleCriteria(UserRoleCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.usersId = other.usersId == null ? null : other.usersId.copy();
        this.rolesId = other.rolesId == null ? null : other.rolesId.copy();
    }

    @Override
    public UserRoleCriteria copy() {
        return new UserRoleCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getUsersId() {
        return usersId;
    }

    public void setUsersId(LongFilter usersId) {
        this.usersId = usersId;
    }

    public LongFilter getRolesId() {
        return rolesId;
    }

    public void setRolesId(LongFilter rolesId) {
        this.rolesId = rolesId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserRoleCriteria that = (UserRoleCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(usersId, that.usersId) &&
            Objects.equals(rolesId, that.rolesId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        updateTime,
        usersId,
        rolesId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserRoleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (usersId != null ? "usersId=" + usersId + ", " : "") +
                (rolesId != null ? "rolesId=" + rolesId + ", " : "") +
            "}";
    }

}

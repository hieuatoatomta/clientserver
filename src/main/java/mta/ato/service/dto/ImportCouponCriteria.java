package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.ImportCoupon} entity. This class is used
 * in {@link mta.ato.web.rest.ImportCouponResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /import-coupons?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ImportCouponCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter description;

    private ZonedDateTimeFilter updateTime;

    private LongFilter dsReceiptId;

    private LongFilter usersId;

    private LongFilter supplierId;

    public ImportCouponCriteria() {
    }

    public ImportCouponCriteria(ImportCouponCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.dsReceiptId = other.dsReceiptId == null ? null : other.dsReceiptId.copy();
        this.usersId = other.usersId == null ? null : other.usersId.copy();
        this.supplierId = other.supplierId == null ? null : other.supplierId.copy();
    }

    @Override
    public ImportCouponCriteria copy() {
        return new ImportCouponCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getDsReceiptId() {
        return dsReceiptId;
    }

    public void setDsReceiptId(LongFilter dsReceiptId) {
        this.dsReceiptId = dsReceiptId;
    }

    public LongFilter getUsersId() {
        return usersId;
    }

    public void setUsersId(LongFilter usersId) {
        this.usersId = usersId;
    }

    public LongFilter getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(LongFilter supplierId) {
        this.supplierId = supplierId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ImportCouponCriteria that = (ImportCouponCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(description, that.description) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(dsReceiptId, that.dsReceiptId) &&
            Objects.equals(usersId, that.usersId) &&
            Objects.equals(supplierId, that.supplierId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        description,
        updateTime,
        dsReceiptId,
        usersId,
        supplierId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImportCouponCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (dsReceiptId != null ? "dsReceiptId=" + dsReceiptId + ", " : "") +
                (usersId != null ? "usersId=" + usersId + ", " : "") +
                (supplierId != null ? "supplierId=" + supplierId + ", " : "") +
            "}";
    }

}

package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.DetailedOrders} entity. This class is used
 * in {@link mta.ato.web.rest.DetailedOrdersResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /detailed-orders?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DetailedOrdersCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter amount;

    private BigDecimalFilter price;

    private ZonedDateTimeFilter updateTime;

    private LongFilter ordersId;

    private LongFilter productSizeId;

    public DetailedOrdersCriteria() {
    }

    public DetailedOrdersCriteria(DetailedOrdersCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.price = other.price == null ? null : other.price.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.ordersId = other.ordersId == null ? null : other.ordersId.copy();
        this.productSizeId = other.productSizeId == null ? null : other.productSizeId.copy();
    }

    @Override
    public DetailedOrdersCriteria copy() {
        return new DetailedOrdersCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getAmount() {
        return amount;
    }

    public void setAmount(LongFilter amount) {
        this.amount = amount;
    }

    public BigDecimalFilter getPrice() {
        return price;
    }

    public void setPrice(BigDecimalFilter price) {
        this.price = price;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(LongFilter ordersId) {
        this.ordersId = ordersId;
    }

    public LongFilter getProductSizeId() {
        return productSizeId;
    }

    public void setProductSizeId(LongFilter productSizeId) {
        this.productSizeId = productSizeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DetailedOrdersCriteria that = (DetailedOrdersCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(price, that.price) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(ordersId, that.ordersId) &&
            Objects.equals(productSizeId, that.productSizeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        amount,
        price,
        updateTime,
        ordersId,
        productSizeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DetailedOrdersCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (price != null ? "price=" + price + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (ordersId != null ? "ordersId=" + ordersId + ", " : "") +
                (productSizeId != null ? "productSizeId=" + productSizeId + ", " : "") +
            "}";
    }

}

package mta.ato.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A DTO for the {@link mta.ato.domain.ObjectAction} entity.
 */
public class ObjectActionDTO implements Serializable {

    private Long id;

    private ZonedDateTime updateTime;


    private Long objectsId;

    private Long actionsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getObjectsId() {
        return objectsId;
    }

    public void setObjectsId(Long objectsId) {
        this.objectsId = objectsId;
    }

    public Long getActionsId() {
        return actionsId;
    }

    public void setActionsId(Long actionsId) {
        this.actionsId = actionsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ObjectActionDTO)) {
            return false;
        }

        return id != null && id.equals(((ObjectActionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ObjectActionDTO{" +
            "id=" + getId() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", objectsId=" + getObjectsId() +
            ", actionsId=" + getActionsId() +
            "}";
    }
}

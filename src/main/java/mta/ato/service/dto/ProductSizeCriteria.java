package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link mta.ato.domain.ProductSize} entity. This class is used
 * in {@link mta.ato.web.rest.ProductSizeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /product-sizes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProductSizeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter amount;

    private LongFilter dsDetailedOrdersId;

    private LongFilter productsId;

    private LongFilter sizesId;

    public ProductSizeCriteria() {
    }

    public ProductSizeCriteria(ProductSizeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.dsDetailedOrdersId = other.dsDetailedOrdersId == null ? null : other.dsDetailedOrdersId.copy();
        this.productsId = other.productsId == null ? null : other.productsId.copy();
        this.sizesId = other.sizesId == null ? null : other.sizesId.copy();
    }

    @Override
    public ProductSizeCriteria copy() {
        return new ProductSizeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getAmount() {
        return amount;
    }

    public void setAmount(LongFilter amount) {
        this.amount = amount;
    }

    public LongFilter getDsDetailedOrdersId() {
        return dsDetailedOrdersId;
    }

    public void setDsDetailedOrdersId(LongFilter dsDetailedOrdersId) {
        this.dsDetailedOrdersId = dsDetailedOrdersId;
    }

    public LongFilter getProductsId() {
        return productsId;
    }

    public void setProductsId(LongFilter productsId) {
        this.productsId = productsId;
    }

    public LongFilter getSizesId() {
        return sizesId;
    }

    public void setSizesId(LongFilter sizesId) {
        this.sizesId = sizesId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProductSizeCriteria that = (ProductSizeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(dsDetailedOrdersId, that.dsDetailedOrdersId) &&
            Objects.equals(productsId, that.productsId) &&
            Objects.equals(sizesId, that.sizesId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        amount,
        dsDetailedOrdersId,
        productsId,
        sizesId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductSizeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (dsDetailedOrdersId != null ? "dsDetailedOrdersId=" + dsDetailedOrdersId + ", " : "") +
                (productsId != null ? "productsId=" + productsId + ", " : "") +
                (sizesId != null ? "sizesId=" + sizesId + ", " : "") +
            "}";
    }

}

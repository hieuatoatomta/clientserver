package mta.ato.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link mta.ato.domain.ProductSize} entity.
 */
public class ProductSizeDTO implements Serializable {

    private Long id;

    private Long amount;

    private String nameSize;

    private Long productsId;

    private Long sizesId;

    public String getNameSize() {
        return nameSize;
    }

    public void setNameSize(String nameSize) {
        this.nameSize = nameSize;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getProductsId() {
        return productsId;
    }

    public void setProductsId(Long productsId) {
        this.productsId = productsId;
    }

    public Long getSizesId() {
        return sizesId;
    }

    public void setSizesId(Long sizesId) {
        this.sizesId = sizesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductSizeDTO)) {
            return false;
        }

        return id != null && id.equals(((ProductSizeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductSizeDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", productsId=" + getProductsId() +
            ", sizesId=" + getSizesId() +
            "}";
    }
}

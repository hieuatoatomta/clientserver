package mta.ato.service.dto;

import mta.ato.utils.DateUtil;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link mta.ato.domain.ImportCoupon} entity.
 */
public class ImportCouponDTO implements Serializable {

    private Long id;

    @Size(max = 60)
    private String code;

    private String nameSupplier;

    @Size(max = 1000)
    private String description;

    private ZonedDateTime updateTime;


    private Long usersId;

    private Long supplierId;


    public String getNameSupplier() {
        return nameSupplier;
    }

    public void setNameSupplier(String nameSupplier) {
        this.nameSupplier = nameSupplier;
    }

    private List<ImportCustomDTO> importCustomDTOList;

    public List<ImportCustomDTO> getImportCustomDTOList() {
        return importCustomDTOList;
    }


    public void setImportCustomDTOList(List<ImportCustomDTO> importCustomDTOList) {
        this.importCustomDTOList = importCustomDTOList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUsersId() {
        return usersId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImportCouponDTO)) {
            return false;
        }

        return id != null && id.equals(((ImportCouponDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImportCouponDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", updateTime='" + getUpdateTime() + "'" +
            ", usersId=" + getUsersId() +
            ", supplierId=" + getSupplierId() +
            "}";
    }
}

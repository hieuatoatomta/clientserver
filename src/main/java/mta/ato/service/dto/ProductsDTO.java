package mta.ato.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A DTO for the {@link mta.ato.domain.Products} entity.
 */
public class ProductsDTO implements Serializable {

    private Long id;

    @Size(max = 60)
    private String name;

    @Size(max = 60)
    private String code;

    private String quantity;

    private String cost;

    private Long rate;

    @Size(max = 1000)
    private String description;

    private Long status;

    private ZonedDateTime updateTime;

    private ZonedDateTime relaseDate;


    private Long objectsId;

    private String parenObject;


    public String getParenObject() {
        return parenObject;
    }

    public void setParenObject(String parenObject) {
        this.parenObject = parenObject;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public ZonedDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public ZonedDateTime getRelaseDate() {
        return relaseDate;
    }

    public void setRelaseDate(ZonedDateTime relaseDate) {
        this.relaseDate = relaseDate;
    }

    public Long getObjectsId() {
        return objectsId;
    }

    public void setObjectsId(Long objectsId) {
        this.objectsId = objectsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductsDTO)) {
            return false;
        }

        return id != null && id.equals(((ProductsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductsDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", quantity='" + getQuantity() + "'" +
            ", cost=" + getCost() +
            ", rate=" + getRate() +
            ", description='" + getDescription() + "'" +
            ", status=" + getStatus() +
            ", updateTime='" + getUpdateTime() + "'" +
            ", relaseDate='" + getRelaseDate() + "'" +
            ", objectsId=" + getObjectsId() +
            "}";
    }
}

package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.Sizes} entity. This class is used
 * in {@link mta.ato.web.rest.SizesResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /sizes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SizesCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter code;

    private StringFilter description;

    private LongFilter status;

    private ZonedDateTimeFilter updateTime;

    private LongFilter dsProductSizeId;

    private LongFilter dsDetailedImportCouponId;

    public SizesCriteria() {
    }

    public SizesCriteria(SizesCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.dsProductSizeId = other.dsProductSizeId == null ? null : other.dsProductSizeId.copy();
        this.dsDetailedImportCouponId = other.dsDetailedImportCouponId == null ? null : other.dsDetailedImportCouponId.copy();
    }

    @Override
    public SizesCriteria copy() {
        return new SizesCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getStatus() {
        return status;
    }

    public void setStatus(LongFilter status) {
        this.status = status;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public LongFilter getDsProductSizeId() {
        return dsProductSizeId;
    }

    public void setDsProductSizeId(LongFilter dsProductSizeId) {
        this.dsProductSizeId = dsProductSizeId;
    }

    public LongFilter getDsDetailedImportCouponId() {
        return dsDetailedImportCouponId;
    }

    public void setDsDetailedImportCouponId(LongFilter dsDetailedImportCouponId) {
        this.dsDetailedImportCouponId = dsDetailedImportCouponId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SizesCriteria that = (SizesCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(code, that.code) &&
            Objects.equals(description, that.description) &&
            Objects.equals(status, that.status) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(dsProductSizeId, that.dsProductSizeId) &&
            Objects.equals(dsDetailedImportCouponId, that.dsDetailedImportCouponId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        code,
        description,
        status,
        updateTime,
        dsProductSizeId,
        dsDetailedImportCouponId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SizesCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (dsProductSizeId != null ? "dsProductSizeId=" + dsProductSizeId + ", " : "") +
                (dsDetailedImportCouponId != null ? "dsDetailedImportCouponId=" + dsDetailedImportCouponId + ", " : "") +
            "}";
    }

}

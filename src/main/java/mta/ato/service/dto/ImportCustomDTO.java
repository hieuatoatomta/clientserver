package mta.ato.service.dto;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * A DTO for the {@link mta.ato.domain.ImportCoupon} entity.
 */
public class ImportCustomDTO implements Serializable {

    private Long id;

    // so luong
    private Long amount;

    // id size
    private Long idSize;

    private Long idObjects;

    // gia nhap san pham
    private String importPrice;

    // ten san pham
    @Size(max = 60)
    private String name;

    // ma san pham
    @Size(max = 60)
    private String code;

    public Long getIdObjects() {
        return idObjects;
    }

    public void setIdObjects(Long idObjects) {
        this.idObjects = idObjects;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getIdSize() {
        return idSize;
    }

    public void setIdSize(Long idSize) {
        this.idSize = idSize;
    }

    public String getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(String importPrice) {
        this.importPrice = importPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}

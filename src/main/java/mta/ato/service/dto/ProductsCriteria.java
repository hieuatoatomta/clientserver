package mta.ato.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link mta.ato.domain.Products} entity. This class is used
 * in {@link mta.ato.web.rest.ProductsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /products?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProductsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter code;

    private StringFilter quantity;

    private BigDecimalFilter cost;

    private LongFilter rate;

    private StringFilter description;

    private LongFilter status;

    private ZonedDateTimeFilter updateTime;

    private ZonedDateTimeFilter relaseDate;

    private LongFilter dsProductSizeId;

    private LongFilter dsImageLinkId;

    private LongFilter dsDetailedImportCouponId;

    private LongFilter objectsId;

    public ProductsCriteria() {
    }

    public ProductsCriteria(ProductsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.quantity = other.quantity == null ? null : other.quantity.copy();
        this.cost = other.cost == null ? null : other.cost.copy();
        this.rate = other.rate == null ? null : other.rate.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.updateTime = other.updateTime == null ? null : other.updateTime.copy();
        this.relaseDate = other.relaseDate == null ? null : other.relaseDate.copy();
        this.dsProductSizeId = other.dsProductSizeId == null ? null : other.dsProductSizeId.copy();
        this.dsImageLinkId = other.dsImageLinkId == null ? null : other.dsImageLinkId.copy();
        this.dsDetailedImportCouponId = other.dsDetailedImportCouponId == null ? null : other.dsDetailedImportCouponId.copy();
        this.objectsId = other.objectsId == null ? null : other.objectsId.copy();
    }

    @Override
    public ProductsCriteria copy() {
        return new ProductsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getQuantity() {
        return quantity;
    }

    public void setQuantity(StringFilter quantity) {
        this.quantity = quantity;
    }

    public BigDecimalFilter getCost() {
        return cost;
    }

    public void setCost(BigDecimalFilter cost) {
        this.cost = cost;
    }

    public LongFilter getRate() {
        return rate;
    }

    public void setRate(LongFilter rate) {
        this.rate = rate;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getStatus() {
        return status;
    }

    public void setStatus(LongFilter status) {
        this.status = status;
    }

    public ZonedDateTimeFilter getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(ZonedDateTimeFilter updateTime) {
        this.updateTime = updateTime;
    }

    public ZonedDateTimeFilter getRelaseDate() {
        return relaseDate;
    }

    public void setRelaseDate(ZonedDateTimeFilter relaseDate) {
        this.relaseDate = relaseDate;
    }

    public LongFilter getDsProductSizeId() {
        return dsProductSizeId;
    }

    public void setDsProductSizeId(LongFilter dsProductSizeId) {
        this.dsProductSizeId = dsProductSizeId;
    }

    public LongFilter getDsImageLinkId() {
        return dsImageLinkId;
    }

    public void setDsImageLinkId(LongFilter dsImageLinkId) {
        this.dsImageLinkId = dsImageLinkId;
    }

    public LongFilter getDsDetailedImportCouponId() {
        return dsDetailedImportCouponId;
    }

    public void setDsDetailedImportCouponId(LongFilter dsDetailedImportCouponId) {
        this.dsDetailedImportCouponId = dsDetailedImportCouponId;
    }

    public LongFilter getObjectsId() {
        return objectsId;
    }

    public void setObjectsId(LongFilter objectsId) {
        this.objectsId = objectsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProductsCriteria that = (ProductsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(code, that.code) &&
            Objects.equals(quantity, that.quantity) &&
            Objects.equals(cost, that.cost) &&
            Objects.equals(rate, that.rate) &&
            Objects.equals(description, that.description) &&
            Objects.equals(status, that.status) &&
            Objects.equals(updateTime, that.updateTime) &&
            Objects.equals(relaseDate, that.relaseDate) &&
            Objects.equals(dsProductSizeId, that.dsProductSizeId) &&
            Objects.equals(dsImageLinkId, that.dsImageLinkId) &&
            Objects.equals(dsDetailedImportCouponId, that.dsDetailedImportCouponId) &&
            Objects.equals(objectsId, that.objectsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        code,
        quantity,
        cost,
        rate,
        description,
        status,
        updateTime,
        relaseDate,
        dsProductSizeId,
        dsImageLinkId,
        dsDetailedImportCouponId,
        objectsId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (quantity != null ? "quantity=" + quantity + ", " : "") +
                (cost != null ? "cost=" + cost + ", " : "") +
                (rate != null ? "rate=" + rate + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (updateTime != null ? "updateTime=" + updateTime + ", " : "") +
                (relaseDate != null ? "relaseDate=" + relaseDate + ", " : "") +
                (dsProductSizeId != null ? "dsProductSizeId=" + dsProductSizeId + ", " : "") +
                (dsImageLinkId != null ? "dsImageLinkId=" + dsImageLinkId + ", " : "") +
                (dsDetailedImportCouponId != null ? "dsDetailedImportCouponId=" + dsDetailedImportCouponId + ", " : "") +
                (objectsId != null ? "objectsId=" + objectsId + ", " : "") +
            "}";
    }

}

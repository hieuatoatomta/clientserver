package mta.ato.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

public class StatisticsDTO implements Serializable {
    private String updateTime;
    private int totalImport;

    private String fromTime;
    private String toTime;
    private Long soLuong;
    private Long totalOrders;
    private Long totalImports;

    public Long getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Long soLuong) {
        this.soLuong = soLuong;
    }

    public Long getTotalOrders() {
        return totalOrders;
    }

    public Long getTotalImports() {
        return totalImports;
    }

    public void setTotalImports(Long totalImports) {
        this.totalImports = totalImports;
    }

    public void setTotalOrders(Long totalOrders) {
        this.totalOrders = totalOrders;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public int getTotalImport() {
        return totalImport;
    }

    public void setTotalImport(int totalImport) {
        this.totalImport = totalImport;
    }

//
//    public Long getLTimeType(){
//        Long ltimeType = null;
//        if(this.getTimeType() >0){
//            ltimeType = Long.valueOf(this.getTimeType());
//        }
//        return ltimeType;
//    }
//
//    public Long getLReportType(){
//        Long lreportType = null;
//        if(this.getReportType() >0){
//            lreportType = Long.valueOf(this.getReportType());
//        }
//        return lreportType;
//    }
}

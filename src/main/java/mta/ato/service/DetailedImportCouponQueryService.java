package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.DetailedImportCoupon;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.DetailedImportCouponRepository;
import mta.ato.service.dto.DetailedImportCouponCriteria;
import mta.ato.service.dto.DetailedImportCouponDTO;
import mta.ato.service.mapper.DetailedImportCouponMapper;

/**
 * Service for executing complex queries for {@link DetailedImportCoupon} entities in the database.
 * The main input is a {@link DetailedImportCouponCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DetailedImportCouponDTO} or a {@link Page} of {@link DetailedImportCouponDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DetailedImportCouponQueryService extends QueryService<DetailedImportCoupon> {

    private final Logger log = LoggerFactory.getLogger(DetailedImportCouponQueryService.class);

    private final DetailedImportCouponRepository detailedImportCouponRepository;

    private final DetailedImportCouponMapper detailedImportCouponMapper;

    public DetailedImportCouponQueryService(DetailedImportCouponRepository detailedImportCouponRepository, DetailedImportCouponMapper detailedImportCouponMapper) {
        this.detailedImportCouponRepository = detailedImportCouponRepository;
        this.detailedImportCouponMapper = detailedImportCouponMapper;
    }

    /**
     * Return a {@link List} of {@link DetailedImportCouponDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DetailedImportCouponDTO> findByCriteria(DetailedImportCouponCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DetailedImportCoupon> specification = createSpecification(criteria);
        return detailedImportCouponMapper.toDto(detailedImportCouponRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DetailedImportCouponDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DetailedImportCouponDTO> findByCriteria(DetailedImportCouponCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DetailedImportCoupon> specification = createSpecification(criteria);
        return detailedImportCouponRepository.findAll(specification, page)
            .map(detailedImportCouponMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DetailedImportCouponCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DetailedImportCoupon> specification = createSpecification(criteria);
        return detailedImportCouponRepository.count(specification);
    }

    /**
     * Function to convert {@link DetailedImportCouponCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DetailedImportCoupon> createSpecification(DetailedImportCouponCriteria criteria) {
        Specification<DetailedImportCoupon> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DetailedImportCoupon_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), DetailedImportCoupon_.amount));
            }
            if (criteria.getImportPrice() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getImportPrice(), DetailedImportCoupon_.importPrice));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), DetailedImportCoupon_.updateTime));
            }
            if (criteria.getImportCouponId() != null) {
                specification = specification.and(buildSpecification(criteria.getImportCouponId(),
                    root -> root.join(DetailedImportCoupon_.importCoupon, JoinType.LEFT).get(ImportCoupon_.id)));
            }
            if (criteria.getProductsId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductsId(),
                    root -> root.join(DetailedImportCoupon_.products, JoinType.LEFT).get(Products_.id)));
            }
            if (criteria.getSizesId() != null) {
                specification = specification.and(buildSpecification(criteria.getSizesId(),
                    root -> root.join(DetailedImportCoupon_.sizes, JoinType.LEFT).get(Sizes_.id)));
            }
        }
        return specification;
    }
}

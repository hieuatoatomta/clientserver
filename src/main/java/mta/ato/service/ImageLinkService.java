package mta.ato.service;

import mta.ato.domain.ImageLink;
import mta.ato.repository.ImageLinkRepository;
import mta.ato.service.dto.ImageLinkDTO;
import mta.ato.service.mapper.ImageLinkMapper;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ImageLink}.
 */
@Service
@Transactional
public class ImageLinkService {

    private final Logger log = LoggerFactory.getLogger(ImageLinkService.class);

    private final ImageLinkRepository imageLinkRepository;

    private final ImageLinkMapper imageLinkMapper;

    public ImageLinkService(ImageLinkRepository imageLinkRepository, ImageLinkMapper imageLinkMapper) {
        this.imageLinkRepository = imageLinkRepository;
        this.imageLinkMapper = imageLinkMapper;
    }

    /**
     * Save a imageLink.
     *
     * @return the persisted entity.
     */
    public ImageLink save(ImageLink imageLink) {
        log.debug("Request to save ImageLink : {}", imageLink);
        return imageLinkRepository.save(imageLink);
    }

    /**
     * Get all the imageLinks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ImageLinkDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ImageLinks");
        return imageLinkRepository.findAll(pageable)
            .map(imageLinkMapper::toDto);
    }


    /**
     * Get one imageLink by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ImageLinkDTO> findOne(Long id) {
        log.debug("Request to get ImageLink : {}", id);
        return imageLinkRepository.findById(id)
            .map(imageLinkMapper::toDto);
    }

    /**
     * Delete the imageLink by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ImageLink : {}", id);
        imageLinkRepository.deleteById(id);
    }


    public Long lock(ImageLinkDTO obj) {
        try {
            Optional<ImageLink> imageLink = imageLinkRepository.findById(obj.getId());
            ImageLink imageLink1 = imageLink.get();
            if (imageLink1.getStatus() == 1) {
                imageLink1.setStatus(0L);
            } else if (imageLink1.getStatus() == 0) {
                imageLink1.setStatus(1L);
            }
            imageLinkRepository.save(imageLink1);
            return obj.getId();

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ServiceException("Có lỗi trong quá trình thêm mới");
        }
    }
}

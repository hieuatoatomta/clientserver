package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.ProductSize;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.ProductSizeRepository;
import mta.ato.service.dto.ProductSizeCriteria;
import mta.ato.service.dto.ProductSizeDTO;
import mta.ato.service.mapper.ProductSizeMapper;

/**
 * Service for executing complex queries for {@link ProductSize} entities in the database.
 * The main input is a {@link ProductSizeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProductSizeDTO} or a {@link Page} of {@link ProductSizeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProductSizeQueryService extends QueryService<ProductSize> {

    private final Logger log = LoggerFactory.getLogger(ProductSizeQueryService.class);

    private final ProductSizeRepository productSizeRepository;

    private final ProductSizeMapper productSizeMapper;

    public ProductSizeQueryService(ProductSizeRepository productSizeRepository, ProductSizeMapper productSizeMapper) {
        this.productSizeRepository = productSizeRepository;
        this.productSizeMapper = productSizeMapper;
    }

    /**
     * Return a {@link List} of {@link ProductSizeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProductSizeDTO> findByCriteria(ProductSizeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProductSize> specification = createSpecification(criteria);
        return productSizeMapper.toDto(productSizeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProductSizeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProductSizeDTO> findByCriteria(ProductSizeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProductSize> specification = createSpecification(criteria);
        return productSizeRepository.findAll(specification, page)
            .map(productSizeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProductSizeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProductSize> specification = createSpecification(criteria);
        return productSizeRepository.count(specification);
    }

    /**
     * Function to convert {@link ProductSizeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ProductSize> createSpecification(ProductSizeCriteria criteria) {
        Specification<ProductSize> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ProductSize_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), ProductSize_.amount));
            }
            if (criteria.getDsDetailedOrdersId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsDetailedOrdersId(),
                    root -> root.join(ProductSize_.dsDetailedOrders, JoinType.LEFT).get(DetailedOrders_.id)));
            }
            if (criteria.getProductsId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductsId(),
                    root -> root.join(ProductSize_.products, JoinType.LEFT).get(Products_.id)));
            }
            if (criteria.getSizesId() != null) {
                specification = specification.and(buildSpecification(criteria.getSizesId(),
                    root -> root.join(ProductSize_.sizes, JoinType.LEFT).get(Sizes_.id)));
            }
        }
        return specification;
    }
}

package mta.ato.service;

import io.github.jhipster.service.QueryService;
import mta.ato.domain.ImageLink;
import mta.ato.domain.ImageLink_;
import mta.ato.domain.Products_;
import mta.ato.repository.ImageLinkRepository;
import mta.ato.service.dto.ImageLinkCriteria;
import mta.ato.service.dto.ImageLinkDTO;
import mta.ato.service.mapper.ImageLinkMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.List;

/**
 * Service for executing complex queries for {@link ImageLink} entities in the database.
 * The main input is a {@link ImageLinkCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ImageLinkDTO} or a {@link Page} of {@link ImageLinkDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ImageLinkQueryService extends QueryService<ImageLink> {

    private final Logger log = LoggerFactory.getLogger(ImageLinkQueryService.class);

    private final ImageLinkRepository imageLinkRepository;

    private final ImageLinkMapper imageLinkMapper;

    public ImageLinkQueryService(ImageLinkRepository imageLinkRepository, ImageLinkMapper imageLinkMapper) {
        this.imageLinkRepository = imageLinkRepository;
        this.imageLinkMapper = imageLinkMapper;
    }

    /**
     * Return a {@link List} of {@link ImageLinkDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ImageLinkDTO> findByCriteria(ImageLinkCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ImageLink> specification = createSpecification(criteria);
        return imageLinkMapper.toDto(imageLinkRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ImageLinkDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ImageLinkDTO> findByCriteria(ImageLinkCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ImageLink> specification = createSpecification(criteria);
        return imageLinkRepository.findAll(specification, page)
            .map(imageLinkMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ImageLinkCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ImageLink> specification = createSpecification(criteria);
        return imageLinkRepository.count(specification);
    }

    /**
     * Function to convert {@link ImageLinkCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ImageLink> createSpecification(ImageLinkCriteria criteria) {
        Specification<ImageLink> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ImageLink_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ImageLink_.name));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), ImageLink_.status));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), ImageLink_.updateTime));
            }
            if (criteria.getImageLink() != null) {
                specification = specification.and(buildStringSpecification(criteria.getImageLink(), ImageLink_.imageLink));
            }
            if (criteria.getProductsId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductsId(),
                    root -> root.join(ImageLink_.products, JoinType.LEFT).get(Products_.id)));
            }
        }
        return specification;
    }
}

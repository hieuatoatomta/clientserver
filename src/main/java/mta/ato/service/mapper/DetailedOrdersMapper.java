package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.DetailedOrdersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DetailedOrders} and its DTO {@link DetailedOrdersDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrdersMapper.class, ProductSizeMapper.class})
public interface DetailedOrdersMapper extends EntityMapper<DetailedOrdersDTO, DetailedOrders> {

    @Mapping(source = "orders.id", target = "ordersId")
    @Mapping(source = "productSize.id", target = "productSizeId")
    DetailedOrdersDTO toDto(DetailedOrders detailedOrders);

    @Mapping(source = "ordersId", target = "orders")
    @Mapping(source = "productSizeId", target = "productSize")
    DetailedOrders toEntity(DetailedOrdersDTO detailedOrdersDTO);

    default DetailedOrders fromId(Long id) {
        if (id == null) {
            return null;
        }
        DetailedOrders detailedOrders = new DetailedOrders();
        detailedOrders.setId(id);
        return detailedOrders;
    }
}

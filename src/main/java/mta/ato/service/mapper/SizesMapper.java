package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.SizesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Sizes} and its DTO {@link SizesDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SizesMapper extends EntityMapper<SizesDTO, Sizes> {


    @Mapping(target = "dsProductSizes", ignore = true)
    @Mapping(target = "removeDsProductSize", ignore = true)
    @Mapping(target = "dsDetailedImportCoupons", ignore = true)
    @Mapping(target = "removeDsDetailedImportCoupon", ignore = true)
    Sizes toEntity(SizesDTO sizesDTO);

    default Sizes fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sizes sizes = new Sizes();
        sizes.setId(id);
        return sizes;
    }
}

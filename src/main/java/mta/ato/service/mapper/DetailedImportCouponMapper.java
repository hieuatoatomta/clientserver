package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.DetailedImportCouponDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link DetailedImportCoupon} and its DTO {@link DetailedImportCouponDTO}.
 */
@Mapper(componentModel = "spring", uses = {ImportCouponMapper.class, ProductsMapper.class, SizesMapper.class})
public interface DetailedImportCouponMapper extends EntityMapper<DetailedImportCouponDTO, DetailedImportCoupon> {

    @Mapping(source = "importCoupon.id", target = "importCouponId")
    @Mapping(source = "products.id", target = "productsId")
    @Mapping(source = "sizes.id", target = "sizesId")
    DetailedImportCouponDTO toDto(DetailedImportCoupon detailedImportCoupon);

    @Mapping(source = "importCouponId", target = "importCoupon")
    @Mapping(source = "productsId", target = "products")
    @Mapping(source = "sizesId", target = "sizes")
    DetailedImportCoupon toEntity(DetailedImportCouponDTO detailedImportCouponDTO);

    default DetailedImportCoupon fromId(Long id) {
        if (id == null) {
            return null;
        }
        DetailedImportCoupon detailedImportCoupon = new DetailedImportCoupon();
        detailedImportCoupon.setId(id);
        return detailedImportCoupon;
    }
}

package mta.ato.service.mapper;


import mta.ato.domain.Objects;
import mta.ato.service.dto.ObjectsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Objects} and its DTO {@link ObjectsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ObjectsMapper extends EntityMapper<ObjectsDTO, Objects> {


    @Mapping(target = "dsRoleObjects", ignore = true)
    @Mapping(target = "removeDsRoleObject", ignore = true)
    @Mapping(target = "dsObjectActions", ignore = true)
    @Mapping(target = "removeDsObjectAction", ignore = true)
    @Mapping(target = "dsObjects", ignore = true)
    @Mapping(target = "removeDsObjects", ignore = true)
    Objects toEntity(ObjectsDTO objectsDTO);

    default Objects fromId(Long id) {
        if (id == null) {
            return null;
        }
        Objects objects = new Objects();
        objects.setId(id);
        return objects;
    }
}

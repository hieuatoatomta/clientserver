package mta.ato.service.mapper;


import mta.ato.domain.ObjectAction;
import mta.ato.service.dto.ObjectActionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link ObjectAction} and its DTO {@link ObjectActionDTO}.
 */
@Mapper(componentModel = "spring", uses = {ObjectsMapper.class, ActionsMapper.class})
public interface ObjectActionMapper extends EntityMapper<ObjectActionDTO, ObjectAction> {

    @Mapping(source = "objects.id", target = "objectsId")
    @Mapping(source = "actions.id", target = "actionsId")
    ObjectActionDTO toDto(ObjectAction objectAction);

    @Mapping(source = "objectsId", target = "objects")
    @Mapping(source = "actionsId", target = "actions")
    ObjectAction toEntity(ObjectActionDTO objectActionDTO);

    default ObjectAction fromId(Long id) {
        if (id == null) {
            return null;
        }
        ObjectAction objectAction = new ObjectAction();
        objectAction.setId(id);
        return objectAction;
    }
}

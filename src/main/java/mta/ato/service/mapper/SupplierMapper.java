package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.SupplierDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Supplier} and its DTO {@link SupplierDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SupplierMapper extends EntityMapper<SupplierDTO, Supplier> {


    @Mapping(target = "dsImportCoupons", ignore = true)
    @Mapping(target = "removeDsImportCoupon", ignore = true)
    Supplier toEntity(SupplierDTO supplierDTO);

    default Supplier fromId(Long id) {
        if (id == null) {
            return null;
        }
        Supplier supplier = new Supplier();
        supplier.setId(id);
        return supplier;
    }
}

package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.RolesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Roles} and its DTO {@link RolesDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RolesMapper extends EntityMapper<RolesDTO, Roles> {


    @Mapping(target = "dsUserRoles", ignore = true)
    @Mapping(target = "removeDsUserRole", ignore = true)
    @Mapping(target = "dsRoleObjects", ignore = true)
    @Mapping(target = "removeDsRoleObject", ignore = true)
    Roles toEntity(RolesDTO rolesDTO);

    default Roles fromId(Long id) {
        if (id == null) {
            return null;
        }
        Roles roles = new Roles();
        roles.setId(id);
        return roles;
    }
}

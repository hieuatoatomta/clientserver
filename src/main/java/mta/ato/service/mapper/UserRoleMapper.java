package mta.ato.service.mapper;


import mta.ato.domain.UserRole;
import mta.ato.service.dto.UserRoleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link UserRole} and its DTO {@link UserRoleDTO}.
 */
@Mapper(componentModel = "spring", uses = {UsersMapper.class, RolesMapper.class})
public interface UserRoleMapper extends EntityMapper<UserRoleDTO, UserRole> {

    @Mapping(source = "users.id", target = "usersId")
    @Mapping(source = "roles.id", target = "rolesId")
    UserRoleDTO toDto(UserRole userRole);

    @Mapping(source = "usersId", target = "users")
    @Mapping(source = "rolesId", target = "roles")
    UserRole toEntity(UserRoleDTO userRoleDTO);

    default UserRole fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserRole userRole = new UserRole();
        userRole.setId(id);
        return userRole;
    }
}

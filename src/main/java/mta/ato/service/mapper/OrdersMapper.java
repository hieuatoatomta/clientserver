package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.OrdersDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Orders} and its DTO {@link OrdersDTO}.
 */
@Mapper(componentModel = "spring", uses = {UsersMapper.class})
public interface OrdersMapper extends EntityMapper<OrdersDTO, Orders> {

    @Mapping(source = "users.id", target = "usersId")
    OrdersDTO toDto(Orders orders);

    @Mapping(target = "dsDetailedOrders", ignore = true)
    @Mapping(target = "removeDsDetailedOrders", ignore = true)
    @Mapping(source = "usersId", target = "users")
    Orders toEntity(OrdersDTO ordersDTO);

    default Orders fromId(Long id) {
        if (id == null) {
            return null;
        }
        Orders orders = new Orders();
        orders.setId(id);
        return orders;
    }
}

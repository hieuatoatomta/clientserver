package mta.ato.service.mapper;


import mta.ato.domain.ImageLink;
import mta.ato.service.dto.ImageLinkDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link ImageLink} and its DTO {@link ImageLinkDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProductsMapper.class})
public interface ImageLinkMapper extends EntityMapper<ImageLinkDTO, ImageLink> {

    @Mapping(source = "products.id", target = "productsId")
    ImageLinkDTO toDto(ImageLink imageLink);

    @Mapping(source = "productsId", target = "products")
    ImageLink toEntity(ImageLinkDTO imageLinkDTO);

    default ImageLink fromId(Long id) {
        if (id == null) {
            return null;
        }
        ImageLink imageLink = new ImageLink();
        imageLink.setId(id);
        return imageLink;
    }
}

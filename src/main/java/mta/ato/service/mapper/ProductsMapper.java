package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.ProductsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Products} and its DTO {@link ProductsDTO}.
 */
@Mapper(componentModel = "spring", uses = {ObjectsMapper.class})
public interface ProductsMapper extends EntityMapper<ProductsDTO, Products> {

    @Mapping(source = "objects.id", target = "objectsId")
    ProductsDTO toDto(Products products);

    @Mapping(target = "dsProductSizes", ignore = true)
    @Mapping(target = "removeDsProductSize", ignore = true)
    @Mapping(target = "dsImageLinks", ignore = true)
    @Mapping(target = "removeDsImageLink", ignore = true)
    @Mapping(target = "dsDetailedImportCoupons", ignore = true)
    @Mapping(target = "removeDsDetailedImportCoupon", ignore = true)
    @Mapping(source = "objectsId", target = "objects")
    Products toEntity(ProductsDTO productsDTO);

    default Products fromId(Long id) {
        if (id == null) {
            return null;
        }
        Products products = new Products();
        products.setId(id);
        return products;
    }
}

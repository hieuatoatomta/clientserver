package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.ImportCouponDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ImportCoupon} and its DTO {@link ImportCouponDTO}.
 */
@Mapper(componentModel = "spring", uses = {UsersMapper.class, SupplierMapper.class})
public interface ImportCouponMapper extends EntityMapper<ImportCouponDTO, ImportCoupon> {

    @Mapping(source = "users.id", target = "usersId")
    @Mapping(source = "supplier.id", target = "supplierId")
    ImportCouponDTO toDto(ImportCoupon importCoupon);

    @Mapping(target = "dsReceipts", ignore = true)
    @Mapping(target = "removeDsReceipt", ignore = true)
    @Mapping(source = "usersId", target = "users")
    @Mapping(source = "supplierId", target = "supplier")
    ImportCoupon toEntity(ImportCouponDTO importCouponDTO);

    default ImportCoupon fromId(Long id) {
        if (id == null) {
            return null;
        }
        ImportCoupon importCoupon = new ImportCoupon();
        importCoupon.setId(id);
        return importCoupon;
    }
}

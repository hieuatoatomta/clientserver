package mta.ato.service.mapper;


import mta.ato.domain.*;
import mta.ato.service.dto.ProductSizeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProductSize} and its DTO {@link ProductSizeDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProductsMapper.class, SizesMapper.class})
public interface ProductSizeMapper extends EntityMapper<ProductSizeDTO, ProductSize> {

    @Mapping(source = "products.id", target = "productsId")
    @Mapping(source = "sizes.id", target = "sizesId")
    ProductSizeDTO toDto(ProductSize productSize);

    @Mapping(target = "dsDetailedOrders", ignore = true)
    @Mapping(target = "removeDsDetailedOrders", ignore = true)
    @Mapping(source = "productsId", target = "products")
    @Mapping(source = "sizesId", target = "sizes")
    ProductSize toEntity(ProductSizeDTO productSizeDTO);

    default ProductSize fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProductSize productSize = new ProductSize();
        productSize.setId(id);
        return productSize;
    }
}

package mta.ato.service.mapper;


import mta.ato.domain.Billing;
import mta.ato.service.dto.BillingDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link Billing} and its DTO {@link BillingDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BillingMapper extends EntityMapper<BillingDTO, Billing> {



    default Billing fromId(Long id) {
        if (id == null) {
            return null;
        }
        Billing billing = new Billing();
        billing.setId(id);
        return billing;
    }
}

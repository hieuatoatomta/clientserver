package mta.ato.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import mta.ato.domain.Sizes;
import mta.ato.domain.*; // for static metamodels
import mta.ato.repository.SizesRepository;
import mta.ato.service.dto.SizesCriteria;
import mta.ato.service.dto.SizesDTO;
import mta.ato.service.mapper.SizesMapper;

/**
 * Service for executing complex queries for {@link Sizes} entities in the database.
 * The main input is a {@link SizesCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SizesDTO} or a {@link Page} of {@link SizesDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SizesQueryService extends QueryService<Sizes> {

    private final Logger log = LoggerFactory.getLogger(SizesQueryService.class);

    private final SizesRepository sizesRepository;

    private final SizesMapper sizesMapper;

    public SizesQueryService(SizesRepository sizesRepository, SizesMapper sizesMapper) {
        this.sizesRepository = sizesRepository;
        this.sizesMapper = sizesMapper;
    }

    /**
     * Return a {@link List} of {@link SizesDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SizesDTO> findByCriteria(SizesCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Sizes> specification = createSpecification(criteria);
        return sizesMapper.toDto(sizesRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SizesDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Sizes> findByCriteria(SizesCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Sizes> specification = createSpecification(criteria);
        return sizesRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SizesCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Sizes> specification = createSpecification(criteria);
        return sizesRepository.count(specification);
    }

    /**
     * Function to convert {@link SizesCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Sizes> createSpecification(SizesCriteria criteria) {
        Specification<Sizes> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Sizes_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Sizes_.name));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Sizes_.code));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Sizes_.description));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), Sizes_.status));
            }
            if (criteria.getUpdateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateTime(), Sizes_.updateTime));
            }
            if (criteria.getDsProductSizeId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsProductSizeId(),
                    root -> root.join(Sizes_.dsProductSizes, JoinType.LEFT).get(ProductSize_.id)));
            }
            if (criteria.getDsDetailedImportCouponId() != null) {
                specification = specification.and(buildSpecification(criteria.getDsDetailedImportCouponId(),
                    root -> root.join(Sizes_.dsDetailedImportCoupons, JoinType.LEFT).get(DetailedImportCoupon_.id)));
            }
        }
        return specification;
    }
}

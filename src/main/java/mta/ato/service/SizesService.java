package mta.ato.service;

import mta.ato.domain.ProductSize;
import mta.ato.domain.Sizes;
import mta.ato.repository.SizesRepository;
import mta.ato.service.dto.SizesDTO;
import mta.ato.service.mapper.SizesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Sizes}.
 */
@Service
@Transactional
public class SizesService {

    private final Logger log = LoggerFactory.getLogger(SizesService.class);

    private final SizesRepository sizesRepository;

    private final SizesMapper sizesMapper;

    public SizesService(SizesRepository sizesRepository, SizesMapper sizesMapper) {
        this.sizesRepository = sizesRepository;
        this.sizesMapper = sizesMapper;
    }

    /**
     * Save a sizes.
     *
     * @param sizesDTO the entity to save.
     * @return the persisted entity.
     */
    public SizesDTO save(SizesDTO sizesDTO) {
        log.debug("Request to save Sizes : {}", sizesDTO);
        Sizes sizes = sizesMapper.toEntity(sizesDTO);
        sizes.setUpdateTime(ZonedDateTime.now());
        sizes = sizesRepository.save(sizes);
        return sizesMapper.toDto(sizes);
    }

    /**
     * Get all the sizes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SizesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Sizes");
        return sizesRepository.findAll(pageable)
            .map(sizesMapper::toDto);
    }


    /**
     * Get one sizes by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Sizes> findOne(Long id) {
        log.debug("Request to get Sizes : {}", id);
        return sizesRepository.findById(id);
    }

    /**
     * Delete the sizes by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Sizes : {}", id);
        sizesRepository.deleteById(id);
    }


    @Transactional(readOnly = true)
    public Sizes findByCode(String code) {
        log.debug("Request to get all Roles");
        return sizesRepository.findByCode(code);
    }

    @Transactional(readOnly = true)
    public List<Sizes> searchSize(Long id) {
        return sizesRepository.searchSize(id);
    }
}

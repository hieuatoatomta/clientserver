package mta.ato.service;

import mta.ato.domain.DetailedOrders;
import mta.ato.repository.DetailedOrdersRepository;
import mta.ato.service.dto.DetailedOrdersDTO;
import mta.ato.service.mapper.DetailedOrdersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link DetailedOrders}.
 */
@Service
@Transactional
public class DetailedOrdersService {

    private final Logger log = LoggerFactory.getLogger(DetailedOrdersService.class);

    private final DetailedOrdersRepository detailedOrdersRepository;

    private final DetailedOrdersMapper detailedOrdersMapper;

    public DetailedOrdersService(DetailedOrdersRepository detailedOrdersRepository, DetailedOrdersMapper detailedOrdersMapper) {
        this.detailedOrdersRepository = detailedOrdersRepository;
        this.detailedOrdersMapper = detailedOrdersMapper;
    }

    /**
     * Save a detailedOrders.
     *
     * @param detailedOrdersDTO the entity to save.
     * @return the persisted entity.
     */
    public DetailedOrdersDTO save(DetailedOrdersDTO detailedOrdersDTO) {
        log.debug("Request to save DetailedOrders : {}", detailedOrdersDTO);
        DetailedOrders detailedOrders = detailedOrdersMapper.toEntity(detailedOrdersDTO);
        detailedOrders = detailedOrdersRepository.save(detailedOrders);
        return detailedOrdersMapper.toDto(detailedOrders);
    }

    /**
     * Get all the detailedOrders.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DetailedOrdersDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DetailedOrders");
        return detailedOrdersRepository.findAll(pageable)
            .map(detailedOrdersMapper::toDto);
    }


    /**
     * Get one detailedOrders by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DetailedOrdersDTO> findOne(Long id) {
        log.debug("Request to get DetailedOrders : {}", id);
        return detailedOrdersRepository.findById(id)
            .map(detailedOrdersMapper::toDto);
    }

    /**
     * Delete the detailedOrders by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DetailedOrders : {}", id);
        detailedOrdersRepository.deleteById(id);
    }
}

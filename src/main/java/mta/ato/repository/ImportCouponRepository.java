package mta.ato.repository;

import mta.ato.domain.ImportCoupon;

import mta.ato.domain.Sizes;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ImportCoupon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImportCouponRepository extends JpaRepository<ImportCoupon, Long>, JpaSpecificationExecutor<ImportCoupon> {
    ImportCoupon findByCode(String code);
}

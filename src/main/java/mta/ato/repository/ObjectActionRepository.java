package mta.ato.repository;

import mta.ato.domain.ObjectAction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ObjectAction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ObjectActionRepository extends JpaRepository<ObjectAction, Long>, JpaSpecificationExecutor<ObjectAction> {
}

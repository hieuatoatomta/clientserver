package mta.ato.repository;

import mta.ato.domain.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Orders entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long>, JpaSpecificationExecutor<Orders> {
    Orders findByCode(String code);

    @Query(value = " SELECT \n" +
        "    DATE_FORMAT(a1.update_time, '%Y/%m') update_time,\n" +
        "    total_import\n" +
        "FROM\n" +
        "    (SELECT \n" +
        "        DATE_FORMAT(a.update_time, '%Y/%m/01') update_time,\n" +
        "            SUM(b.amount) total_import\n" +
        "    FROM\n" +
        "        import_coupon AS a\n" +
        "    INNER JOIN detailed_import_coupon AS b ON a.id = b.import_coupon_id\n" +
        "    WHERE\n" +
        "        DATE(a.update_time) >= :fromTime\n" +
        "            AND DATE(a.update_time) <= :toTime\n" +
        "    GROUP BY DATE_FORMAT(a.update_time, '%Y/%m/01') UNION ALL SELECT \n" +
        "        *\n" +
        "    FROM\n" +
        "        (SELECT \n" +
        "        DATE_FORMAT(m1, '%Y/%m/01') AS update_time, 0 AS totalImport\n" +
        "    FROM\n" +
        "        (SELECT \n" +
        "        (:fromTime - INTERVAL DAYOFMONTH(:fromTime) - 1 DAY) + INTERVAL m MONTH AS m1\n" +
        "    FROM\n" +
        "        (SELECT \n" +
        "        @rownum::=@rownum + 1 AS m\n" +
        "    FROM\n" +
        "        (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t1, (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t2, (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t3, (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t4, (SELECT @rownum ::=- 1) t0) d1) d2\n" +
        "    WHERE\n" +
        "        m1 <= :toTime\n" +
        "    ORDER BY m1) AS t1\n" +
        "    WHERE\n" +
        "        t1.update_time NOT IN (SELECT \n" +
        "                DATE_FORMAT(a.update_time, '%Y/%m/01') update_time\n" +
        "            FROM\n" +
        "                import_coupon AS a\n" +
        "            WHERE\n" +
        "                DATE(a.update_time) >= :fromTime\n" +
        "                    AND DATE(a.update_time) <= :toTime\n" +
        "            GROUP BY DATE_FORMAT(a.update_time, '%Y/%m/01'))) AS a1\n" +
        "ORDER BY a1.update_time ", nativeQuery = true)
    List<Object[]> getSendCount(@Param("fromTime") String fromTime, @Param("toTime") String toTime);



    @Query(value = "\n" +
        "select DATE_FORMAT(a1.update_time, '%Y/%m'), total_import FROM (\n" +
        "SELECT \n" +
        "    DATE_FORMAT(a.update_time, '%Y/%m/01') update_time,\n" +
        "    SUM(b.amount) total_import\n" +
        "FROM\n" +
        "    orders AS a\n" +
        "        INNER JOIN\n" +
        "    detailed_orders AS b ON a.id = b.orders_id\n" +
        "WHERE\n" +
        "    DATE(a.update_time) >= :fromTime\n" +
        "        AND DATE(a.update_time) <= :toTime\n" +
        "GROUP BY DATE_FORMAT(a.update_time, '%Y/%m/01') \n" +
        "UNION ALL SELECT \n" +
        "    *\n" +
        "FROM\n" +
        "    (SELECT \n" +
        "        DATE_FORMAT(m1, '%Y/%m/01') AS update_time, 0 AS totalImport\n" +
        "    FROM\n" +
        "        (SELECT \n" +
        "        (:fromTime - INTERVAL DAYOFMONTH(:fromTime) - 1 DAY) + INTERVAL m MONTH AS m1\n" +
        "    FROM\n" +
        "        (SELECT \n" +
        "        @rownum::=@rownum + 1 AS m\n" +
        "    FROM\n" +
        "        (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t1, (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t2, (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t3, (SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t4, (SELECT @rownum::=- 1) t0) d1) d2\n" +
        "    WHERE\n" +
        "        m1 <= :toTime\n" +
        "    ORDER BY m1) AS t1\n" +
        "WHERE\n" +
        "    t1.update_time NOT IN (SELECT \n" +
        "            DATE_FORMAT(a.update_time, '%Y/%m/01') update_time\n" +
        "        FROM\n" +
        "            import_coupon AS a\n" +
        "        WHERE\n" +
        "            DATE(a.update_time) >= :fromTime\n" +
        "                AND DATE(a.update_time) <= :toTime\n" +
        "        GROUP BY DATE_FORMAT(a.update_time, '%Y/%m/01'))\n" +
        ") as a1 order by a1.update_time ", nativeQuery = true)
    List<Object[]> getOrdersCount(@Param("fromTime") String fromTime, @Param("toTime") String toTime);


    @Query(value = "\tSELECT \n" +
        "    SUM(ctn.amount * ctn.import_price) AS total_imports,\n" +
        "    SUM(cthd.amount * cthd.price) AS total_orders \n" +
        "FROM\n" +
        "    detailed_orders AS cthd,\n" +
        "    detailed_import_coupon AS ctn,\n" +
        "    import_coupon WHERE\n" +
        "    DATE(cthd.update_time) >= :fromTime \n" +
        "        AND DATE(cthd.update_time) <= :toTime and  DATE(ctn.update_time) >= :fromTime \n" +
        "        AND DATE(ctn.update_time) <= :toTime ", nativeQuery = true)
    List<Object[]> getDoanhThu(@Param("fromTime") String fromTime, @Param("toTime") String toTime);

    @Query(value = "select p.code, p.name as nameProducts, s.name as nameSize, SUM(dto.amount) as totalOrder\n" +
        "from detailed_orders as dto, product_size as ps, products as p, sizes as s\n" +
        "where dto.product_size_id = ps.id\n" +
        "and ps.products_id = p.id\n" +
        "and ps.sizes_id = s.id\n" +
        "and  DATE(dto.update_time) >= :fromTime\n" +
        "                AND DATE(dto.update_time) <= :toTime\n" +
        "group by p.name, s.name, p.code\n" +
        "Order by SUM(dto.amount) desc limit :soLuong ", nativeQuery = true)
    List<Object[]> getSanPhamBanChay(@Param("soLuong") Long soLuong, @Param("fromTime") String fromTime, @Param("toTime") String toTime);

    @Query(value = "select  u.full_name as full_name, u.mail as mail, sum(dor.amount) as totalOrder, sum(dor.amount*dor.price) as totalPrice\n" +
        " from users as u\n" +
        " join orders as uo on u.id = uo.users_id\n" +
        " join detailed_orders as dor on dor.orders_id = uo.id\n" +
        " where DATE(uo.update_time) >= :fromTime\n" +
        "                AND DATE(uo.update_time) <= :toTime\n" +
        " group by uo.users_id\n" +
        " order by sum(dor.amount) desc\n" +
        " limit :soLuong ", nativeQuery = true)
    List<Object[]> getTopKhachHang(@Param("soLuong") Long soLuong, @Param("fromTime") String fromTime, @Param("toTime") String toTime);
}

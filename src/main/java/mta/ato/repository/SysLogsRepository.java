package mta.ato.repository;

import mta.ato.domain.SysLogs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the SysLogs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SysLogsRepository extends JpaRepository<SysLogs, Long>, JpaSpecificationExecutor<SysLogs> {
}

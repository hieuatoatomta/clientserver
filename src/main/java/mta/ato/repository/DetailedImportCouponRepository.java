package mta.ato.repository;

import mta.ato.domain.DetailedImportCoupon;

import mta.ato.domain.ImportCoupon;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the DetailedImportCoupon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetailedImportCouponRepository extends JpaRepository<DetailedImportCoupon, Long>, JpaSpecificationExecutor<DetailedImportCoupon> {
    List<DetailedImportCoupon> findAllByImportCoupon(ImportCoupon importCoupon);
}

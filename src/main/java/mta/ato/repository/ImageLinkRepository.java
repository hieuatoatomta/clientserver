package mta.ato.repository;

import mta.ato.domain.ImageLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ImageLink entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImageLinkRepository extends JpaRepository<ImageLink, Long>, JpaSpecificationExecutor<ImageLink> {
}

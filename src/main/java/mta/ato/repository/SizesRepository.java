package mta.ato.repository;

import mta.ato.domain.ProductSize;
import mta.ato.domain.Sizes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Sizes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SizesRepository extends JpaRepository<Sizes, Long>, JpaSpecificationExecutor<Sizes> {
    Sizes findByCode(String code);
    @Query(value = "SELECT sizes " +
        "FROM Sizes sizes inner join ProductSize product_size on sizes.id = product_size.sizes.id\n" +
        "where product_size.products.id = :id")
    List<Sizes> searchSize(@Param("id") Long id);
}

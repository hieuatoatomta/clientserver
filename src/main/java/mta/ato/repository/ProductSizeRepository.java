package mta.ato.repository;

import mta.ato.domain.ProductSize;
import mta.ato.domain.Products;
import mta.ato.domain.Sizes;
import mta.ato.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ProductSize entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductSizeRepository extends JpaRepository<ProductSize, Long>, JpaSpecificationExecutor<ProductSize> {
    ProductSize findByProductsAndSizes(Products products, Sizes sizes);
}

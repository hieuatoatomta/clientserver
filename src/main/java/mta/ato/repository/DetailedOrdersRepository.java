package mta.ato.repository;

import mta.ato.domain.DetailedOrders;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the DetailedOrders entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DetailedOrdersRepository extends JpaRepository<DetailedOrders, Long>, JpaSpecificationExecutor<DetailedOrders> {
}

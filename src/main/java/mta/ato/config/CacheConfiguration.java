package mta.ato.config;

import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.config.cache.PrefixedKeyGenerator;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@EnableCaching
public class CacheConfiguration {
    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, mta.ato.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, mta.ato.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, mta.ato.domain.User.class.getName());
            createCache(cm, mta.ato.domain.Authority.class.getName());
            createCache(cm, mta.ato.domain.User.class.getName() + ".authorities");
            createCache(cm, mta.ato.domain.Users.class.getName());
            createCache(cm, mta.ato.domain.Users.class.getName() + ".dsUsers");
            createCache(cm, mta.ato.domain.UserRole.class.getName());
            createCache(cm, mta.ato.domain.RoleObject.class.getName());
            createCache(cm, mta.ato.domain.ObjectAction.class.getName());
            createCache(cm, mta.ato.domain.Roles.class.getName());
            createCache(cm, mta.ato.domain.Roles.class.getName() + ".dsRoles");
            createCache(cm, mta.ato.domain.Objects.class.getName());
            createCache(cm, mta.ato.domain.Objects.class.getName() + ".dsObjects");
            createCache(cm, mta.ato.domain.Actions.class.getName());
            createCache(cm, mta.ato.domain.Actions.class.getName() + ".dsActions");
            createCache(cm, mta.ato.domain.Products.class.getName());
            createCache(cm, mta.ato.domain.Products.class.getName() + ".dsProducts");
            createCache(cm, mta.ato.domain.Sizes.class.getName());
            createCache(cm, mta.ato.domain.Sizes.class.getName() + ".dsSizes");
            createCache(cm, mta.ato.domain.ImageLink.class.getName());
            createCache(cm, mta.ato.domain.ProductSize.class.getName());
            createCache(cm, mta.ato.domain.Users.class.getName() + ".dsUserRoles");
            createCache(cm, mta.ato.domain.Roles.class.getName() + ".dsUserRoles");
            createCache(cm, mta.ato.domain.Roles.class.getName() + ".dsRoleObjects");
            createCache(cm, mta.ato.domain.Objects.class.getName() + ".dsRoleObjects");
            createCache(cm, mta.ato.domain.Objects.class.getName() + ".dsObjectActions");
            createCache(cm, mta.ato.domain.Actions.class.getName() + ".dsRoleObjects");
            createCache(cm, mta.ato.domain.Actions.class.getName() + ".dsObjectActions");
            createCache(cm, mta.ato.domain.Products.class.getName() + ".dsProductSizes");
            createCache(cm, mta.ato.domain.Products.class.getName() + ".dsImageLinks");
            createCache(cm, mta.ato.domain.Sizes.class.getName() + ".dsProductSizes");
            createCache(cm, mta.ato.domain.SysLogs.class.getName());
            createCache(cm, mta.ato.domain.Users.class.getName() + ".dsImportCoupons");
            createCache(cm, mta.ato.domain.Products.class.getName() + ".dsDetailedReceipts");
            createCache(cm, mta.ato.domain.Orders.class.getName());
            createCache(cm, mta.ato.domain.Orders.class.getName() + ".dsImageLinks");
            createCache(cm, mta.ato.domain.ImportCoupon.class.getName());
            createCache(cm, mta.ato.domain.ImportCoupon.class.getName() + ".dsReceipts");
            createCache(cm, mta.ato.domain.DetailedImportCoupon.class.getName());
            createCache(cm, mta.ato.domain.Supplier.class.getName());
            createCache(cm, mta.ato.domain.Supplier.class.getName() + ".dsImportCoupons");
            createCache(cm, mta.ato.domain.Billing.class.getName());
            createCache(cm, mta.ato.domain.ProductSize.class.getName());
            createCache(cm, mta.ato.domain.DetailedImportCoupon.class.getName() + ".dsProductImports");
            createCache(cm, mta.ato.domain.Products.class.getName() + ".dsDetailedImportCoupons");
            createCache(cm, mta.ato.domain.Sizes.class.getName() + ".dsDetailedImportCoupons");
            createCache(cm, mta.ato.domain.Users.class.getName() + ".dsOrders");
            createCache(cm, mta.ato.domain.Orders.class.getName() + ".dsDetailedOrders");
            createCache(cm, mta.ato.domain.DetailedOrders.class.getName());
            createCache(cm, mta.ato.domain.ProductSize.class.getName() + ".dsDetailedOrders");
            createCache(cm, mta.ato.domain.Payment.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}

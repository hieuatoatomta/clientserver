package mta.ato.config;


import mta.ato.repository.ObjectsRepository;
import mta.ato.repository.UsersRepository;
import mta.ato.security.UserDetailsServiceImpl;
import mta.ato.security.jwt.TokenProvider;
import mta.ato.service.dto.DsRolesDTO;
import mta.ato.service.dto.UsersDTO;
import mta.ato.utils.DataUtil;
import mta.ato.utils.Translator;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    TokenProvider tokenProvider;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    ObjectsRepository objectsRepository;

    @Autowired
    UsersRepository UsersRepository;

    @Autowired
    UserDetailsServiceImpl userDetailService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsersDTO loginRequest = (UsersDTO) authentication.getPrincipal();
        CustomUserDetails customUserDetails = (CustomUserDetails) userDetailService.loadUserByUsername( loginRequest.getName() );
        UsersDTO userDTO = customUserDetails.getUser();
        UsernamePasswordAuthenticationToken result = null;
        if (userDTO.getName().equals( loginRequest.getName() )) {
            List<DsRolesDTO> listTreeDTO = getRoles(userDTO);
            customUserDetails.setList( listTreeDTO );
            customUserDetails.setUser( userDTO );
            UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken( customUserDetails.getUser().getName(), customUserDetails.getUser().getPass() );
            String jwt = tokenProvider.createToken( authenticationToken, true );
            customUserDetails.setJwt( jwt );
            result = new UsernamePasswordAuthenticationToken( customUserDetails, customUserDetails.getPassword(),
                    customUserDetails.getAuthorities() );
        } else {
            throw new ServiceException( Translator.toLocale( "login.false" ) );
        }
        return result;
    }
    private List<DsRolesDTO> getRoles(UsersDTO userDTO) {
        List<DsRolesDTO> lstObject = objectsRepository.getRoleAction( userDTO.getId() )
                .stream().map( e -> {
                DsRolesDTO dto = new DsRolesDTO();
                    dto.setCodeAction( DataUtil.safeToString( e[0] ) );
                    dto.setId( DataUtil.safeToLong( e[1] ) );
                    dto.setCode( DataUtil.safeToString( e[2] ) );
                    dto.setTitle( DataUtil.safeToString( e[3] ) );
                    dto.setStatus( DataUtil.safeToLong( e[4] ) );
                    dto.setType(DataUtil.safeToLong( e[5] ) );
                    dto.setIcon( DataUtil.safeToString( e[6] ) );
                    dto.setLink( DataUtil.safeToString( e[7] ) );
                    dto.setParenId( DataUtil.safeToLong( e[8] ) );
                    return dto;
                } ).collect( Collectors.toList() );
        return lstObject;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }
}
